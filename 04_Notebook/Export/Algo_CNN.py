#!/usr/bin/env python
# coding: utf-8

# # CNN ALGORITH

# In[1]:


from tensorflow import keras
import numpy as np
import matplotlib.pyplot as plt
import os
import csv
import pandas as pd
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from itertools import product

import warnings
warnings.filterwarnings('ignore')

plt.style.use('dark_background')
plt.rcParams['figure.figsize'] = [15, 5]


# In[3]:


input_directory = 'C:/Temp/MeasHydroDone/VarPompe/All/'
list_files = os.listdir(input_directory)


# ## DISPLAY LOSS

# In[2]:


def display_loss(history):
    metric = "loss"
    plt.figure()
    plt.plot(history.history[metric])
    plt.plot(history.history["val_" + metric])
    plt.title("model " + metric)
    plt.ylabel(metric, fontsize="large")
    plt.xlabel("epoch", fontsize="large")
    plt.legend(["train", "val"], loc="best")
    plt.show()
    plt.close()


# ## PLOT TEST PREDICTION

# In[3]:


def plotTestPred(test_labels, test_predictions):
    diff_pred = test_predictions - test_labels
    print("Diff Min = ", diff_pred.min(), " Max = ", diff_pred.max())
    a = plt.axes(aspect='equal')
    plt.scatter(test_labels, test_predictions)
    plt.xlabel('True Values')
    plt.ylabel('Predictions')
    lims = [9, 40]
    plt.xlim(lims)
    plt.ylim(lims)
    _ = plt.plot(lims, lims)
    plt.show()


# ## SCALE FEATURES

# In[4]:


"""
    Scale the train and the test set
"""
def scale_features(train_features, test_features):
    scaler = StandardScaler()
    scaler.fit(train_features)
    train_features_n = scaler.transform(train_features)
    test_features_n = scaler.transform(test_features)
    return train_features_n, test_features_n


# ## Split Dataset between Train and test set

# In[5]:


"""
    Split dataset into train/test (70/30)
"""
def split_dataset(dataset, testsize=0.1):
    train, test = train_test_split(dataset, test_size=testsize, random_state=35)
    train_labels = train['FlowRate'].astype(float).to_numpy()
    train_features = train.drop(['FlowRate'], axis=1).to_numpy()
    test_labels = test['FlowRate'].astype(float).to_numpy()
    test_features = test.drop(['FlowRate'], axis=1).to_numpy()

    print("Train data = ", len(train_features), " Test data = ", len(test_features))
    return train_features, train_labels, test_features, test_labels


# ## PREPARE DATA AND SPLIT TRAIN / FIXED TEST

# In[6]:


def split_dataset_fix(dataset, testsize=0.1):
    dataset = dataset.sort_values(by=['FlowRate'])

    len_data = len(dataset)
    y_len = int(len_data * testsize)
    x_len = len_data - y_len
    print(y_len, x_len)

    inter_y = int(np.trunc(len_data / y_len))
    # Split train/test
    test_data = pd.DataFrame()
    train_data = dataset
    for x in range(1, len_data, inter_y):
        test_data = test_data.append(dataset.iloc[x,:])
        train_data = train_data.drop(x)
    # Create numpy array
    train_y = train_data['FlowRate'].astype(float).to_numpy()
    train_X = train_data.drop(['FlowRate'], axis=1).to_numpy()
    test_y = test_data['FlowRate'].astype(float).to_numpy()
    test_X = test_data.drop(['FlowRate'], axis=1).to_numpy()
    # Make permutation after splitting
    idx = np.random.permutation(len(train_X))
    train_X = train_X[idx]
    train_y = train_y[idx]
    
    return train_X, train_y, test_X, test_y


# ## Constraint function

# In[8]:


class CenterAround(keras.constraints.Constraint):
    """Constrains weight tensors to be centered around `ref_value`."""

    def __init__(self, fixPointBits):
        self.fixPointBits = fixPointBits

    def __call__(self, w):
        intWeight = (w * 2**self.fixPointBits)
        return intWeight


# ## MODEL DNN (ONLY MLP)

# In[9]:


"""
    Prepare the model with Keras with only Dense Layer
"""
def make_model_DNN(input_shape, hidden_layer):
    print("Input Shape : ", input_shape)
    input_layer = keras.layers.Input(input_shape)
    previous_layer = input_layer
    constraint = CenterAround(7)
    for layer in hidden_layer:
        next_layer = keras.layers.Dense(layer[0])(previous_layer)
        previous_layer = keras.layers.Activation(layer[1])(next_layer)
    output_layer = keras.layers.Dense(1, activation="tanh")(previous_layer) # TANH FOR REGRESSION OUTPUT
    return keras.models.Model(inputs=input_layer, outputs=output_layer)


# ## MODEL CNN PARALLEL

# In[10]:


"""
    Prepare the model with 3 convolutional layers in parallel
"""
def make_model_CNN_para(input_shape):
    print("Input Shape = ", input_shape)
    input_layer = keras.layers.Input(input_shape)

    conv1 = keras.layers.Conv1D(filters=32, kernel_size=6, padding="same", name='l1')(input_layer)
    #conv1 = keras.layers.BatchNormalization()(conv1)
    conv1 = keras.layers.Activation('relu')(conv1)
    conv1_mp = keras.layers.AveragePooling1D(pool_size=2, strides=1, padding='same', name='l1_mp')(conv1)
    
    conv2 = keras.layers.Conv1D(filters=32, kernel_size=6, padding="same", name='l2')(input_layer)
    #conv2 = keras.layers.BatchNormalization()(conv2)
    conv2 = keras.layers.Activation('relu')(conv2)
    conv2_mp = keras.layers.AveragePooling1D(pool_size=2, strides=1, padding='same', name='l2_mp')(conv2)

    conv3 = keras.layers.Conv1D(filters=32, kernel_size=6, padding="same", name='l3')(input_layer)
    #conv3 = keras.layers.BatchNormalization()(conv3)
    conv3 = keras.layers.Activation('relu')(conv3)
    conv3_mp = keras.layers.AveragePooling1D(pool_size=2, strides=1, padding='same', name='l3_mp')(conv3)
    
    x = keras.layers.Add()([conv1_mp, conv2_mp, conv3_mp])
    x = keras.layers.Activation('relu')(x)
    
    gap = keras.layers.GlobalAveragePooling1D()(x)
    
    x = keras.layers.Dense(32)(gap)
    x = keras.layers.LeakyReLU()(x)
    x = keras.layers.Dense(32)(x)
    x = keras.layers.LeakyReLU()(x)
    output_layer = keras.layers.Dense(1, activation="tanh")(x) # TANH FOR REGRESSION OUTPUT

    return keras.models.Model(inputs=input_layer, outputs=output_layer)


# ## MODEL CNN SEQUENTIAL

# In[29]:


"""
    Prepare a Convolutional Neural Network in sequential 
"""
def make_model_CNN(input_shape, 
                   conv_layer=[(8,9),(8,6),(8,3)], 
                   mlp_layer=[70,70], 
                   out_fct='tanh',
                   pooling='GlobalAverage'):
    input_layer = keras.layers.Input(input_shape)
    
    constraint = CenterAround(7)
    p_l = input_layer
    for layer in conv_layer:
        p_l = keras.layers.Conv1D(filters=layer[0], 
                                  kernel_size=layer[1], 
                                  padding="same", 
                                  kernel_initializer='he_uniform')(p_l)
        p_l = keras.layers.Activation('relu')(p_l)
        #p_l = keras.layers.AveragePooling1D(pool_size=2, strides=2)(p_l)
    if(pooling == 'Max'):
        p_l = keras.layers.MaxPooling1D()(p_l)
    elif(pooling == 'Average'):
        p_l = keras.layers.AveragePooling1D()(p_l)
    elif(pooling == 'GlobalMax'):
        p_l = keras.layers.GlobalMaxPooling1D()(p_l)
    else:
        p_l = keras.layers.GlobalAveragePooling1D()(p_l)
    for layer in mlp_layer:
        p_l = keras.layers.Dense(layer)(p_l)
        p_l = keras.layers.Activation('relu')(p_l)
    p_l = keras.layers.Dense(1)(p_l)
    if out_fct == 'linear':
        output_layer = p_l
    else :
        output_layer = keras.layers.Activation(out_fct)(p_l) # TANH FOR REGRESSION OUTPUT
    return keras.models.Model(inputs=input_layer, outputs=output_layer)


# ## MODEL CNN SPEAKER (KERAS EXAMPLE)

# In[30]:


"""
    Prepare the model with Keras according to the Speaker example from keras.io
"""
def residual_block(x, filters, conv_num=3, activation="relu"):
    # Shortcut
    s = keras.layers.Conv1D(filters, 1, padding="same")(x)
    for i in range(conv_num - 1):
        x = keras.layers.Conv1D(filters, 3, padding="same")(x)
        x = keras.layers.Activation(activation)(x)
    x = keras.layers.Conv1D(filters, 3, padding="same")(x)
    x = keras.layers.Add()([x, s])
    x = keras.layers.Activation(activation)(x)
    return keras.layers.MaxPool1D(pool_size=2, strides=2)(x)

def make_model_CNN_speak(input_shape, num_classes):
    inputs = keras.layers.Input(shape=input_shape, name="input")

    x = residual_block(inputs, 16, 2)
    x = residual_block(x, 32, 2)
    x = residual_block(x, 32, 3)
    x = residual_block(x, 32, 3)
    x = residual_block(x, 32, 3)

    x = keras.layers.GlobalAveragePooling1D()(x)
    x = keras.layers.Flatten()(x)
    x = keras.layers.Dense(256, activation="relu")(x)
    x = keras.layers.Dense(128, activation="relu")(x)

    outputs = keras.layers.Dense(1, activation="tanh", name="output")(x)

    return keras.models.Model(inputs=inputs, outputs=outputs)


# ## TRAIN NN

# In[32]:


# Select the model's type
MODEL_SELECTED = 'CNN' # 'DNN', 'CNN', 'CNN_PARA', 'CNN_SPEAKER'
# Select the features
file = 'C:/Temp/MeasHydroDone/Features/feat_fft_20_150.csv'
#file = 'C:/Temp/MeasHydroDone/Features/feat_fft_50_150_only2.csv'
#file = 'C:/Temp/MeasHydroDone/VarPompe/Features/feat_fft_70_200.csv'
#file = 'C:/Temp/MeasHydroDone/VarPompe/Features/feat_vib_fft_abs.csv'
''' Split of the features and the labels '''
dataset = pd.read_csv(file)
dataset = dataset.drop(dataset.columns[0], axis=1)
train_X, train_y, test_X, test_y = split_dataset_fix(dataset, testsize=0.2)
# Print features
for i in range(len(train_X)):
    plt.plot(train_X[i])
plt.show()
# Select max/min
train_X_n, test_X_n = train_X, test_X
max_train = train_y.max()
min_train = train_y.min()
# Arrange Y labels between 0 and 1
train_y_n = (train_y-min_train) / (max_train - min_train)
test_y_n = (test_y-min_train) / (max_train - min_train)

''' Reshape the features to match NN input shapes'''
if(MODEL_SELECTED != 'DNN'):    # NO RESHAPE WITH DNN MODEL
    train_X_n = train_X.reshape((train_X.shape[0],train_X.shape[1], 1))
    test_X_n = test_X.reshape((test_X.shape[0], test_X.shape[1], 1))
    num_classes = len(np.unique(train_y_n))
    
''' Choose nb of epochs and batch size using to train the NN '''
epochs = 1000
batch_size = 70

''' Define callbacks which should be called during the training '''
callbacks = [
    #keras.callbacks.ModelCheckpoint("../best_model.h5", save_best_only=True, monitor="val_loss"),
    keras.callbacks.ReduceLROnPlateau(monitor="val_loss", factor=0.5, patience=20, min_lr=0.0001),
    keras.callbacks.EarlyStopping(monitor="val_loss", patience=100, verbose=1),
]

''' Prepare the model '''
if(MODEL_SELECTED == 'DNN'):
    hidden_layer = [(128,'relu'), (84,'relu'), (32,'relu')]
    model = make_model_DNN(train_X_n.shape[1:], hidden_layer)
elif(MODEL_SELECTED == 'CNN'):
    convolution =  [(32, 9), (32, 6), (32, 3)]#[(32,6),(32,6),(32,6)] #[(8,9),(8,6),(8,3)]
    mlp = [32,32]
    fct='tanh'
    pooling= 'GlobalAverage'  # 'Max', 'Average', 'GlobalMax', 'GlobalAverage'
    model = make_model_CNN(train_X_n.shape[1:], convolution, mlp, fct, pooling)
elif(MODEL_SELECTED == 'CNN_PARA'):
    model = make_model_CNN_para(train_X_n.shape[1:])
elif(MODEL_SELECTED == 'CNN_SPEAKER'):
    model = make_model_CNN_speak(train_X_n.shape[1:], 1)

model.summary()
keras.utils.plot_model(model, show_shapes=True)
model.compile(optimizer=keras.optimizers.RMSprop(), 
              loss="mean_squared_error", 
              metrics=[keras.losses.MeanSquaredError()])
''' Train the model '''
history = model.fit(
    train_X_n,
    train_y_n,
    batch_size=batch_size,
    epochs=epochs,
    callbacks=callbacks,
    validation_split=0.3,
    #validation_data=(test_X_n, test_y_n),
    verbose=0
)
display_loss(history)

''' Evaluate the model and plot results'''
test_results = model.evaluate(test_X_n, test_y_n, verbose=2)
print("Test Results : ", test_results)

pred = model.predict(test_X_n).flatten()
pred = pred * (max_train - min_train) + min_train
# print("Pred : ", pred)
# print("Test_y : ", train_y)
mse = keras.losses.MeanSquaredError()
print("MSE : ",mse(test_y, pred).numpy())
plotTestPred(test_y, pred)


# In[20]:


# Save the model if good enough
model.save('C:\Temp\k2arm\models\cnn_model_tanh_large.h5') 


# ## PSEUDO GRID SEARCH WITH CNN

# In[35]:


''' Normalisation of the features and the labels '''
file = 'C:/Temp/MeasHydroDone/Features/feat_fft_20_150.csv'
dataset = pd.read_csv(file)
dataset = dataset.drop(dataset.columns[0], axis=1)
train_X, train_y, test_X, test_y = split_dataset_fix(dataset, testsize=0.2)

#train_X_n, test_X_n = scale_features(train_X, test_X)
train_X_n, test_X_n = train_X, test_X
max_train = train_y.max()
min_train = train_y.min()
train_y_n = (train_y-min_train) / (max_train - min_train)
test_y_n = (test_y-min_train) / (max_train - min_train)

''' Reshape the features to match NN input shapes'''
train_X_n = train_X.reshape((train_X_n.shape[0],train_X_n.shape[1], 1))
test_X_n = test_X.reshape((test_X_n.shape[0], test_X_n.shape[1], 1))
num_classes = len(np.unique(train_y_n))

''' Choose nb of epochs and batch size using to train the NN '''
epochs = 1000
batch_size = 70

''' Define callbacks which should be called during the training '''
callbacks = [
    #keras.callbacks.ModelCheckpoint("../best_model.h5", save_best_only=True, monitor="val_loss"),
    keras.callbacks.ReduceLROnPlateau(monitor="val_loss", factor=0.5, patience=20, min_lr=0.0001),
    keras.callbacks.EarlyStopping(monitor="val_loss", patience=100, verbose=0),
]

''' Prepare combination '''
conv = [[(32,3),(32,3),(32,3)],
        [(32,6),(32,6),(32,6)],
        [(32,9),(32,9),(32,9)],
        [(32,9),(32,6),(32,3)],
        [(32,3),(32,6),(32,9)]]
# conv = [[(3,6),(3,6),(3,6)],
#         [(6,6),(6,6),(6,6)],
#         [(9,6),(9,6),(9,6)],
#         [(3,3),(3,6),(3,9)],
#         [(6,3),(6,6),(6,9)],
#         [(9,3),(9,6),(9,9)],]
mlp = [[16,16],
       [32,32],
       [64,64],
       [64,32],
       [32,16],
       [16],
       [32],
       [64]]
fct = ['tanh']
pool = ['GlobalAverage'] 

grid = pd.DataFrame(list(product(conv, mlp, fct, pool)), columns=['Conv', 'MLP', 'Fct', 'Pool'])
print("Shape = ", grid.shape)

''' Search the best result'''
best_diff = []
best_error = 100
best_model = grid.loc[0]
for index, row in grid.iterrows():
    print("Model : ")
    print("    Convolution : ", row['Conv'])
    print("    MLP         : ", row['MLP'])
    print("    Fct         : ", row['Fct'])
    print("    Pool        : ", row['Pool'])
    print()
    ''' Prepare the model'''
    model = make_model_CNN(train_X_n.shape[1:], row['Conv'], row['MLP'], row['Fct'], row['Pool'])
    model.compile(optimizer=keras.optimizers.RMSprop(), 
                  loss="mean_squared_error", 
                  metrics=[keras.losses.MeanSquaredError()])
    ''' Train the model '''
    history = model.fit(
        train_X_n,
        train_y_n,
        batch_size=batch_size,
        epochs=epochs,
        callbacks=callbacks,
        validation_split=0.3,
        verbose=0,
        use_multiprocessing=True,
    )
    ''' Evaluate the model and plot results'''
    test_results = model.evaluate(test_X_n, test_y_n,verbose=0)
    print("Test Results : ", test_results)
    pred = model.predict(test_X_n).flatten()
    pred = pred * (max_train - min_train) + min_train
    mse = keras.losses.MeanSquaredError()
    res_mse = mse(test_y, pred).numpy()
    print("MSE : ",res_mse)
    plotTestPred(test_y, pred)
    print()
    print()
    if(res_mse < best_error):
        best_error = res_mse
        best_diff = diff
        best_model = row

''' Display Final Result'''
print("FINAL RESULT : ")
print("Best Error : ", best_error)
print("Best Diff  : ", diff)
print("Best Model :")
print("    Convolution : ", best_model['Conv'])
print("    MLP         : ", best_model['MLP'])
print("    Fct         : ", best_model['Fct'])
print("    Pooling     : ", best_model['Pool'])


# ## BEST RESULTS

# In[46]:


'''
    Display the whole dataset's prediction
'''
model_ref_path = 'C:\Temp\k2arm\models\cnn_model_tanh_large.h5'
model_ref = keras.models.load_model(model_ref_path, compile=False)
model_ref.summary()
file = 'C:/Temp/MeasHydroDone/Features/feat_fft_20_150.csv'
dataset = pd.read_csv(file)
dataset = dataset.drop(dataset.columns[0], axis=1)
y_data = dataset['FlowRate'].astype(float).to_numpy()
x_data = dataset.drop(['FlowRate'], axis=1).to_numpy()
max_train = y_data.max()
min_train = y_data.min()
x_data_n = x_data.reshape((x_data.shape[0],x_data.shape[1], 1))
y_data_n = (y_data-min_train) / (max_train - min_train)

pred = model_ref.predict(x_data_n).flatten()
pred = pred * (max_train - min_train) + min_train
mse = keras.losses.MeanSquaredError()
res_mse = mse(y_data, pred).numpy()
print("MSE : ",res_mse)
plotTestPred(y_data, pred)

