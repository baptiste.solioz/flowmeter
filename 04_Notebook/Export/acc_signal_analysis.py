#!/usr/bin/env python
# coding: utf-8

# # Accelerometer Signal Analysis

# In[1]:


# Import
import numpy as np
from scipy import signal
import matplotlib.pyplot as plt
from scipy.signal import find_peaks
import pandas as pd
import os
import csv
from python_speech_features import mfcc
from python_speech_features import logfbank
plt.style.use('dark_background')


# In[2]:


# Get the root path of the project
ROOT_DIR = os.path.relpath("C:/Temp/MeasureAccDone")
os.chdir(ROOT_DIR)
print(os.getcwd())


# In[3]:


'''
Function : getDataFromFile
Param    : path to file
Return   : data_np -> Numpy array with all data (3 acc x 3 axis)
           hpfEnable -> return if the High Pass Filter was enabled during the measure
           FlowRate -> return the flow rate measured during the test
'''
def getDataFromFile(path):
    data = pd.read_csv(path, index_col=False , sep=';', skiprows=1, header=None)
    data_np = np.array(data, np.float64)

    with open(path) as csvFile:
        reader = csv.reader(csvFile, delimiter=';')
        field_names_list = next(reader)
        #print(field_names_list)
        hpfEnable = False
        if field_names_list[6] == 'HPF_Enable' :
            hpfEnable = True
        flowRate = field_names_list[10]
    
    return data_np, hpfEnable, flowRate


# In[4]:


'''
Function : plotDataset
Param    : Dataset to display
Return   : None
'''
def plotDataset(dataset):
    for i in range (1,dataset.shape[1]) :
        plot_acc1 = dataset.loc['Acc1']
        plot_acc2 = dataset.loc['Acc2']
        plot_acc3 = dataset.loc['Acc3']
        column = dataset.columns.values[i]
        plt.plot(plot_acc1['FlowRate'], plot_acc1[column], 'ro',
                 plot_acc2['FlowRate'], plot_acc2[column], 'bs',
                 plot_acc3['FlowRate'], plot_acc3[column], 'g^')
        plt.legend(['Acc1', 'Acc2', 'Acc3'])
        plt.title(dataset.columns.values[i])
        plt.xlabel('FlowRate', fontsize=15)
        plt.xticks(rotation=90)
        plt.ylabel('Value', fontsize=15)
        plt.grid(b=True, color='DarkTurquoise', alpha=0.2, linestyle=':', linewidth=2)
        plt.show()


# In[5]:


'''
Function : plotHandledSignal
Param    : path -> path to the file
           pStd -> plot standard data
           pLow -> plot low-filtered data
           pFFT -> plot FFT data
           pHigh -> plot high-filterd data
           axis -> Select axis 'xyz'
           freq -> Cut-off frequency
           winSize -> number of sample by window
           winNb -> number of window
           peak -> true if the peaks must be calculated
Return   : None
'''
def plotHandledSignal(path, 
                      pStd=False, pLow=False, pFFT=False, pHigh=False, 
                      axis='xyz', freq=10, winSize=800, winNb=1, peak=True):
    # Retrieve data from file
    data_np, hpfEnable, flowRate = getDataFromFile(path)    
    print("FlowRate = ", flowRate)
    
    # Select the Axis to handle
    if axis == 'x' : 
        acc1 = data_np[0,:]
        acc2 = data_np[3,:]
        acc3 = data_np[6,:]
    elif axis == 'y' : 
        acc1 = data_np[1,:]
        acc2 = data_np[4,:]
        acc3 = data_np[7,:]
    elif axis == 'z' :
        acc1 = data_np[2,:]
        acc2 = data_np[5,:]
        acc3 = data_np[8,:]
    elif axis == 'xy' :
        acc1 = np.sqrt(np.power(data_np[0,:],2)+np.power(data_np[1,:],2))
        acc2 = np.sqrt(np.power(data_np[3,:],2)+np.power(data_np[4,:],2))
        acc3 = np.sqrt(np.power(data_np[6,:],2)+np.power(data_np[7,:],2))
    elif axis == 'yz' :
        acc1 = np.sqrt(np.power(data_np[1,:],2)+np.power(data_np[2,:],2))
        acc2 = np.sqrt(np.power(data_np[4,:],2)+np.power(data_np[5,:],2))
        acc3 = np.sqrt(np.power(data_np[7,:],2)+np.power(data_np[8,:],2))
    elif axis == 'xz' :
        acc1 = np.sqrt(np.power(data_np[0,:],2)+np.power(data_np[2,:],2))
        acc2 = np.sqrt(np.power(data_np[3,:],2)+np.power(data_np[5,:],2))
        acc3 = np.sqrt(np.power(data_np[6,:],2)+np.power(data_np[8,:],2))
    else : 
        acc1 = np.sqrt(np.power(data_np[0,:],2)+np.power(data_np[1,:],2)+np.power(data_np[2,:],2))
        acc2 = np.sqrt(np.power(data_np[3,:],2)+np.power(data_np[4,:],2)+np.power(data_np[5,:],2))
        acc3 = np.sqrt(np.power(data_np[6,:],2)+np.power(data_np[7,:],2)+np.power(data_np[8,:],2))
    
    # Display only a short window of data
    startPt = int(winSize * winNb)
    stopPt = int(winSize * winNb + winSize)

    # Perform a butterworth high-pass filter on the data
    b, a = signal.butter(8, freq, fs=800, btype='highpass')
    zi = signal.lfilter_zi(b, a)
    acc1_high = signal.lfilter(b, a, acc1, zi=None)
    acc2_high = signal.lfilter(b, a, acc2, zi=None)
    acc3_high = signal.lfilter(b, a, acc3, zi=None)
    
    # Perform a butterworth low-pass filter on the data
    b, a = signal.butter(8, freq, fs=800, btype='lowpass')
    #b, a = signal.cheby1(8, 3,freq, fs=800, btype='lowpass')
    #b, a = signal.bessel(8, freq, fs=800, btype='lowpass')
    #b, a = signal.ellip(10, 1, 1,freq, fs=800, btype='lowpass')
    zi = signal.lfilter_zi(b, a)
    acc1_low = signal.lfilter(b, a, acc1, zi=None)
    acc2_low = signal.lfilter(b, a, acc2, zi=None)
    acc3_low = signal.lfilter(b, a, acc3, zi=None) 
    
    # Frequency informations
    acc1_fft = np.fft.rfft(acc1)
    acc2_fft = np.fft.rfft(acc2)
    acc3_fft = np.fft.rfft(acc3)
    # Compute the module of complexes numbers
    acc1_Norm = np.sqrt(np.power(acc1_fft.real,2)+np.power(acc1_fft.imag,2))
    acc2_Norm = np.sqrt(np.power(acc2_fft.real,2)+np.power(acc2_fft.imag,2))
    acc3_Norm = np.sqrt(np.power(acc3_fft.real,2)+np.power(acc3_fft.imag,2))
    acc1_Freq = np.fft.rfftfreq(len(acc1),d=0.00125)
    acc2_Freq = np.fft.rfftfreq(len(acc2),d=0.00125)
    acc3_Freq = np.fft.rfftfreq(len(acc3),d=0.00125)
    acc1_Freq = acc1_Freq[acc1_Freq <= 400]
    acc2_Freq = acc2_Freq[acc2_Freq <= 400]
    acc3_Freq = acc3_Freq[acc3_Freq <= 400]
    acc1_Norm = acc1_Norm[:len(acc1_Freq)-1]
    acc2_Norm = acc2_Norm[:len(acc2_Freq)-1]
    acc3_Norm = acc3_Norm[:len(acc3_Freq)-1]
    
    condition = (acc1_Freq <= 200) & (acc1_Freq > 10)
    # Filter data which previous condition
    acc1_ess = acc1_Norm[np.where(condition)]
    acc2_ess = acc2_Norm[np.where(condition)]
    acc3_ess = acc3_Norm[np.where(condition)]
    
    acc1_ess_f = acc1_Freq[np.where(condition)]
    acc2_ess_f = acc2_Freq[np.where(condition)]
    acc3_ess_f = acc3_Freq[np.where(condition)]

    print("Max Norm 1 = ", acc1_ess.max(), "at freq ", acc1_ess_f[acc1_ess.argmax()])
    print("Max Norm 2 = ", acc2_ess.max(), "at freq ", acc2_ess_f[acc2_ess.argmax()])
    print("Max Norm 3 = ", acc3_ess.max(), "at freq ", acc3_ess_f[acc3_ess.argmax()])
    
    fig, (acc1_x, acc1_y, acc1_z) = plt.subplots(3, 1)
    fig.suptitle("FFT Filtered")
    acc1_x.plot(acc1_ess_f, acc1_ess)
    acc1_y.plot(acc2_ess_f, acc2_ess)
    acc1_z.plot(acc3_ess_f, acc3_ess)
    plt.show()
    
    # Find Peaks on the signal
    peak_feat = pd.DataFrame()    
    if peak:
        peak1, _ = find_peaks(acc1_low[startPt:stopPt], distance=1)
        peak2, _ = find_peaks(acc2_low[startPt:stopPt], distance=1)
        peak3, _ = find_peaks(acc3_low[startPt:stopPt], distance=1)
        diff1 = np.diff(peak1)
        diff2 = np.diff(peak2)
        diff3 = np.diff(peak3)
        diffAcc12 = []
        diffAcc23 = []
        for val in peak1[:len(peak1)-3] :
            iPeak2 = peak2[peak2 > val][0]
            iPeak3 = peak3[peak3 > iPeak2][0]
            diffAcc12 = np.append(diffAcc12, [iPeak2-val])
            diffAcc23 = np.append(diffAcc23, [iPeak3-iPeak2])
        print("peak1 : ", peak1)
        print("peak2 : ", peak2)
        print("peak3 : ", peak3)
        
        print("diffAcc12 : ", diffAcc12)
        print("diffAcc23 : ", diffAcc23)
    
        # Extract features from peaks and diff between peaks
        peak_feat['FlowRate'] = [flowRate,flowRate, flowRate]
        #peak_feat['DiffMin'] = [diff1.min(), diff2.min(), diff3.min()] 
        #peak_feat['DiffMax'] = [diff1.max(), diff2.max(), diff3.max()] 
        peak_feat['DiffMean'] = [diff1.mean(), diff2.mean(), diff3.mean()] 
        peak_feat['DiffMedian'] = [np.median(diff1), np.median(diff2), np.median(diff3)] 
        peak_feat['DiffRMS'] = [np.sqrt(sum(pow(diff1, 2)) / len(diff1)),
                                np.sqrt(sum(pow(diff2, 2)) / len(diff2)),
                                np.sqrt(sum(pow(diff3, 2)) / len(diff3))]
        peak_feat['DiffStdDev'] = [np.sqrt(sum(pow(diff1 - diff1.mean(), 2)) / len(diff1)),
                                    np.sqrt(sum(pow(diff2 - diff2.mean(), 2)) / len(diff2)),
                                    np.sqrt(sum(pow(diff3 - diff3.mean(), 2)) / len(diff3))]
        peak_feat['PeakMean'] = [peak1.mean(), peak2.mean(), peak3.mean()] 
        peak_feat['PeakMedian'] = [np.median(peak1), np.median(peak2), np.median(peak3)] 
        peak_feat['PeakRMS'] = [np.sqrt(sum(pow(peak1, 2)) / len(peak1)),
                                np.sqrt(sum(pow(peak2, 2)) / len(peak2)),
                                np.sqrt(sum(pow(peak3, 2)) / len(peak3))]
        peak_feat['PeakStdDev'] = [np.sqrt(sum(pow(peak1 - peak1.mean(), 2)) / len(peak1)),
                                    np.sqrt(sum(pow(peak2 - peak2.mean(), 2)) / len(peak2)),
                                    np.sqrt(sum(pow(peak3 - peak3.mean(), 2)) / len(peak3))]
        peak_feat['DiffAccMean'] = [diffAcc12.mean(), diffAcc23.mean(), (diffAcc12-diffAcc23).mean()]
        peak_feat['DiffAccMedian'] = [np.median(diffAcc12), np.median(diffAcc23), np.median(diffAcc12-diffAcc23)]
        peak_feat['DiffAccRMS'] =  [np.sqrt(sum(pow(diffAcc12, 2)) / len(diffAcc12)),
                                    np.sqrt(sum(pow(diffAcc23, 2)) / len(diffAcc23)),
                                    np.sqrt(sum(pow(peak3, 2)) / len(peak3))]
        peak_feat['DiffAccStdDev'] = [np.sqrt(sum(pow(diffAcc12 - diffAcc12.mean(), 2)) / len(diffAcc12)),
                                      np.sqrt(sum(pow(diffAcc23 - diffAcc23.mean(), 2)) / len(diffAcc23)),
                                      np.sqrt(sum(pow((diffAcc12-diffAcc23) - (diffAcc12-diffAcc23).mean(), 2)) / len((diffAcc12-diffAcc23)))]

        peak_feat.rename(index={0:'Acc1'}, inplace=True)
        peak_feat.rename(index={1:'Acc2'}, inplace=True)
        peak_feat.rename(index={2:'Acc3'}, inplace=True)
    
    # Display standard data
    if pStd :
        fig, (acc1_x, acc1_y, acc1_z) = plt.subplots(3, 1)
        fig.suptitle("Standard")
        acc1_x.plot(acc1[startPt:stopPt])
        acc1_y.plot(acc2[startPt:stopPt])
        acc1_z.plot(acc3[startPt:stopPt])
        plt.show()
    # Display Filtered signals
    if pHigh :
        fig, (acc1_plt, acc2_plt, acc3_plt) = plt.subplots(3, 1)
        fig.suptitle("High-Filtered Signal")
        acc1_plt.plot(acc1_high[startPt:stopPt])
        acc2_plt.plot(acc2_high[startPt:stopPt])
        acc3_plt.plot(acc3_high[startPt:stopPt])
        plt.show()
    # Display Filtered signals
    if pLow :
        fig, (acc1_plt, acc2_plt, acc3_plt) = plt.subplots(3, 1)
        fig.suptitle("LowFiltered Signal")
        acc1_plt.plot(acc1_low[startPt:stopPt])
        acc2_plt.plot(acc2_low[startPt:stopPt])
        acc3_plt.plot(acc3_low[startPt:stopPt])
        plt.show()
    # Display FFT data
    if pFFT :
        fig, (acc1_x, acc1_y, acc1_z) = plt.subplots(3, 1)
        fig.suptitle("FFT")
        acc1_x.plot(acc1_Freq[1:len(acc1_Norm)], acc1_Norm[1:])
        acc1_y.plot(acc2_Freq[1:len(acc1_Norm)], acc2_Norm[1:])
        acc1_z.plot(acc3_Freq[1:len(acc1_Norm)], acc3_Norm[1:])
        plt.show()
    
    return peak_feat, hpfEnable


# ### Show Signals

# In[14]:


directory = "Meas_Outdoor_200924/Measure_1/HPF_Disable/"
list_meas_1 = os.listdir(directory)
all_peaks = pd.DataFrame()
for file in list_meas_1:
    peak_feat, hpf = plotHandledSignal(directory+file, 
                                       pStd=False,                       # Display Standard signal
                                       pLow=True,                       # Display Low-Pass filtered signal
                                       pFFT=False,                       # Display FFT Signal
                                       axis='xyz',                      # Choose axis to display
                                       freq=40,                         # Cut-off frequency LPF
                                       winSize=500,                      # Nb of samples to show
                                       winNb=2,                         # Offset "winSize" for the samples
                                       peak=True)                      # Show Peaks informations
    all_peaks = all_peaks.append(peak_feat)

plotDataset(all_peaks)

