#!/usr/bin/env python
# coding: utf-8

# # USS Visualization from Ultrasonic sensor

# In[20]:


from hmmlearn import hmm
import librosa
from librosa import load
from librosa import display
import numpy as np
import pandas as pd
import csv
import sklearn
import os
import matplotlib.pyplot as plt
import warnings
plt.style.use('dark_background')


# In[4]:


ROOT_DIR = os.path.relpath("C:\\git\\flowrate\\07_Measure\\USS_Capture_201001")
os.chdir(ROOT_DIR)


# In[26]:


'''
Function : getDataFromFile
Param    : path to file
Return   : data_np -> Numpy array with all data (3 acc x 3 axis)
           hpfEnable -> return if the High Pass Filter was enabled during the measure
           FlowRate -> return the flow rate measured during the test
'''
def getDataFromFile(path):
    data = pd.read_csv(path, index_col=False , sep=',', header=0)
    print(data.columns.values)
    data_np = np.array(data, np.float64)
    return data_np


# In[27]:


'''
    Plot Data UPS VS DNS
'''
def plotData(data_np):
    plt.plot(data_np[50:,1], data_np[50:,2], 'r',
             data_np[50:,4], data_np[50:,5], 'b')
    plt.legend(['UPS', 'DNS'])
    plt.xlabel('Index', fontsize=15)
    plt.xticks(rotation=90)
    plt.ylabel('Value', fontsize=15)
    plt.grid(b=True, color='DarkTurquoise', alpha=0.2, linestyle=':', linewidth=2)
    plt.show()


# In[29]:


# Plot data "Tete beche"
directory = "Beche/"
list_meas_1 = os.listdir(directory)
for file in list_meas_1:
    data_np = getDataFromFile(directory+file)
    plotData(data_np)


# In[30]:


# Plot data Doppler mode
directory = "Doppler/"
list_meas_1 = os.listdir(directory)
for file in list_meas_1:
    data_np = getDataFromFile(directory+file)
    plotData(data_np)

