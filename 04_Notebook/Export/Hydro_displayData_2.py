#!/usr/bin/env python
# coding: utf-8

# # Get Hydro Data from lab

# In[1]:


import numpy as np
import pandas as pd
import os
import csv
import matplotlib.pyplot as plt
from scipy.signal import find_peaks
plt.style.use('dark_background')
plt.rcParams['figure.figsize'] = [20, 5]


# In[2]:


'''
    Get Data from file
'''
def getDataFromFile(path, d_type):
    data = pd.read_csv(path, index_col=False , sep=';', skiprows=0, header=None, decimal=',', nrows=1)
    flowRate = float(data.iloc[0,1])
    data = []
    if(d_type == 'ADC1'):
        data = pd.read_csv(path, index_col=False , sep=';', skiprows=1, header=None, decimal=',', nrows=1)
    elif(d_type == 'ADC2'):
        data = pd.read_csv(path, index_col=False , sep=';', skiprows=2, header=None, decimal=',', nrows=1)
    elif(d_type == 'ADC3'):
        data = pd.read_csv(path, index_col=False , sep=';', skiprows=3, header=None, decimal=',', nrows=1)
    elif(d_type == 'ADC4'):
        data = pd.read_csv(path, index_col=False , sep=';', skiprows=4, header=None, decimal=',', nrows=1)
    elif(d_type == 'ACC1'):
        data = pd.read_csv(path, index_col=False , sep=';', skiprows=6, header=None, decimal=',', nrows=3)
    elif(d_type == 'ACC2'):
        data = pd.read_csv(path, index_col=False , sep=';', skiprows=9, header=None, decimal=',', nrows=3)
    elif(d_type == 'ACC3'):
        data = pd.read_csv(path, index_col=False , sep=';', skiprows=12, header=None, decimal=',', nrows=3)
    elif(d_type == 'ACC4'):
        data = pd.read_csv(path, index_col=False , sep=';', skiprows=15, header=None, decimal=',', nrows=3)
    data_np = np.array(data, np.float64)
    return data_np, flowRate
'''
    Get a Window size of a signal by condition min/max
'''
def getWindow(sig_x, sig_y, min_x, max_x):
    condition = (sig_x < max_x) & (sig_x > min_x)
    trunc_y = sig_y[np.where(condition)]
    trunc_x = sig_x[np.where(condition)]
    return trunc_x, trunc_y


# In[3]:


'''
    Apply a FFT on a signal according to its frequency
'''
def applyFFT(data, freq, display='OFF', title='None'):
    fft = np.fft.rfft(data)
    fft_freq = np.fft.rfftfreq(len(data),d=1/freq)
    if(display == 'ON'):
        fig, (ax1, ax2) = plt.subplots(1, 2)
        fig.suptitle(title)
        ax1.plot(data)
        ax1.title.set_text('Time Domain')
        ax1.set_xlabel('Samples', fontsize=15)
        ax2.title.set_text('FFT')
        ax2.set_xlabel('Freq [Hz]', fontsize=15)
        ax2.plot(fft_freq[1:1000], fft.real[1:1000])
        fig.show()
    return fft_freq, fft.real


# In[8]:


'''
    Get Data from file
'''
input_directory = 'C:/Temp/MeasHydroDone/VarPompe_2_inv/'
list_files = os.listdir(input_directory)
for file in list_files:
    print(file)
    d_types = ['ACC1','ACC2', 'ACC3', 'ACC4']
    colors = ['g', 'r', 'y', 'b']
    ci = 0
    for dt in d_types:
        data_acc, flowRate = getDataFromFile(input_directory+file, d_type=dt)
        data_sqrt = np.sqrt(np.power(data_acc[0,:],2) + np.power(data_acc[1,:],2) + np.power(data_acc[2,:],2))
        fft_acc_x, fft_acc_y = applyFFT(data_sqrt, 800, 'OFF', 'ACC1')
        fft_x, fft_y = getWindow(fft_acc_x, fft_acc_y, 10, 200)
        plt.plot(fft_x, fft_y, colors[ci])
        ci = ci + 1
    label = pd.DataFrame()
    label['FlowRate'] = [flowRate]
    plt.title(flowRate)
    plt.show()


# ## Extract FFT as Feature

# In[14]:


# Give the min and max frequency
min_d = 20
max_d = 150
# Retrieve data from files
features = pd.DataFrame()
input_directory = 'C:/Temp/MeasHydroDone/VarPompe_2/'
list_files = os.listdir(input_directory)
# Extract first part of the measure
for file in list_files:
    d_types = ['ADC1','ADC2','ADC3','ADC4']
    for dt in d_types:
        data_adc, flowRate = getDataFromFile(input_directory+file, d_type=dt)
        data_adc = data_adc.flatten()
        sig_x = np.fft.rfftfreq(8192,d=1/10000)[0:len(data_adc)]
        fft_x, fft_y = getWindow(sig_x, data_adc, min_d, max_d)
        fft_feat = pd.DataFrame(data=[fft_y], columns=fft_x)
        label = pd.DataFrame()
        label['FlowRate'] = [flowRate]
        subject_features = pd.DataFrame()
        subject_features = pd.concat([label,fft_feat], axis=1, sort=False)
        features = features.append(subject_features)
input_directory = 'C:/Temp/MeasHydroDone/VarPompe_2_inv/'
list_files = os.listdir(input_directory)
# Extract second part of the measure
for file in list_files:
    d_types = ['ADC1','ADC2','ADC3','ADC4']
    for dt in d_types:
        data_adc, flowRate = getDataFromFile(input_directory+file, d_type=dt)
        data_adc = data_adc.flatten()
        sig_x = np.fft.rfftfreq(8192,d=1/10000)[0:len(data_adc)]
        fft_x, fft_y = getWindow(sig_x, data_adc, min_d, max_d)
        fft_feat = pd.DataFrame(data=[fft_y], columns=fft_x)
        label = pd.DataFrame()
        label['FlowRate'] = [flowRate]
        subject_features = pd.DataFrame()
        subject_features = pd.concat([label,fft_feat], axis=1, sort=False)
        features = features.append(subject_features)
# Remove null flow rate
features = features[features['FlowRate'] > 0]
features = features[features['FlowRate'] < 35]
print(features)


# In[9]:


# Save features
features.to_csv('C:/Temp/MeasHydroDone/Features/feat_fft_asdf.csv')


# ## Features extraction function

# In[7]:


'''
Function : getFeatures
Param    : dx, dy, dz -> numpy data corresponding to the 3 axis
Return   : list of features extracted + flowRate
'''
def getFeatures(data, segmentation_level=1, sig='std'):
    features_df = pd.DataFrame()
    chunk_size = int(data.shape[0] / segmentation_level) 
    segment_count = 1
    for start in range(0, data.shape[0], chunk_size):
        # Divide the driving session in segments when segmentation_lvl > 1
        feat = data[start:start + chunk_size]
        feat_len = len(feat)
        # Minimum
        features_df['s{}_'.format(segment_count) + sig+'_'+'Min'] = [feat.min()]
        # Maximum
        features_df['s{}_'.format(segment_count) + sig+'_'+'Max'] = [feat.max()]
        # Min-Max
        features_df['s{}_'.format(segment_count) + sig+'_'+'MinMax'] = [feat.max()-feat.min()]
        # Arithmetic Mean
        feat_am = sum(feat) / feat_len
        features_df['s{}_'.format(segment_count) + sig+'_'+'ArithMean'] = [feat_am]
        # Mean Absolute
        feat_ma = np.abs(sum(feat)) / feat_len
        features_df['s{}_'.format(segment_count) + sig+'_'+'MeanAbs'] = [feat_ma]
        # Root Mean Square
        feat_rms = np.sqrt(sum(pow(feat, 2)) / feat_len)
        features_df['s{}_'.format(segment_count) + sig+'_'+'RMS'] = [feat_rms]
        # Standard Deviation
        feat_sd = np.sqrt(sum(pow(feat - feat_am, 2)) / feat_len)
        features_df['s{}_'.format(segment_count) + sig+'_'+'StandDev'] = [feat_sd]
        # Variance
        feat_var = sum(pow(feat - feat_am, 2)) / ((feat_len - 1) * pow(feat_sd, 2))
        features_df['s{}_'.format(segment_count) + sig+'_'+'Variance'] = [feat_var]
        # Skewness
        feat_sn = sum((feat - feat_am) * 3 / ((feat_len - 1) * pow(feat_sd, 3)))
        features_df['s{}_'.format(segment_count) + sig+'_'+'Skewness'] = [feat_sn]
        # Kurthosis
        feat_kt = sum((feat - feat_am) * 4 / ((feat_len - 1) * pow(feat_sd, 4)))
        features_df['s{}_'.format(segment_count) + sig+'_'+'Kurthosis'] = [feat_kt]
        # Median
        feat_med = np.median(feat)
        features_df['s{}_'.format(segment_count) + sig+'_'+'Median'] = [feat_med]
        # Moment 3th degree
        feat_mo3 = sum(pow(feat, 3)) / feat_len
        features_df['s{}_'.format(segment_count) + sig+'_'+'Moment3'] = [feat_mo3]
        # Moment 4th degree
        feat_mo4 = sum(pow(feat, 4)) / feat_len
        features_df['s{}_'.format(segment_count) + sig+'_'+'Moment4'] = [feat_mo4]
        # Moment 5th degree
        feat_mo5 = sum(pow(feat, 5)) / feat_len
        features_df['s{}_'.format(segment_count) + sig+'_'+'Moment5'] = [feat_mo5]
        # Moment 6th degree
        feat_mo6 = sum(pow(feat, 6)) / feat_len
        features_df['s{}_'.format(segment_count) + sig+'_'+'Moment6'] = [feat_mo6]
        # Shape Factor
        feat_sf = feat_rms / feat_ma
        features_df['s{}_'.format(segment_count) + sig+'_'+'ShapeFactor'] = [feat_sf]
        
        segment_count += 1
        if segment_count > segmentation_level:
            break

    return features_df


# ## Extract Accelerometer data

# In[8]:


# Get the accelerometer data from lab's measurements
features = pd.DataFrame()
directories = ['C:/Temp/MeasHydroDone/VarPompe_2/',
                'C:/Temp/MeasHydroDone/VarPompe_2_inv/']
for directory in directories:
    list_files = os.listdir(directory)
    for file in list_files:
        d_types = ['ACC1', 'ACC2', 'ACC3', 'ACC4']
        for dt in d_types:
            data_adc, flowRate = getDataFromFile(directory+file, d_type=dt)
            acc_x = data_adc[0,:] - data_adc[0,:].mean()
            acc_y = data_adc[1,:] - data_adc[1,:].mean()
            acc_z = data_adc[2,:]# - data_adc[2,:].mean()
            data_fm_acc_all = np.sqrt(pow(acc_x,2) + pow(acc_y,2) + pow(acc_z,2))
            label = pd.DataFrame()
            label['FlowRate'] = [flowRate]
            feat_acc = getFeatures(acc_z, 1, sig='acc')
            subject_features = pd.concat([label,feat_acc], axis=1, sort=False)
            features = features.append(subject_features)
# Remove null flow rate
features = features[features['FlowRate'] > 0]
features = features[features['FlowRate'] < 35]
print(features)


# In[9]:


# Show accelerometer features
for i in features:
    plt.plot(features['FlowRate'], features[i], 'ro')
    plt.title(i)
    plt.show()


# ## Extract FFT Features

# In[15]:


# Give min/max frequency
min_d = 50
max_d = 150
# Extract data from files
features = pd.DataFrame()
directories = ['C:/Temp/MeasHydroDone/VarPompe_2/',
                'C:/Temp/MeasHydroDone/VarPompe_2_inv/']
# Extract features
for directory in directories:
    list_files = os.listdir(directory)
    for file in list_files:
        d_types = ['ADC1', 'ADC2', 'ADC3', 'ADC4']
        for dt in d_types:
            data_adc, flowRate = getDataFromFile(directory+file, d_type=dt)
            data_adc = data_adc.flatten()
            sig_x = np.fft.rfftfreq(8192,d=1/10000)[0:len(data_adc)]
            fft_x, fft_y = getWindow(sig_x, data_adc, min_d, max_d)
            label = pd.DataFrame()
            label['FlowRate'] = [flowRate]
            feat_adc = getFeatures(fft_y, 1, sig='fft')
            subject_features = pd.concat([label,feat_adc], axis=1, sort=False)
            features = features.append(subject_features)
# Remove null flow rate
features = features[features['FlowRate'] > 0]
features = features[features['FlowRate'] < 35]
print(features)


# In[16]:


# Show FFT Features
for i in features:
    plt.plot(features['FlowRate'], features[i], 'ro')
    plt.title(i)
    plt.show()


# In[17]:


# Export FFT Features
features.to_csv('C:/Temp/MeasHydroDone/Features/features_50_150.csv')

