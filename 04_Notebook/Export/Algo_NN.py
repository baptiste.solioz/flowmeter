#!/usr/bin/env python
# coding: utf-8

# # NEURAL NETWORK ALGO

# In[19]:


from tensorflow import keras
import numpy as np
import matplotlib.pyplot as plt
import os
import csv
import pandas as pd
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from itertools import product

import warnings
warnings.filterwarnings('ignore')

plt.style.use('dark_background')
plt.rcParams['figure.figsize'] = [15, 5]


# In[2]:


ROOT_DIR = os.path.relpath("C:\Temp\MeasHydroDone\VarPompe\Features")
os.chdir(ROOT_DIR)


# In[3]:


"""
    Split dataset into train/test (70/30)
"""
def split_dataset(dataset, testsize=0.1):
    train, test = train_test_split(dataset, test_size=testsize, random_state=35)
    train_labels = train['FlowRate'].astype(float).to_numpy()
    train_features = train.drop(['FlowRate'], axis=1)
    test_labels = test['FlowRate'].astype(float).to_numpy()
    test_features = test.drop(['FlowRate'], axis=1)

    print("Train data = ", len(train_features), " Test data = ", len(test_features))
    return train_features, train_labels, test_features, test_labels


# In[4]:


"""
    Scale the train and the test set
"""
def scale_features(train_features, test_features):
    scaler = StandardScaler()
    scaler.fit(train_features)
    train_features_n = scaler.transform(train_features)
    test_features_n = scaler.transform(test_features)
    return train_features_n, test_features_n


# In[5]:


"""
    PCA for dimentionaly reduction
"""
def reduce_features(train_features_n, test_features_n):
    pca = PCA(0.99)  # Variance of the features
    pca.fit(train_features_n)
    print(pca.n_components_)
    train_features_pca = pca.transform(train_features_n)
    test_features_pca = pca.transform(test_features_n)
    return train_features_pca, test_features_pca


# In[21]:


"""
    Prepare the model with Keras
"""
def make_model(input_shape, hidden_layer, fct_mlp='relu', fct_out='tanh'):
    input_layer = keras.layers.Input(input_shape)
    previous_layer = input_layer
    for layer in hidden_layer:
        next_layer = keras.layers.Dense(layer)(previous_layer)
        previous_layer = keras.layers.Activation(fct_mlp)(next_layer)
    output_layer = keras.layers.Dense(1)(previous_layer)
    output_layer = keras.layers.Activation(fct_out)(output_layer)
    return keras.models.Model(inputs=input_layer, outputs=output_layer)


# In[7]:


"""
    Display loss according to the keras history
"""
def display_loss(history):
    metric = "loss"
    plt.figure()
    plt.plot(history.history[metric])
    plt.plot(history.history["val_" + metric])
    plt.title("model " + metric)
    plt.ylabel(metric, fontsize="large")
    plt.xlabel("epoch", fontsize="large")
    plt.legend(["train", "val"], loc="best")
    plt.show()
    plt.close()


# In[15]:


"""
    Plot test prediction VS test labels
"""
def plotTestPred(test_labels, test_predictions):
    diff_pred = test_predictions - test_labels
    print("Diff Min = ", diff_pred.min(), " Max = ", diff_pred.max())
    a = plt.axes(aspect='equal')
    plt.scatter(test_labels, test_predictions)
    plt.xlabel('True Values [MPG]')
    plt.ylabel('Predictions [MPG]')
    lims = [9, 40]
    plt.xlim(lims)
    plt.ylim(lims)
    _ = plt.plot(lims, lims)
    plt.show()


# ### PSEUDO GRID SEARCH WITH NN

# In[23]:


# Select callbacks
callbacks = [
    # Reduce learning rate if needed
    keras.callbacks.ReduceLROnPlateau(monitor="val_loss", factor=0.5, patience=20, min_lr=0.0001),
    # Stop the learning if no improvements
    keras.callbacks.EarlyStopping(monitor="val_loss", patience=100, verbose=1),
]
# Select Epochs and batch size
epochs = 500
batch_size = 10

''' Prepare combination '''
mlp = [[5,5], [10,10], [5,10], [10,5], [7,5], [5,7], [5], [10], [7]]
fct_mlp = ['tanh','relu']
fct_out = ['tanh','relu','linear']

''' Create grid search '''
grid = pd.DataFrame(list(product(mlp, fct_mlp, fct_out)), columns=['MLP', 'Fct_MLP', 'Fct_Out'])    
# Initialize best results
best_mse = 100
best_file = ""
best_model = grid.loc[0]

# Pre-processing of the features
file = ['C:/Temp/MeasHydroDone/Features/features_50_150.csv']
dataset = pd.read_csv(file[0])
dataset = dataset.drop(dataset.columns[0], axis=1)
train_X, train_y, test_X, test_y = split_dataset(dataset, testsize=0.2)
train_X_n, test_X_n = scale_features(train_X, test_X)
train_X_r, test_X_r = reduce_features(train_X_n, test_X_n)
# Start pseudo grid search
for index, row in grid.iterrows():
    print("Model : ")
    print("    MLP         : ", row['MLP'])
    print("    Fct_MLP     : ", row['Fct_MLP'])
    print("    Fct_Out     : ", row['Fct_Out'])
    print()
    # Prepare the model with parameters
    model = make_model(train_X_r.shape[1:], row['MLP'], row['Fct_MLP'], row['Fct_Out'])
    keras.utils.plot_model(model, show_shapes=True)
    # Compile it with optimizer and loss function
    model.compile(optimizer=keras.optimizers.RMSprop(), 
                  loss="mean_squared_error", 
                  metrics=[keras.losses.MeanSquaredError()])
    # Train the system
    history = model.fit(
        train_X_r,
        train_y,
        batch_size=batch_size,
        epochs=epochs,
        callbacks=callbacks,
        validation_split=0.2,
        verbose=0
    )
    # Predict and check the best result
    pred = model.predict(test_X_r)
    mse = keras.losses.MeanSquaredError()
    res_mse = mse(test_y, pred.flatten()).numpy()
    print("MSE : ",res_mse)
    plotTestPred(test_y, pred.flatten())
    if(res_mse < best_mse):
        best_mse = res_mse
        best_file = file
        best_model = row


# In[27]:


# Print the best Results
print("Shape = ", grid.shape)
print("Best MSE = ", best_mse)
print("Best model = ", best_model)

