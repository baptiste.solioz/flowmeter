#!/usr/bin/env python
# coding: utf-8

# # LINEAR MACHINE LEARNING ALGO

# In[1]:


# Import
import numpy as np
from scipy import signal
import matplotlib.pyplot as plt
import pandas as pd
import os
import csv
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn.svm import SVC
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import classification_report
from sklearn.ensemble import RandomForestRegressor
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score
from sklearn.svm import SVR
from sklearn.metrics import mean_squared_error
from python_speech_features import mfcc
from python_speech_features import logfbank
from sklearn import neighbors
plt.style.use('dark_background')
plt.rcParams['figure.figsize'] = [15, 5]
import warnings
warnings.filterwarnings('ignore')


# # Get Data From File

# In[2]:


ROOT_DIR = os.path.relpath("C:\Temp\MeasHydroDone\VarPompe\Features_extract")
os.chdir(ROOT_DIR)


# # Split Dataset between Train and test set

# In[3]:


"""
    Split dataset into train/test (70/30)
"""
def split_dataset(dataset, testsize=0.1):
    train, test = train_test_split(dataset, test_size=testsize, random_state=35)
    train_labels = train['FlowRate'].astype(float).to_numpy()
    train_features = train.drop(['FlowRate'], axis=1).to_numpy()
    test_labels = test['FlowRate'].astype(float).to_numpy()
    test_features = test.drop(['FlowRate'], axis=1).to_numpy()

    print("Train data = ", len(train_features), " Test data = ", len(test_features))
    return train_features, train_labels, test_features, test_labels


# # Scale features

# In[4]:


"""
    Scale the train and the test set
"""
def scale_features(train_features, test_features):
    scaler = StandardScaler()
    scaler.fit(train_features)
    train_features_n = scaler.transform(train_features)
    test_features_n = scaler.transform(test_features)
    return train_features_n, test_features_n


# # PCA Dimentionaly Reduction

# In[5]:


"""
    PCA for dimentionaly reduction
"""
def reduce_features(train_features_n, test_features_n):
    pca = PCA(0.99)  # Variance of the features
    pca.fit(train_features_n)
    print(pca.n_components_)
    train_features_pca = pca.transform(train_features_n)
    test_features_pca = pca.transform(test_features_n)
    return train_features_pca, test_features_pca


# ## PREPARE DATA AND SPLIT TRAIN / FIXED TEST

# In[6]:


def split_dataset_fixed(dataset, testsize=0.1):
    dataset = dataset.sort_values(by=['FlowRate'])

    len_data = len(dataset)
    y_len = int(len_data * testsize)
    x_len = len_data - y_len
    print(y_len, x_len)

    inter_y = int(np.trunc(len_data / y_len))

    test_data = pd.DataFrame()
    train_data = dataset
    for x in range(1, len_data, inter_y):
        test_data = test_data.append(dataset.iloc[x,:])
        train_data = train_data.drop(x)

    train_y = train_data['FlowRate'].astype(float).to_numpy()
    train_X = train_data.drop(['FlowRate'], axis=1).to_numpy()
    test_y = test_data['FlowRate'].astype(float).to_numpy()
    test_X = test_data.drop(['FlowRate'], axis=1).to_numpy()

    idx = np.random.permutation(len(train_X))

    return train_X, train_y, test_X, test_y


# # GridSearch

# In[39]:


rfr_grid = {
    'bootstrap': [True],
    'max_depth': [50, 100, 150, 200],
    'max_features': [2, 3, 4, 5, 6, 7],
    'min_samples_leaf': [1, 2, 3, 4, 5],
    'min_samples_split': [4, 6, 8, 10, 12],
    'n_estimators': [100, 200, 300, 400, 500]
}
'''
svr_grid = [{'kernel': ['rbf'], 'tol': [0.1], 'gamma': [1], 'C': [0.001, 0.01, 0.1, 10, 100]},
            {'kernel': ['linear'], 'tol': [0.1], 'C': [10, 100, 1000]},
            {'kernel': ['poly'], 'tol': [0.1], 'gamma': [1], 'degree': [2, 3], 'C': [0.1, 10, 100, 1000]},
            {'kernel': ['sigmoid'], 'tol': [0.1], 'gamma': [1], 'C': [0.001, 0.01, 0.1, 10, 100]}]
'''
svr_grid = [{'kernel': ['rbf'], 'gamma': [1e-8, 1e-6,1e-4, 1e-2, 1, 1e2, 1e4], 'C': [0.001, 0.01, 0.1, 10, 100]},
            {'kernel': ['linear'], 'C': [0.0001, 0.001, 0.01, 0.1, 10, 100, 1000]},
            {'kernel': ['sigmoid'], 'tol': [0.1], 'gamma': [1], 'C': [0.001, 0.01, 0.1, 10, 100]}]

model = [RandomForestRegressor(), SVR()]
grid = [rfr_grid, svr_grid]
#model = [SVR()]
#grid = [svr_grid]

dataset = pd.read_csv('C:/Temp/MeasHydroDone/Features/features_50_150.csv')
dataset = dataset.drop(dataset.columns[0], axis=1)
train_X, train_y, test_X, test_y = split_dataset(dataset, testsize=0.2)
#train_X, train_y, test_X, test_y = split_dataset_fixed(dataset, testsize=0.2)

train_X_n, test_X_n = scale_features(train_X, test_X)
train_X_r, test_X_r = reduce_features(train_X_n, test_X_n)

for i in range(len(grid)):
    print("Start Model : ", model[i])
    grid_search = GridSearchCV(estimator = model[i], param_grid = grid[i], cv = 5, n_jobs = -1, verbose = 2)

    # Fit the grid search to the data
    grid_search.fit(train_X_r, train_y)
    print("Grid Search Best Param : ", grid_search.best_params_)
    # Print the results
    y_true, y_pred = test_y, grid_search.predict(test_X_r)
    print("Y True : ", np.array(y_true))
    print("Y Pred : ", y_pred)
    print("MSE : ", mean_squared_error(y_true, y_pred))
    plotTestPred(y_true, y_pred)


# ### Plot Test prediction VS Test labels

# In[27]:


def plotTestPred(test_labels, test_predictions):
    diff_pred = test_predictions - test_labels
    print("Diff Min = ", diff_pred.min(), " Max = ", diff_pred.max())
    a = plt.axes(aspect='equal')
    plt.scatter(test_labels, test_predictions)
    plt.xlabel('True Values [m3/h]')
    plt.ylabel('Predictions [m3/h]')
    lims = [9, 40]
    plt.xlim(lims)
    plt.ylim(lims)
    _ = plt.plot(lims, lims)
    plt.show()


# ### TEST RANDOM FOREST REGRESSOR

# In[28]:


dataset = pd.read_csv('C:/Temp/MeasHydroDone/Features/features_50_150.csv')
dataset = dataset.drop(dataset.columns[0], axis=1)
train_X, train_y, test_X, test_y = split_dataset(dataset, testsize=0.1)
# Pre-processing
train_X_n, test_X_n = scale_features(train_X, test_X)
train_X_r, test_X_r = reduce_features(train_X_n, test_X_n)
# Use best result with grid search
rfr = RandomForestRegressor(bootstrap=True, criterion='mse', max_depth=50,
                      max_features=7, max_leaf_nodes=None,
                      min_impurity_decrease=0.0, min_impurity_split=None,
                      min_samples_leaf=3, min_samples_split=12,
                      min_weight_fraction_leaf=0.0, n_estimators=100,
                      n_jobs=None, oob_score=False, random_state=None,
                      verbose=0, warm_start=False)
rfr.fit(train_X_r, train_y)
y_true, y_pred = test_y, rfr.predict(test_X_r)
plotTestPred(y_true, y_pred)


# ### Test Support Vector Regressor

# In[29]:


# Use Grid Search result
clf = SVR(kernel='rbf', gamma=0.01, C=100)
clf.fit(train_X_r, train_y)
y_true, y_pred = test_y, clf.predict(test_X_r)
plotTestPred(y_true, y_pred)


# ### Test RFR + SVR + KNN

# In[37]:


input_directory = "C:\Temp\MeasHydroDone\VarPompe\Features_extract"
list_files = os.listdir(input_directory)

rfr_grid = {
    'bootstrap': [True],
    'max_depth': [80, 90, 100, 110],
    'max_features': [2, 3, 4, 5],
    'min_samples_leaf': [3, 4, 5],
    'min_samples_split': [8, 10, 12],
    'n_estimators': [100, 200, 300, 1000]
}
svr_grid = [{'kernel': ['rbf'], 'gamma': [1e-8, 1e-6,1e-4, 1e-2, 1, 1e2, 1e4], 'C': [0.001, 0.01, 0.1, 10, 100]},
            {'kernel': ['linear'], 'C': [0.0001, 0.001, 0.01, 0.1, 10, 100, 1000]},
            {'kernel': ['sigmoid'], 'tol': [0.1], 'gamma': [1], 'C': [0.001, 0.01, 0.1, 10, 100]}]
''' GRID SEARCH WITH RFR '''
def check_RFR(train_features_pca, train_labels, test_features_pca, test_labels):
    grid_search = GridSearchCV(estimator = RandomForestRegressor(), param_grid = rfr_grid, cv = 3, n_jobs = -1, verbose = 0)
    # Fit the grid search to the data
    grid_search.fit(train_features_pca, train_labels)
    print("Grid Search Best Param : ", grid_search.best_params_)
    # Print the results
    y_true, y_pred = test_labels, grid_search.predict(test_features_pca)
    print("Y True : ", np.array(y_true))
    print("Y Pred : ", y_pred)
    print("MSE : ", mean_squared_error(y_true, y_pred))
''' GRID SEARCH WITH SVR '''
def check_SVR(train_features_pca, train_labels, test_features_pca, test_labels):
    grid_search = GridSearchCV(estimator = SVR(), param_grid = svr_grid, cv = 3, n_jobs = -1, verbose = 1)
    # Fit the grid search to the data
    grid_search.fit(train_features_pca, train_labels)
    print("Grid Search Best Param : ", grid_search.best_params_)
    # Print the results
    y_true, y_pred = test_labels, grid_search.predict(test_features_pca)
    print("Y True : ", np.array(y_true))
    print("Y Pred : ", y_pred)
    print("MSE : ", mean_squared_error(y_true, y_pred))
    plotTestPred(y_true, y_pred)
''' PSEUDO GRID SEARCH WITH KNN '''
def check_knn(train_features_pca, train_labels, test_features_pca, test_labels):
    best_k = 0
    best_error = 100
    for K in range(20):
        K = K+1
        model = neighbors.KNeighborsRegressor(n_neighbors = K)

        model.fit(train_features_pca, train_labels)  #fit the model
        pred=model.predict(test_features_pca) #make prediction on test set
        error = mean_squared_error(test_labels,pred) #calculate mse
        if(error < best_error):
            best_error = error
            best_k = K
    return best_k, best_error

list_files = ['C:/Temp/MeasHydroDone/Features/features_50_150.csv']
''' Check all differents features previously extracted '''
for file in list_files:
    print(file)
    dataset = pd.read_csv(file)
    dataset = dataset.drop(dataset.columns[0], axis=1)
    train_X, train_y, test_X, test_y = split_dataset(dataset, testsize=0.2)
    #train_X, train_y, test_X, test_y = split_dataset_fixed(dataset, testsize=0.1)
    
    train_X_n, test_X_n = scale_features(train_X, test_X)
    train_X_r, test_X_r = reduce_features(train_X_n, test_X_n)

    best_k, best_error = check_knn(train_X_r, train_y, test_X_r, test_y)
    print("Best result for ", file, " is best_k = ", best_k, ", best_error = ", best_error)
    #check_RFR(train_X_r, train_y, test_X_r, test_y)
    
    model = neighbors.KNeighborsRegressor(n_neighbors = best_k)
    model.fit(train_X_r, train_y)  #fit the model
    pred=model.predict(test_X_r) #make prediction on test set
    error = mean_squared_error(test_y,pred) #calculate mse
    diff = test_y-pred
    
    print("MSE : ", error)
    print("Diff_max = ", diff.max(), ", Diff_min = ", diff.min())    
    plotTestPred(test_y, pred)

    


# ### TEST OTHERS CLASSIFIERS

# In[38]:


from sklearn import linear_model

dataset = pd.read_csv('C:/Temp/MeasHydroDone/Features/features_50_150.csv')
dataset = dataset.drop(dataset.columns[0], axis=1)
train_X, train_y, test_X, test_y = split_dataset(dataset, testsize=0.2)

train_X_n, test_X_n = scale_features(train_X, test_X)
train_X_r, test_X_r = reduce_features(train_X_n, test_X_n)

classifiers = [
    linear_model.SGDRegressor(),
    linear_model.BayesianRidge(),
    linear_model.LassoLars(),
    linear_model.ARDRegression(),
    linear_model.PassiveAggressiveRegressor(),
    linear_model.TheilSenRegressor(),
    linear_model.LinearRegression()]

for classifier in classifiers:
    clf = classifier
    clf.fit(train_X_r, train_y)
    pred=clf.predict(test_X_r)
    error = mean_squared_error(test_y,pred) #calculate mse
    print(classifier)
    print(error)
    # plotTestPred(test_y, pred)

