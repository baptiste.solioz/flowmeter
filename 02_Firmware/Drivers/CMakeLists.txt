#---------------------------------------------------------------------------------------
# Author    : Solioz Baptiste
# Project   : FlowMeter - Drivers
#---------------------------------------------------------------------------------------
cmake_minimum_required(VERSION 3.15.3)
project(Drivers)

#---------------------------------------------------------------------------------------
# Target
#---------------------------------------------------------------------------------------
file(GLOB ${PROJECT_NAME}_SOURCES
    "${CMAKE_CURRENT_SOURCE_DIR}/BSP/*.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/CMSIS/DSP/Source/TransformFunctions/arm_rfft_init_q15.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/CMSIS/DSP/Source/TransformFunctions/arm_cfft_q15.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/CMSIS/DSP/Source/TransformFunctions/arm_rfft_q15.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/CMSIS/DSP/Source/TransformFunctions/arm_cfft_radix4_q15.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/CMSIS/DSP/Source/TransformFunctions/arm_bitreversal2.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/CMSIS/DSP/Source/StatisticsFunctions/arm_mean_q15.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/CMSIS/DSP/Source/StatisticsFunctions/arm_max_q15.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/CMSIS/DSP/Source/StatisticsFunctions/arm_min_q15.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/CMSIS/DSP/Source/BasicMathFunctions/arm_offset_q15.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/CMSIS/DSP/Source/BasicMathFunctions/arm_mult_q15.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/CMSIS/DSP/Source/BasicMathFunctions/arm_abs_q15.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/CMSIS/DSP/Source/SupportFunctions/arm_fill_q15.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/CMSIS/DSP/Source/CommonTables/arm_const_structs.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/CMSIS/DSP/Source/CommonTables/arm_common_tables.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/CMSIS/NN/Source/FullyConnectedFunctions/*.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/CMSIS/NN/Source/ActivationFunctions/*.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/CMSIS/NN/Source/ConvolutionFunctions/*.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/CMSIS/NN/Source/PoolingFunctions/*.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/CMSIS/NN/Source/NNSupportFunctions/*.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/STM32WLxx_HAL_Driver/Src/stm32wlxx_hal.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/STM32WLxx_HAL_Driver/Src/stm32wlxx_hal_rcc.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/STM32WLxx_HAL_Driver/Src/stm32wlxx_hal_rcc_ex.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/STM32WLxx_HAL_Driver/Src/stm32wlxx_hal_gpio.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/STM32WLxx_HAL_Driver/Src/stm32wlxx_hal_dma.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/STM32WLxx_HAL_Driver/Src/stm32wlxx_hal_dma_ex.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/STM32WLxx_HAL_Driver/Src/stm32wlxx_hal_pwr.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/STM32WLxx_HAL_Driver/Src/stm32wlxx_hal_pwr_ex.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/STM32WLxx_HAL_Driver/Src/stm32wlxx_hal_cortex.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/STM32WLxx_HAL_Driver/Src/stm32wlxx_hal_spi.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/STM32WLxx_HAL_Driver/Src/stm32wlxx_hal_spi_ex.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/STM32WLxx_HAL_Driver/Src/stm32wlxx_hal_uart.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/STM32WLxx_HAL_Driver/Src/stm32wlxx_hal_uart_ex.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/STM32WLxx_HAL_Driver/Src/stm32wlxx_hal_crc.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/STM32WLxx_HAL_Driver/Src/stm32wlxx_hal_crc_ex.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/STM32WLxx_HAL_Driver/Src/stm32wlxx_hal_rtc.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/STM32WLxx_HAL_Driver/Src/stm32wlxx_hal_rtc_ex.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/STM32WLxx_HAL_Driver/Src/stm32wlxx_hal_adc.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/STM32WLxx_HAL_Driver/Src/stm32wlxx_hal_adc_ex.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/STM32WLxx_HAL_Driver/Src/stm32wlxx_hal_tim.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/STM32WLxx_HAL_Driver/Src/stm32wlxx_hal_tim_ex.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/STM32WLxx_HAL_Driver/Src/stm32wlxx_hal_subghz.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/STM32WLxx_HAL_Driver/Src/stm32wlxx_hal_cryp_ex.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/STM32WLxx_HAL_Driver/Src/stm32wlxx_hal_cryp.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/STM32WLxx_HAL_Driver/Src/stm32wlxx_hal_exti.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/STM32WLxx_HAL_Driver/Src/stm32wlxx_hal_flash.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/STM32WLxx_HAL_Driver/Src/stm32wlxx_hal_flash_ex.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/STM32WLxx_HAL_Driver/Src/stm32wlxx_hal_gtzc.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/STM32WLxx_HAL_Driver/Src/stm32wlxx_hal_hsem.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/STM32WLxx_HAL_Driver/Src/stm32wlxx_hal_i2c_ex.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/STM32WLxx_HAL_Driver/Src/stm32wlxx_hal_i2c.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/STM32WLxx_HAL_Driver/Src/stm32wlxx_hal_ipcc.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/STM32WLxx_HAL_Driver/Src/stm32wlxx_hal_lptim.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/STM32WLxx_HAL_Driver/Src/stm32wlxx_hal_pka.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/STM32WLxx_HAL_Driver/Src/stm32wlxx_hal_rng_ex.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/STM32WLxx_HAL_Driver/Src/stm32wlxx_hal_rng.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/STM32WLxx_HAL_Driver/Src/stm32wlxx_hal_smbus.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/STM32WLxx_HAL_Driver/Src/stm32wlxx_hal_usart_ex.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/STM32WLxx_HAL_Driver/Src/stm32wlxx_hal_usart.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/STM32WLxx_HAL_Driver/Src/stm32wlxx_hal_wwdg.c"
)

add_library(${PROJECT_NAME} OBJECT EXCLUDE_FROM_ALL ${${PROJECT_NAME}_SOURCES})

#---------------------------------------------------------------------------------------
# Definitions
#---------------------------------------------------------------------------------------
target_compile_definitions(${PROJECT_NAME} PUBLIC -DUSE_FREERTOS)
target_compile_definitions(${PROJECT_NAME} PUBLIC -DUSE_HAL_DRIVER)
target_compile_definitions(${PROJECT_NAME} PUBLIC -DSTM32WLE5xx)

target_compile_definitions(${PROJECT_NAME} PUBLIC ACTIVE_REGION=${ACTIVE_REGION})

# Loops through all regions and add compile time definitions for the enabled ones.
foreach( REGION ${REGION_LIST} )
    if(${REGION})
        target_compile_definitions(${PROJECT_NAME} PUBLIC -D"${REGION}")
    endif()
endforeach()

# Use of Lora Advanced Packages
if(${LORA_ADVANCED_ENABLED} MATCHES ON)
    target_compile_definitions(${PROJECT_NAME} PUBLIC -DUSE_LORA_ADVANCED)
endif()

#---------------------------------------------------------------------------------------
# Includes
#---------------------------------------------------------------------------------------
target_include_directories( ${PROJECT_NAME} PUBLIC
    ${CMAKE_CURRENT_SOURCE_DIR}
    ${CMAKE_CURRENT_SOURCE_DIR}/BSP
    ${CMAKE_CURRENT_SOURCE_DIR}/CMSIS/Include
    ${CMAKE_CURRENT_SOURCE_DIR}/CMSIS/DSP/Include
    ${CMAKE_CURRENT_SOURCE_DIR}/CMSIS/NN/Include
    ${CMAKE_CURRENT_SOURCE_DIR}/STM32WLxx_HAL_Driver/Inc
    ${CMAKE_CURRENT_SOURCE_DIR}/CMSIS/Device/ST/STM32WLxx/Include
    ${CMAKE_CURRENT_SOURCE_DIR}/STM32WLxx_HAL_Driver/Inc/Legacy
    $<TARGET_PROPERTY:FlowMeter,INTERFACE_INCLUDE_DIRECTORIES>
    $<TARGET_PROPERTY:Middlewares,INTERFACE_INCLUDE_DIRECTORIES>
    $<TARGET_PROPERTY:Utilities,INTERFACE_INCLUDE_DIRECTORIES>
)

#---------------------------------------------------------------------------------------
# Properties
#---------------------------------------------------------------------------------------
set_property(TARGET ${PROJECT_NAME} PROPERTY C_STANDARD 11)
