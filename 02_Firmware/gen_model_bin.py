VERSION = '1.0.0'

USAGE = '''gen_model_bin.py: Generate a binary file from an hex file with only the NN model
Usage:
    python gen_model_bin.py [options] HEXFILE
Options:
    -o, --output            output file name (default new.bin)
    -s, --start             start of firmware address   (default 0x8031800)
    -f, --fuota             size of Fuota space         (default 32768)
    -h, --help              this help message.
    -v, --version           version info.
'''

import sys
import getopt
from intelhex import IntelHex, diff_dumps

def main(argv=None):

    fileFuota = "new.bin"
    startAddr = 0x8031800
    fuotaSize = 32768

    if argv is None:
        argv = sys.argv[1:]
    try:
        opts, args = getopt.gnu_getopt(argv, 'osfhv', ['ouptut', 'start', 'fuota', 'help', 'version'])
        argnb = 0
        for o,a in opts:
            arg = args[argnb]
            #print("option : " + o + " is " + arg)
            argnb += 1
            if o in ('-h', '--help'):
                print(USAGE)
                return 0
            elif o in ('-v', '--version'):
                print(VERSION)
                return 0
            elif o in ('-o', '--output'):
                fileFuota = arg
            elif o in ('-s', '--start'):
                try:
                    startAddr = int(arg, base=16)
                except:
                    sys.stderr.write("Start Address is not numeric\n")
                    return 1
            elif o in ('-f', '--fuota'):
                try:
                    fuotaSize = int(arg)
                except:
                    sys.stderr.write("Fuota Size is not numeric\n")
                    return 1

    except getopt.GetoptError:
        e = sys.exc_info()[1]     # current exception
        sys.stderr.write(str(e)+"\n")
        sys.stderr.write(USAGE+"\n")
        return 1

    fname1 = args[argnb]
    
    ih1 = IntelHex(fname1)
    
    try:
        fnew=open(fileFuota,"wb")
    except getopt.GetoptError:
        sys.stderr.write("Error when open new file for FUOTA : " + str(e) + "\n")

    block_a = (ih1.tobinstr(start=startAddr, size=fuotaSize))
    fnew.write(block_a)
    fnew.close()

if __name__ == '__main__':
    sys.exit(main())