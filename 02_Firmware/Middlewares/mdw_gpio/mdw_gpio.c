/**
  ******************************************************************************
  * @file           : mdw_gpio.c
  * @brief          : Middleware for the GPIO
  *                   
  * @author         : Baptiste Solioz
  * @date           : 13. November 2020
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "mdw_gpio.h"
#include "app_conf.h"

/* Private typedef -----------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/
void (*mdw_gpio_exti2_callback)();
void (*mdw_gpio_exti4_callback)();
void (*mdw_gpio_exti5_callback)();

/********************************************************************************
  * @brief SPI1 Initialization Function
  * @param None
  * @retval None
********************************************************************************/
void mdw_gpio_init(void)
{
    GPIO_InitTypeDef   GPIO_InitStructure;
    /* Enable GPIOB clock */
    __HAL_RCC_GPIOB_CLK_ENABLE();

    /* Configure PA.00 pin as input floating */
    GPIO_InitStructure.Mode = GPIO_MODE_IT_FALLING;
    GPIO_InitStructure.Pull = GPIO_PULLUP;
    GPIO_InitStructure.Pin = IN_PUSH_USER_Pin;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStructure);
    GPIO_InitStructure.Mode = GPIO_MODE_IT_RISING;
    GPIO_InitStructure.Pull = GPIO_PULLDOWN;
    GPIO_InitStructure.Pin = INT1_BMA400_Pin | INT2_BMA400_Pin;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStructure);

    /* Enable and set line 2 Interrupt to the lowest priority */
    HAL_NVIC_SetPriority(EXTI2_IRQn, 8, 0);
    HAL_NVIC_EnableIRQ(EXTI2_IRQn);
    /* Enable and set line 4 Interrupt to the lowest priority */
    HAL_NVIC_SetPriority(EXTI4_IRQn, 8, 0);
    HAL_NVIC_EnableIRQ(EXTI4_IRQn);
    /* Enable and set line 2 Interrupt to the lowest priority */
    HAL_NVIC_SetPriority(EXTI9_5_IRQn, 8, 0);
    HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);

    mdw_gpio_exti2_callback = NULL;
    mdw_gpio_exti4_callback = NULL;
    mdw_gpio_exti5_callback = NULL;
}
/********************************************************************************
  * @brief SPI Read function
  * @param 
  * @retval None
********************************************************************************/
void mdw_gpio_exti_subscribe(uint8_t line, void* callback)
{
    //APP_LOG(TS_ON, VLEVEL_L, "EXTI Subscribe\n\r");
    if(line == IN_PUSH_USER_Pin)
    {
        mdw_gpio_exti2_callback = callback;
    }
    else if(line == INT1_BMA400_Pin)
    {
        mdw_gpio_exti4_callback = callback;
    }
    else if(line == INT2_BMA400_Pin)
    {
        mdw_gpio_exti5_callback = callback;
    }
}

/********************************************************************************
  * @brief SPI Read function
  * @param 
  * @retval None
********************************************************************************/
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
    //APP_LOG(TS_ON, VLEVEL_L, "EXTI Callback\n\r");
    if (GPIO_Pin == IN_PUSH_USER_Pin)
    {
        if(mdw_gpio_exti2_callback != NULL)
        {
            mdw_gpio_exti2_callback();
        }
    }
    else if(GPIO_Pin == INT1_BMA400_Pin)
    {
        if(mdw_gpio_exti4_callback != NULL)
        {
            mdw_gpio_exti4_callback();
        }
    }
    else if(GPIO_Pin == INT2_BMA400_Pin)
    {
        if(mdw_gpio_exti5_callback != NULL)
        {
            mdw_gpio_exti5_callback();
        }
    }
}


void EXTI2_IRQHandler(void)
{
    HAL_GPIO_EXTI_IRQHandler(IN_PUSH_USER_Pin);
}
void EXTI4_IRQHandler(void)
{
    HAL_GPIO_EXTI_IRQHandler(INT1_BMA400_Pin);
}
void EXTI9_5_IRQHandler(void)
{
    HAL_GPIO_EXTI_IRQHandler(INT2_BMA400_Pin);
}


/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
