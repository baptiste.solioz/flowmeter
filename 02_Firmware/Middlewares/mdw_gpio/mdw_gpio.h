/**
  ******************************************************************************
  * @file           : mdw_gpio.h
  * @brief          : Header for mdw_gpio.c file.
  *                   This file contains the definitions for mdw_gpio.h
  * @author         : Baptiste Solioz
  * @date           : 13. November 2020
  ******************************************************************************
  */

#ifndef __MDW_GPIO_H
#define __MDW_GPIO_H

#ifdef __cplusplus
extern "C" {
#endif

/* Private includes ----------------------------------------------------------*/
#include "main.h"

/* Exported types ------------------------------------------------------------*/


/* Exported constants --------------------------------------------------------*/

/* Exported macro ------------------------------------------------------------*/


/* Exported functions prototypes ---------------------------------------------*/
void mdw_gpio_init(void);
void mdw_gpio_exti_subscribe(uint8_t line, void* callback);



/* Private defines -----------------------------------------------------------*/


#ifdef __cplusplus
}
#endif

#endif // __MDW_GPIO_H
