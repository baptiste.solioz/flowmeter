/**
  ******************************************************************************
  * @file           : mdw_mem.h
  * @brief          : Header for mdw_mem.c file.
  *                   This file contains the definitions for mdw_mem.h
  * @author         : Baptiste Solioz
  * @date           : 11. January 2020
  ******************************************************************************
  */

#ifndef __MDW_MEM_H
#define __MDW_MEM_H

#ifdef __cplusplus
extern "C" {
#endif

/* Private includes ----------------------------------------------------------*/
#include "main.h"
#include "cmsis_os2.h"

/* Exported types ------------------------------------------------------------*/


/* Exported constants --------------------------------------------------------*/

/* Exported macro ------------------------------------------------------------*/


/* Exported functions prototypes ---------------------------------------------*/
void mdw_mem_init(void);
void* mdw_mem_get(uint16_t size);
void mdw_mem_release(void* mem_ptr);
uint8_t mdw_mem_get_available();

/* Private defines -----------------------------------------------------------*/


#ifdef __cplusplus
}
#endif

#endif // __MDW_MEM_H
