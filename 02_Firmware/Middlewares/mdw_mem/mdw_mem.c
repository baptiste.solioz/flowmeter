/**
  ******************************************************************************
  * @file           : mdw_mem.c
  * @brief          : Middleware for the memory management
  *                   
  * @author         : Baptiste Solioz
  * @date           : 11. January 2020
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "mdw_mem.h"
#include "mw_log_conf.h"
#include <stdbool.h>

/* Private define ------------------------------------------------------------*/
#define MEM_MAX_SIZE_PER_BLOC   16384 + 512
#define NB_BLOC                 2

/* Private typedef -----------------------------------------------------------*/
typedef struct
{
    uint8_t bloc[MEM_MAX_SIZE_PER_BLOC];
    bool inUse;
} mem_bloc_t;

/* Private variables ---------------------------------------------------------*/
osSemaphoreId_t mem_sema;

mem_bloc_t     mem_bloc[NB_BLOC];

/********************************************************************************
  * @brief Memory Initialization Function
  * @param None
  * @retval None
********************************************************************************/
void mdw_mem_init(void)
{
    mem_sema = osSemaphoreNew(NB_BLOC, NB_BLOC, NULL);
    if (mem_sema == NULL) {
        ; // Semaphore object not created, handle failure
    }
    for(int i=0; i<NB_BLOC; i++)
    {
        mem_bloc[i].inUse = false;
    }
    MW_LOG(TS_ON, VLEVEL_M, "%s : Initialization\r\n", __FUNCTION__);
}
/********************************************************************************
  * @brief Get memory pointer
  * @param 
  * @retval None
********************************************************************************/
void* mdw_mem_get(uint16_t size)
{
    osStatus_t ret;
    MW_LOG(TS_ON, VLEVEL_M, "%s : Try\r\n", __FUNCTION__);
    if(size > MEM_MAX_SIZE_PER_BLOC)
        return NULL;
    ret = osSemaphoreAcquire(mem_sema, osWaitForever);
    
    if(ret == osOK)
    {
        for(int i=0; i<NB_BLOC; i++)
        {
            if(mem_bloc[i].inUse == false)
            {
                mem_bloc[i].inUse = true;
                MW_LOG(TS_ON, VLEVEL_M, "%s : Acquire Pointer %d\r\n", __FUNCTION__, i);
                return (uint8_t*)&mem_bloc[i].bloc;
            }
        }
        return NULL;
    }
    else
    {
        return NULL;
    }
}

/********************************************************************************
  * @brief Release memory pointer
  * @param 
  * @retval None
********************************************************************************/
void mdw_mem_release(void* mem_ptr)
{
    MW_LOG(TS_ON, VLEVEL_M, "%s : Release Pointer\r\n", __FUNCTION__);
    for(int i=0; i<NB_BLOC; i++)
    {
        if(mem_bloc[i].bloc == mem_ptr)
        {
            mem_bloc[i].inUse = false;
            MW_LOG(TS_ON, VLEVEL_M, "%s : Bloc %d released\r\n", __FUNCTION__, i);
            break;
        }
    }
    osSemaphoreRelease(mem_sema);
}

/********************************************************************************
  * @brief Return the number of bloc available
  * @param None
  * @retval Nb of bloc available
********************************************************************************/
uint8_t mdw_mem_get_available()
{
    return osSemaphoreGetCount(mem_sema);
}
