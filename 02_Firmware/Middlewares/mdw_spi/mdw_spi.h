/**
  ******************************************************************************
  * @file           : mdw_spi.h
  * @brief          : Header for mdw_spi.c file.
  *                   This file contains the definitions for mdw_spi.h
  * @author         : Baptiste Solioz
  * @date           : 13. November 2020
  ******************************************************************************
  */

#ifndef __MDW_SPI_H
#define __MDW_SPI_H

#ifdef __cplusplus
extern "C" {
#endif

/* Private includes ----------------------------------------------------------*/
#include "main.h"

/* Exported types ------------------------------------------------------------*/


/* Exported constants --------------------------------------------------------*/

/* Exported macro ------------------------------------------------------------*/


/* Exported functions prototypes ---------------------------------------------*/
void mdw_spi_init(void);
int8_t mdw_spi_read(uint8_t address, uint8_t* buffer, uint32_t size, void* intf_ptr);
int8_t mdw_spi_write(uint8_t address, const uint8_t* buffer, uint32_t size, void* intf_ptr);



/* Private defines -----------------------------------------------------------*/


#ifdef __cplusplus
}
#endif

#endif // __MDW_SPI_H
