/**
  ******************************************************************************
  * @file           : mdw_spi.c
  * @brief          : Middleware for the SPI
  *                   
  * @author         : Baptiste Solioz
  * @date           : 13. November 2020
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "mdw_spi.h"
#include "app_conf.h"

/* Private typedef -----------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/
SPI_HandleTypeDef hspi1;

uint8_t addr;

/********************************************************************************
  * @brief SPI1 Initialization Function
  * @param None
  * @retval None
********************************************************************************/
void mdw_spi_init(void)
{
    GPIO_InitTypeDef  gpio_init_structure = {0};

    /* SPI1 parameter configuration*/
    hspi1.Instance = SPI1;
    hspi1.Init.Mode = SPI_MODE_MASTER;
    hspi1.Init.Direction = SPI_DIRECTION_2LINES;
    hspi1.Init.DataSize = SPI_DATASIZE_8BIT;
    hspi1.Init.CLKPolarity = SPI_POLARITY_LOW;
    hspi1.Init.CLKPhase = SPI_PHASE_1EDGE;
    hspi1.Init.NSS = SPI_NSS_SOFT;
    hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_16;
    hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
    hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
    hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
    hspi1.Init.CRCPolynomial = 7;
    hspi1.Init.CRCLength = SPI_CRC_LENGTH_DATASIZE;
    hspi1.Init.NSSPMode = SPI_NSS_PULSE_DISABLE;
    if (HAL_SPI_Init(&hspi1) != HAL_OK)
    {
        Error_Handler();
    }
    /* Configure the GPIO_LED pin */
    gpio_init_structure.Pin = ACC_NSS_Pin;
    gpio_init_structure.Mode = GPIO_MODE_OUTPUT_PP;
    gpio_init_structure.Pull = GPIO_NOPULL;
    gpio_init_structure.Speed = GPIO_SPEED_FREQ_HIGH;

    HAL_GPIO_Init(ACC_NSS_GPIO_Port, &gpio_init_structure);
    HAL_GPIO_WritePin(ACC_NSS_GPIO_Port, ACC_NSS_Pin, GPIO_PIN_SET);
}

/********************************************************************************
  * @brief SPI Read function
  * @param 
  * @retval None
********************************************************************************/
int8_t mdw_spi_read(uint8_t address, uint8_t* buffer, uint32_t size, void* intf_ptr)
{
    //APP_LOG(TS_ON, VLEVEL_L, "SPI READ REG=0x%x Size=%d\n\r", address, size);
    HAL_GPIO_WritePin(ACC_NSS_GPIO_Port, ACC_NSS_Pin, GPIO_PIN_RESET);
    addr = address;
    HAL_SPI_Transmit(&hspi1, &addr, 1, 1000);
    if (HAL_SPI_Receive(&hspi1, (uint8_t *)buffer, size, 1000)!= HAL_OK)
    {
        APP_LOG(TS_ON, VLEVEL_L, "Error SPI Read\n\r");
    }
    HAL_GPIO_WritePin(ACC_NSS_GPIO_Port, ACC_NSS_Pin, GPIO_PIN_SET);
    return 0;
}

/********************************************************************************
  * @brief SPI Read function
  * @param 
  * @retval None
********************************************************************************/
int8_t mdw_spi_write(uint8_t address, const uint8_t* buffer, uint32_t size, void* intf_ptr)
{
    //APP_LOG(TS_ON, VLEVEL_L, "SPI WRITE REG=0x%x Size=%d\n\r", address, size);
    HAL_GPIO_WritePin(ACC_NSS_GPIO_Port, ACC_NSS_Pin, GPIO_PIN_RESET);
    addr = address;
    HAL_SPI_Transmit(&hspi1, &addr, 1, 1000);
    if (HAL_SPI_Transmit(&hspi1, (uint8_t *)buffer, size, 1000)!= HAL_OK)
    {
        APP_LOG(TS_ON, VLEVEL_L, "Error SPI Write\n\r");
    }
    HAL_GPIO_WritePin(ACC_NSS_GPIO_Port, ACC_NSS_Pin, GPIO_PIN_SET);
    return 0;
}

/********************************************************************************
  * @brief SPI MSP Initialization
  * This function configures the hardware resources used in this example
  * @param hspi: SPI handle pointer
  * @retval None
********************************************************************************/
void HAL_SPI_MspInit(SPI_HandleTypeDef* hspi)
{
    GPIO_InitTypeDef GPIO_InitStruct = {0};
    if(hspi->Instance==SPI1)
    {
        /* Peripheral clock enable */
        __HAL_RCC_SPI1_CLK_ENABLE();

        __HAL_RCC_GPIOA_CLK_ENABLE();

        GPIO_InitStruct.Pin = ACC_SCK_Pin|ACC_MISO_Pin|ACC_MOSI_Pin;
        GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
        GPIO_InitStruct.Pull = GPIO_NOPULL;
        GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
        GPIO_InitStruct.Alternate = GPIO_AF5_SPI2;
        HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
    }
}

/********************************************************************************
  * @brief SPI MSP De-Initialization
  * This function freeze the hardware resources used in this example
  * @param hspi: SPI handle pointer
  * @retval None
********************************************************************************/
void HAL_SPI_MspDeInit(SPI_HandleTypeDef* hspi)
{
    if(hspi->Instance==SPI1)
    {
        /* Peripheral clock disable */
        __HAL_RCC_SPI2_CLK_DISABLE();

        /* SPI2 GPIO Configuration */
        HAL_GPIO_DeInit(GPIOA, ACC_SCK_Pin|ACC_MISO_Pin|ACC_MOSI_Pin);
    }
}

/********************************************************************************
  * @brief ADC Conversion Complete Callback
  * @param AdcHandle: ADC handle pointer
  * @retval None
********************************************************************************/


/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
