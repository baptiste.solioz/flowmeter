/** 
 * @brief       Autogenerated module by Keras2arm.py
 *              This module contains a function which can be used to
 *              classify MNIST images using the ARM-CMSIS-NN lib
 * @Note        This module uses the q15 implementation
 * @date        2021-02-08 11:07
 * @file        mdw_nn.c
 * @author      Raphael Zingg zing@zhaw.ch
 * @copyright   2018 ZHAW / Institute of Embedded Systems
 */
#include "arm_nnfunctions.h"
#include "weights.h"
#include "mdw_nn.h"
#include "mdw_mem.h"
#include "local_fct.h"
#include <stdint.h>
#include "app_conf.h"

#define MAX_BUFFER_SIZE CONV1D_1_OU_CH*CONV1D_1_IN_DIM

q15_t* aq15_input_Buf;
q15_t* aq15_output_Buf;
q15_t buffer[MAX_BUFFER_SIZE];

int16_t mdw_nn_run(q15_t * aq15_input_data)
{
    q31_t prediction = 0;

    /* GET MEMORY BUFFER FROM SHARED MEMORY */
    aq15_input_Buf = mdw_mem_get(MAX_BUFFER_SIZE*2);
    aq15_output_Buf = mdw_mem_get(MAX_BUFFER_SIZE*2);

    memset(aq15_input_Buf, 0, sizeof(q15_t)*MAX_BUFFER_SIZE);
    memset(aq15_output_Buf, 0, sizeof(q15_t)*MAX_BUFFER_SIZE);

    /* CONVOLUTION LAYER 1 */
    memcpy(aq15_input_Buf, aq15_input_data, sizeof(q15_t)*CONV1D_1_IN_DIM);
    local_convolve_HWC_q15_fast_nonsquare(aq15_input_Buf,
                                        CONV1D_1_IN_DIM,
                                        1,
                                        CONV1D_1_IN_CH,
                                        aq15_conv1d_1_weights,
                                        CONV1D_1_OU_CH,
                                        CONV1D_1_KER_DIM,
                                        1,
                                        CONV1D_1_PADDING,
                                        1,
                                        CONV1D_1_STRIDE,
                                        1,
                                        aq15_conv1d_1_bias,
                                        CONV1D_1_IN_SHIFT,
                                        CONV1D_1_OU_SHIFT,
                                        aq15_output_Buf,
                                        CONV1D_1_OU_DIM,
                                        1,
                                        buffer,
                                        NULL);

    /* ACTIVATION LAYER */
    arm_relu_q15(   aq15_output_Buf,
                    CONV1D_1_OU_CH*CONV1D_1_OU_DIM);
    /* CONVOLUTION LAYER 2 */
    memcpy(aq15_input_Buf, aq15_output_Buf, sizeof(q15_t)*MAX_BUFFER_SIZE);
    local_convolve_HWC_q15_fast_nonsquare(aq15_input_Buf,
                                        CONV1D_2_IN_DIM,
                                        1,
                                        CONV1D_2_IN_CH,
                                        aq15_conv1d_2_weights,
                                        CONV1D_2_OU_CH,
                                        CONV1D_2_KER_DIM,
                                        1,
                                        CONV1D_2_PADDING,
                                        1,
                                        CONV1D_2_STRIDE,
                                        1,
                                        aq15_conv1d_2_bias,
                                        CONV1D_2_IN_SHIFT,
                                        CONV1D_2_OU_SHIFT,
                                        aq15_output_Buf,
                                        CONV1D_2_OU_DIM,
                                        1,
                                        buffer,
                                        NULL);

    /* ACTIVATION LAYER */
    arm_relu_q15(   aq15_output_Buf,
                    CONV1D_2_OU_CH*CONV1D_2_OU_DIM);
    /* CONVOLUTION LAYER 3 */
    memcpy(aq15_input_Buf, aq15_output_Buf, sizeof(q15_t)*MAX_BUFFER_SIZE);
    local_convolve_HWC_q15_fast_nonsquare(aq15_input_Buf,
                                        CONV1D_3_IN_DIM,
                                        1,
                                        CONV1D_3_IN_CH,
                                        aq15_conv1d_3_weights,
                                        CONV1D_3_OU_CH,
                                        CONV1D_3_KER_DIM,
                                        1,
                                        CONV1D_3_PADDING,
                                        1,
                                        CONV1D_3_STRIDE,
                                        1,
                                        aq15_conv1d_3_bias,
                                        CONV1D_3_IN_SHIFT,
                                        CONV1D_3_OU_SHIFT,
                                        aq15_output_Buf,
                                        CONV1D_3_OU_DIM,
                                        1,
                                        buffer,
                                        NULL);

    /* ACTIVATION LAYER */
    arm_relu_q15(   aq15_output_Buf,
                    CONV1D_3_OU_CH*CONV1D_3_OU_DIM);
    /* AVERAGE POOLING LAYER 1 */
    memcpy(aq15_input_Buf, aq15_output_Buf, sizeof(q15_t)*MAX_BUFFER_SIZE);
    local_avepool_q15_HWC(  aq15_input_Buf,
                            POOL_1_IN_DIM,
                            1,
                            POOL_1_IN_CH,
                            POOL_1_KER_DIM,
                            1,
                            POOL_1_PADDING,
                            1,
                            POOL_1_STRIDE,
                            1,
                            POOL_1_OU_DIM,
                            1,
                            0,
                            (q7_t*)buffer,
                            aq15_output_Buf);

    /* FULL CONNECTED LAYER */
    memcpy(aq15_input_Buf, aq15_output_Buf, sizeof(q15_t)*MAX_BUFFER_SIZE);
    arm_fully_connected_q15(aq15_input_Buf,
                            aq15_dense_1_weights,
                            DENSE_1_IN_DIM,
                            DENSE_1_OU_DIM,
                            DENSE_1_IN_SHIFT,
                            DENSE_1_OU_SHIFT,
                            aq15_dense_1_bias,
                            aq15_output_Buf,
                            NULL);
    /* ACTIVATION LAYER */
    arm_relu_q15(   aq15_output_Buf,
                    DENSE_1_OU_DIM);
    /* FULL CONNECTED LAYER */
    memcpy(aq15_input_Buf, aq15_output_Buf, sizeof(q15_t)*MAX_BUFFER_SIZE);
    arm_fully_connected_q15(aq15_input_Buf,
                            aq15_dense_2_weights,
                            DENSE_2_IN_DIM,
                            DENSE_2_OU_DIM,
                            DENSE_2_IN_SHIFT,
                            DENSE_2_OU_SHIFT,
                            aq15_dense_2_bias,
                            aq15_output_Buf,
                            NULL);
    /* ACTIVATION LAYER */
    arm_relu_q15(   aq15_output_Buf,
                    DENSE_2_OU_DIM);
    /* FULL CONNECTED LAYER */
    memcpy(aq15_input_Buf, aq15_output_Buf, sizeof(q15_t)*MAX_BUFFER_SIZE);
    arm_fully_connected_q15(aq15_input_Buf,
                            aq15_dense_3_weights,
                            DENSE_3_IN_DIM,
                            DENSE_3_OU_DIM,
                            DENSE_3_IN_SHIFT,
                            DENSE_3_OU_SHIFT,
                            aq15_dense_3_bias,
                            aq15_output_Buf,
                            NULL);
    /* ACTIVATION LAYER */
    int16_t index = 0;

    for (int i = 0; i < DENSE_3_OU_DIM; i++)
    {
        index = (int16_t) aq15_output_Buf[i]>>TANH_SHIFT;
        if(index < 0)
            index = 0;
        if(index >= sizeof(tanh_lut))
            index = sizeof(tanh_lut) - 1;
        aq15_output_Buf[i] = (q31_t) tanh_lut[index];
    }

    /* GET THE PREDICTION */
    prediction = (q31_t) aq15_output_Buf[0];

    q31_t max_train_q = 17869;          // 34.9 converted in Q6.9 format
    q31_t min_train_q = 4710;           // 9.8 converted in Q6.9 format

    // Q2.13 * Q6.9 = Q8.23 => min_train_q << OUTPUT_SHIFT
    prediction = prediction * (max_train_q - min_train_q) + (min_train_q << OUTPUT_SHIFT);
    prediction = prediction >> OUTPUT_SHIFT;       // Q8.23 => Q6.9

    /* RELEASE MEMORY BLOC */
    mdw_mem_release(aq15_output_Buf);
    mdw_mem_release(aq15_input_Buf);

    return (q15_t) prediction;
}
