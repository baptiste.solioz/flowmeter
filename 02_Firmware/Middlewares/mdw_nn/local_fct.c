#include "local_fct.h"
#include "app_conf.h"

//Implementation found to nnom library

// modified from CMSIS-NN test_ref
void local_avepool_q15_HWC(const q15_t *Im_in,           // input image
	const uint16_t dim_im_in_x,  // input image dimension x or W
	const uint16_t dim_im_in_y,  // input image dimension y or H
	const uint16_t ch_im_in,     // number of input image channels
	const uint16_t dim_kernel_x, // window kernel size
	const uint16_t dim_kernel_y, // window kernel size
	const uint16_t padding_x,    // padding sizes
	const uint16_t padding_y,    // padding sizes
	const uint16_t stride_x,     // stride
	const uint16_t stride_y,     // stride
	const uint16_t dim_im_out_x, // output image dimension x or W
	const uint16_t dim_im_out_y, // output image dimension y or H
	const uint16_t output_shift, // output right shift
	q7_t *bufferA,               // a buffer for local storage, NULL by now
	q15_t *Im_out)
{
    int16_t i_x;
    int16_t k_x;

    for (i_x = 0; i_x < dim_im_out_x; i_x++)    //32
    {
        int32_t sum = 0;
        int16_t count = 0;

        for (k_x = 0; k_x < dim_im_in_x; k_x++)
        {
            sum += Im_in[i_x+k_x*dim_im_out_x];
            count++;
        }
        Im_out[i_x] = (int16_t) (sum / count);
    }
}

// arm_status
// local_convolve_HWC_q15_fast_nonsquare(const q15_t * Im_in,
//                                     const uint16_t dim_im_in_x,
//                                     const uint16_t dim_im_in_y,
//                                     const uint16_t ch_im_in,
//                                     const q15_t * wt,
//                                     const uint16_t ch_im_out,
//                                     const uint16_t dim_kernel_x,
//                                     const uint16_t dim_kernel_y,
//                                     const uint16_t padding_x,
//                                     const uint16_t padding_y,
//                                     const uint16_t stride_x,
//                                     const uint16_t stride_y,
//                                     const q15_t * bias,
//                                     const uint16_t bias_shift,
//                                     const uint16_t out_shift,
//                                     q15_t * Im_out,
//                                     const uint16_t dim_im_out_x,
//                                     const uint16_t dim_im_out_y, 
//                                     q15_t * bufferA, 
//                                     q7_t * bufferB)
// {
//     /* Run the following code as reference implementation for Cortex-M0 and Cortex-M3 */
//     uint16_t  i, j, k, l, m, n;
//     int       conv_out;
//     signed char in_row, in_col;

//     for (i = 0; i < ch_im_out; i++)             // 32
//     {
//         for (k = 0; k < dim_im_out_x; k++)      // 82
//         {
//             conv_out = ((q31_t)bias[i] << bias_shift) + NN_ROUND(out_shift);
//             for (n = 0; n < dim_kernel_x; n++)  // 6
//             {
//                 in_col = stride_x * k + n - padding_x;
//                 if (in_col >= 0 && in_col < dim_im_in_x)
//                 {
//                     for (l = 0; l < ch_im_in; l++)  // 32
//                     {
//                         int16_t index_im = in_col * ch_im_in + l;
//                         int16_t index_wt = i * ch_im_in * dim_kernel_x + n * ch_im_in + l;
//                         conv_out += Im_in[in_col * ch_im_in + l] * wt[i * ch_im_in * dim_kernel_x + n * ch_im_in + l];
//                         if(i==0 && k == 0 && ch_im_in != 1)
//                         {
//                             //APP_PPRINTF(VLEVEL_H, "Im : %d @ %d, Wt :  %d @ %d\r\n", Im_in[index_im], index_im, wt[index_wt], index_wt);
//                         }
//                     }
//                 }
//             }
                
//             Im_out[i + (k) * ch_im_out] = (q15_t) __SSAT((conv_out >> out_shift), 16);
//         }
//     }

//     /* Return to application */
//     return ARM_MATH_SUCCESS;
// }

arm_status
local_convolve_HWC_q15_fast_nonsquare(const q15_t * Im_in,
                                    const uint16_t dim_im_in_x,
                                    const uint16_t dim_im_in_y,
                                    const uint16_t ch_im_in,
                                    const q15_t * wt,
                                    const uint16_t ch_im_out,
                                    const uint16_t dim_kernel_x,
                                    const uint16_t dim_kernel_y,
                                    const uint16_t padding_x,
                                    const uint16_t padding_y,
                                    const uint16_t stride_x,
                                    const uint16_t stride_y,
                                    const q15_t * bias,
                                    const uint16_t bias_shift,
                                    const uint16_t out_shift,
                                    q15_t * Im_out,
                                    const uint16_t dim_im_out_x,
                                    const uint16_t dim_im_out_y, 
                                    q15_t * bufferA, 
                                    q7_t * bufferB)
{
    int16_t   i_out_x, i_ker_x;

    q15_t    *pBuffer = bufferA;
    q15_t    *im_buffer = bufferA;
    q15_t    *pOut = Im_out;

    /* This part implements the im2col function */

    for (i_out_x = 0; i_out_x < dim_im_out_x; i_out_x++)
    {
        for (i_ker_x = i_out_x * stride_x - padding_x; i_ker_x < i_out_x * stride_x - padding_x + dim_kernel_x; i_ker_x++)
        {
            if (i_ker_x < 0 || i_ker_x >= dim_im_in_x)
            {
                /* arm_fill_q15(0, pBuffer, ch_im_in); */
                memset(pBuffer, 0, sizeof(q15_t)*ch_im_in);
            } else
            {
                /* arm_copy_q15((q15_t *) Im_in + (i_ker_y * dim_im_in_x + i_ker_x) * ch_im_in, pBuffer, ch_im_in); */
                memcpy(pBuffer, (q15_t *) Im_in + (i_ker_x) * ch_im_in, sizeof(q15_t)*ch_im_in);
            }
            pBuffer += ch_im_in;
        }

        if (i_out_x & 0x1)
        {
            int       i;
            /* initialize the matrix pointers for A */
            const q15_t *pA = wt;

            /* set up the second output pointers */
            q15_t    *pOut2 = pOut + ch_im_out;

            /* this loop over rows in A */
            for (i = 0; i < ch_im_out; i += 2)
            {
                /* setup pointers for B */
                q15_t    *pB = im_buffer;
                const q15_t *pB2 = pB + ch_im_in * dim_kernel_x;

                /* aling the second pointer for A */
                const q15_t *pA2 = pA + ch_im_in * dim_kernel_x;

                /* init the sum with bias */
                q31_t     sum =  ((q31_t)bias[i] << bias_shift) + NN_ROUND(out_shift);
                q31_t     sum2 = ((q31_t)bias[i] << bias_shift) + NN_ROUND(out_shift);
                q31_t     sum3 = ((q31_t)bias[i + 1] << bias_shift) + NN_ROUND(out_shift);
                q31_t     sum4 = ((q31_t)bias[i + 1] << bias_shift) + NN_ROUND(out_shift);

                uint16_t  colCnt = ch_im_in * dim_kernel_x >> 1;
                /* accumulate over the vector */
                while (colCnt)
                {
                    q31_t     inA1 = *__SIMD32(pA)++;
                    q31_t     inB1 = *__SIMD32(pB)++;
                    q31_t     inA2 = *__SIMD32(pA2)++;
                    q31_t     inB2 = *__SIMD32(pB2)++;

                    sum = __SMLAD(inA1, inB1, sum);
                    sum2 = __SMLAD(inA1, inB2, sum2);
                    sum3 = __SMLAD(inA2, inB1, sum3);
                    sum4 = __SMLAD(inA2, inB2, sum4);

                    colCnt--;
                }           /* while over colCnt */
                colCnt = ch_im_in * dim_kernel_x & 0x1;
                while (colCnt)
                {
                    q15_t     inA1 = *pA++;
                    q15_t     inB1 = *pB++;
                    q15_t     inA2 = *pA2++;
                    q15_t     inB2 = *pB2++;

                    sum += inA1 * inB1;
                    sum2 += inA1 * inB2;
                    sum3 += inA2 * inB1;
                    sum4 += inA2 * inB2;
                    colCnt--;
                }           /* while over colCnt */
                *pOut++ = (q15_t) __SSAT(sum >> out_shift, 16);
                *pOut++ = (q15_t) __SSAT(sum3 >> out_shift, 16);
                *pOut2++ = (q15_t) __SSAT(sum2 >> out_shift, 16);
                *pOut2++ = (q15_t) __SSAT(sum4 >> out_shift, 16);

                /* skip the row computed with A2 */
                pA += ch_im_in * dim_kernel_x;
            }               /* for over ch_im_out */

            pOut += ch_im_out;
            /* counter reset */
            pBuffer = im_buffer;
        }
    }
    /* Return to application */
    return ARM_MATH_SUCCESS;
}