/**
  ******************************************************************************
  * @file           : mdw_flash.h
  * @brief          : Header for mdw_flash.c file.
  *                   This file contains the definitions for mdw_flash.h
  * @author         : Baptiste Solioz
  * @date           : 14. February 2021
  ******************************************************************************
  */

#ifndef __MDW_FLASH_H
#define __MDW_FLASH_H

#ifdef __cplusplus
extern "C" {
#endif

/* Private includes ----------------------------------------------------------*/
#include "main.h"
#include "stdbool.h"
#include "stm32wlxx_hal_flash.h"

/* Exported types ------------------------------------------------------------*/


/* Exported constants --------------------------------------------------------*/

/* Exported macro ------------------------------------------------------------*/


/* Exported functions prototypes ---------------------------------------------*/
HAL_StatusTypeDef mdw_flash_init(void);
HAL_StatusTypeDef mdw_flash_erase(void *pStart, uint32_t uLength);
HAL_StatusTypeDef mdw_flash_write(void *pDestination, const void *pSource, uint32_t uLength);
HAL_StatusTypeDef mdw_flash_read(void *pDestination, const void *pSource, uint32_t Length);

/* Private defines -----------------------------------------------------------*/


#ifdef __cplusplus
}
#endif

#endif // __MDW_FLASH_H
