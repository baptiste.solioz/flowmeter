/**
  ******************************************************************************
  * @file           : mdw_flash.c
  * @brief          : Middleware for the Flash Read/Write
  *                   
  * @author         : Baptiste Solioz
  * @date           : 14. February 2021
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "mdw_flash.h"
#include "mw_log_conf.h"
#include <string.h>
/* Private typedef -----------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/
static __IO uint32_t DoubleECC_Error_Counter = 0U;
static __IO bool DoubleECC_Check;

/* Private define ------------------------------------------------------------*/
#define NB_PAGE_SECTOR_PER_ERASE  2U    /*!< Nb page erased per erase */
/* Private functions ---------------------------------------------------------*/
static uint32_t GetPage(uint32_t uAddr);
static uint32_t GetBank(uint32_t uAddr);

/********************************************************************************
  * @brief  Unlocks Flash for write access
  * @param  None
  * @retval HAL Status.
********************************************************************************/
HAL_StatusTypeDef mdw_flash_init(void)
{
    HAL_StatusTypeDef ret = HAL_ERROR;

    /* Unlock the Program memory */
    if (HAL_FLASH_Unlock() == HAL_OK)
    {
        /* Clear all FLASH flags */
        __HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_ALL_ERRORS);
        /* Unlock the Program memory */
        if (HAL_FLASH_Lock() == HAL_OK)
        {
            ret = HAL_OK;
        }                           
    }
    return ret;
}

/********************************************************************************
  * @brief  This function does an erase of n (depends on Length) pages in user flash area
  * @param  pStart: Start of user flash area
  * @param  uLength: number of bytes.
  * @retval HAL status.
********************************************************************************/
HAL_StatusTypeDef mdw_flash_erase(void *pStart, uint32_t uLength)
{
  uint32_t page_error = 0U;
  uint32_t uStart = (uint32_t)pStart;
  FLASH_EraseInitTypeDef x_erase_init;
  HAL_StatusTypeDef e_ret_status = HAL_ERROR;
  uint32_t first_page = 0U, nb_pages = 0U;
  uint32_t chunk_nb_pages;
  uint32_t erase_command = 0U;
  uint32_t bank_number = 0U;

  /* Initialize Flash */
  e_ret_status = mdw_flash_init();

  first_page = GetPage(uStart);
  bank_number = GetBank(uStart);
  MW_LOG(TS_ON, VLEVEL_M, "%s : FirstPage = 0x%x, bank = 0x%x\r\n", __FUNCTION__, first_page, bank_number);
  //FLASHMEMHANDLER_TRACE("[FLASH_IF] FirstPage = 0x%x, bank = 0x%x\r\n", first_page, bank_number);

  if (e_ret_status == HAL_OK)
  {
    /* Unlock the Flash to enable the flash control register access *************/
    if (HAL_FLASH_Unlock() == HAL_OK)
    {
      do
      {
        /* Get the 1st page to erase */
        first_page = GetPage(uStart);
        
        /* Get the number of pages to erase from 1st page */
        nb_pages = GetPage(uStart + uLength - 1U) - first_page + 1U;

        /* Fill EraseInit structure*/
        x_erase_init.TypeErase = FLASH_TYPEERASE_PAGES;
        //x_erase_init.Banks = bank_number;

        /* Erase flash per NB_PAGE_SECTOR_PER_ERASE to avoid watch-dog */
        do
        {
        chunk_nb_pages = (nb_pages >= NB_PAGE_SECTOR_PER_ERASE) ? NB_PAGE_SECTOR_PER_ERASE : nb_pages;
        x_erase_init.Page = first_page;
        x_erase_init.NbPages = chunk_nb_pages;
        first_page += chunk_nb_pages;
        nb_pages -= chunk_nb_pages;
        if (HAL_FLASHEx_Erase(&x_erase_init, &page_error) != HAL_OK)
        {
            HAL_FLASH_GetError();
            e_ret_status = HAL_ERROR;
        }
        /* Refresh Watchdog */
        /* WRITE_REG(IWDG->KR, IWDG_KEY_RELOAD); */
        }
        while (nb_pages > 0);
        erase_command = 1U;
      }
      while (erase_command == 0);
      /* Lock the Flash to disable the flash control register access (recommended
      to protect the FLASH memory against possible unwanted operation) *********/
      HAL_FLASH_Lock();

    }
    else
    {
      e_ret_status = HAL_ERROR;
    }
  }

  return e_ret_status;
}

/********************************************************************************
  * @brief  This function writes a data buffer in flash (data are 64-bit aligned).
  * @note   After writing data buffer, the flash content is checked.
  * @param  pDestination: Start address for target location
  * @param  pSource: pointer on buffer with data to write
  * @param  uLength: Length of data buffer in byte. It has to be 64-bit aligned.
  * @retval HAL Status.
********************************************************************************/
HAL_StatusTypeDef mdw_flash_write(void *pDestination, const void *pSource, uint32_t uLength)
{
  HAL_StatusTypeDef e_ret_status = HAL_ERROR;
  uint32_t i = 0U;
  uint32_t pdata = (uint32_t)pSource;

  /* Initialize Flash */
  e_ret_status = mdw_flash_init();

  if (e_ret_status == HAL_OK)
  {
    /* Unlock the Flash to enable the flash control register access *************/
    if (HAL_FLASH_Unlock() != HAL_OK)
    {
      MW_LOG(TS_ON, VLEVEL_M, "ERROR ==> Unlock not possible\n");
      return HAL_ERROR;
    }
    else
    {
      //PRINTF("Flash Write : Memory addr 0x%08x length%04d\r\n", pDestination, uLength);

      /* DataLength must be a multiple of 32 bit */
      for (i = 0U; i < uLength; i += 8U)
      {
        /* Device voltage range supposed to be [2.7V to 3.6V], the operation will
        be done by word */
    	e_ret_status = HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD, (uint32_t)pDestination,  *((uint64_t *)(pdata + i)));
        if (e_ret_status == HAL_OK)
        {
          /* Check the written value */
          if (*(uint32_t *)pDestination != *(uint32_t *)(pdata + i))
          {
            /* Flash content doesn't match SRAM content */
            e_ret_status = HAL_ERROR;
            MW_LOG(TS_ON, VLEVEL_M, "ERROR ==> Memory check failure\n");
            break;
          }
          /* Increment FLASH Destination address */
          pDestination = (void *)((uint32_t)pDestination + 8U);
        }
        else
        {
            
            /* Error occurred while writing data in Flash memory */
            MW_LOG(TS_ON, VLEVEL_M, "ERROR ==> Memory write failure %d\n", e_ret_status);
            break;
        }
      }
      /* Lock the Flash to disable the flash control register access (recommended
      to protect the FLASH memory against possible unwanted operation) *********/
      HAL_FLASH_Lock();
    }
  }
  return e_ret_status;
}

/********************************************************************************
  * @brief  This function reads flash
  * @param  pDestination: Start address for target location
  * @param  pSource: pointer on buffer with data to write
  * @param  Length: Length in bytes of data buffer
  * @retval HAL_StatusTypeDef HAL_OK if successful, HAL_ERROR otherwise.
********************************************************************************/
HAL_StatusTypeDef mdw_flash_read(void *pDestination, const void *pSource, uint32_t Length)
{
  HAL_StatusTypeDef e_ret_status = HAL_ERROR;

  DoubleECC_Error_Counter = 0U;
  DoubleECC_Check = true;
  memcpy(pDestination, pSource, Length);
  DoubleECC_Check = false;
  if (DoubleECC_Error_Counter == 0U)
  {
    e_ret_status = HAL_OK;
  }
  DoubleECC_Error_Counter = 0U;

  return e_ret_status;
}

/********************************************************************************
  * @brief  Gets the page of a given address
  * @param  uAddr: Address of the FLASH Memory
  * @retval The page of a given address
********************************************************************************/
static uint32_t GetPage(uint32_t uAddr)
{
  uint32_t page = 0U;

/* Bank 1 */
page = (uAddr - FLASH_BASE) / FLASH_PAGE_SIZE;

  return page;
}
/********************************************************************************
  * @brief  Gets the bank of a given address
  * @param  uAddr: Address of the FLASH Memory
  * @retval The bank of a given address
********************************************************************************/
static uint32_t GetBank(uint32_t uAddr)
{
  return FLASH_BASE;
}