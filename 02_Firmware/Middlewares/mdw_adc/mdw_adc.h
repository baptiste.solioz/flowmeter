/**
  ******************************************************************************
  * @file           : mdw_adc.h
  * @brief          : Header for mdw_adc.c file.
  *                   This file contains the definitions for mdw_adc.h
  * @author         : Baptiste Solioz
  * @date           : 30. October 2020
  ******************************************************************************
  */

#ifndef __MDW_ADC_H
#define __MDW_ADC_H

#ifdef __cplusplus
extern "C" {
#endif

/* Private includes ----------------------------------------------------------*/
#include "main.h"
#include "stdbool.h"

/* Exported types ------------------------------------------------------------*/


/* Exported constants --------------------------------------------------------*/

/* Exported macro ------------------------------------------------------------*/


/* Exported functions prototypes ---------------------------------------------*/
void mdw_adc_init(void);
void mdw_adc_start(uint32_t* adc_buffer, uint32_t nb_data, void *callback);
bool mdw_adc_is_measuring();
void mdw_adc_enable();
void mdw_adc_disable();

/* Private defines -----------------------------------------------------------*/


#ifdef __cplusplus
}
#endif

#endif // __MDW_ADC_H
