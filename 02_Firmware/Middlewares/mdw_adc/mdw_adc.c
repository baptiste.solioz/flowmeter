/**
  ******************************************************************************
  * @file           : mdw_adc.c
  * @brief          : Middleware for the ADC
  *                   
  * @author         : Baptiste Solioz
  * @date           : 30. October 2020
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "mdw_adc.h"
#include "mdw_dma.h"
#include "app_conf.h"
#include "stm32wlxx_board.h"

/* Private typedef -----------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;
DMA_HandleTypeDef hdma_adc1;
TIM_HandleTypeDef htim2;

bool _isMeasuring;

/* Private define ------------------------------------------------------------*/
#define     ADC_SAMPLING_RATE_HZ   10000
/* Private functions ---------------------------------------------------------*/
void (*mdw_adc_callback)();

/********************************************************************************
  * @brief ADC1 Initialization Function
  * @param None
  * @retval Thread Id
********************************************************************************/
void mdw_adc_init(void)
{
    ADC_ChannelConfTypeDef sConfig = {0};

    _isMeasuring = false;

    TIM_ClockConfigTypeDef sClockSourceConfig = {0};
    TIM_MasterConfigTypeDef sMasterConfig = {0};
    TIM_OC_InitTypeDef sConfigOC = {0};
    // Initialize TIM2 for sampling rate
    htim2.Instance = TIM2;
    htim2.Init.Prescaler = 48;                      // Frequency TIM2 48MHz
    htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
    htim2.Init.Period = 1000000 / ADC_SAMPLING_RATE_HZ;
    htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
    htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE;
    if (HAL_TIM_Base_Init(&htim2) != HAL_OK)
    {
        Error_Handler();
    }
    sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
    if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK)
    {
        Error_Handler();
    }
    if (HAL_TIM_OC_Init(&htim2) != HAL_OK)
    {
        Error_Handler();
    }
    sMasterConfig.MasterOutputTrigger = TIM_TRGO_UPDATE;
    sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
    if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
    {
        Error_Handler();
    }
    sConfigOC.OCMode = TIM_OCMODE_TIMING;
    sConfigOC.Pulse = 0;
    sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
    sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
    if (HAL_TIM_OC_ConfigChannel(&htim2, &sConfigOC, TIM_CHANNEL_4) != HAL_OK)
    {
        Error_Handler();
    }
    // Initialize ADC
    hadc1.Instance = ADC;
    hadc1.Init.ClockPrescaler        = ADC_CLOCK_SYNC_PCLK_DIV2;
    hadc1.Init.Resolution            = ADC_RESOLUTION_12B;
    hadc1.Init.DataAlign             = ADC_DATAALIGN_RIGHT;
    hadc1.Init.ScanConvMode          = ADC_SCAN_DISABLE;              /* Sequencer set to fully configurable: only the rank 1 is enabled (no scan sequence on several ranks) */
    hadc1.Init.EOCSelection          = ADC_EOC_SINGLE_CONV;
    hadc1.Init.ContinuousConvMode    = DISABLE;                       /* Continuous mode disabled to have only 1 conversion at each conversion trig */
    hadc1.Init.NbrOfConversion       = 1;                             /* Parameter discarded because sequencer is disabled. Parameter relevancy depending on setting of parameter "ScanConvMode" */
    hadc1.Init.DiscontinuousConvMode = DISABLE;                       /* Parameter discarded because sequencer is disabled */
    hadc1.Init.ExternalTrigConv      = ADC_EXTERNALTRIG_T2_TRGO;            /* Software start to trig the 1st conversion manually, without external event */
    hadc1.Init.ExternalTrigConvEdge  = ADC_EXTERNALTRIGCONVEDGE_RISING; /* Parameter discarded because trig of conversion by software start (no external event) */
    hadc1.Init.DMAContinuousRequests = ENABLE;                       /* ADC with DMA transfer: continuous requests to DMA disabled (default state) since DMA is not used in this example. */
    hadc1.Init.Overrun               = ADC_OVR_DATA_OVERWRITTEN;
    hadc1.Init.SamplingTimeCommon1   = ADC_SAMPLETIME_12CYCLES_5;
    hadc1.Init.OversamplingMode      = ENABLE;
    hadc1.Init.Oversampling.Ratio    = ADC_OVERSAMPLING_RATIO_16;
    hadc1.Init.Oversampling.RightBitShift = ADC_RIGHTBITSHIFT_4;
    hadc1.Init.Oversampling.TriggeredMode = ADC_TRIGGEREDMODE_SINGLE_TRIGGER;
    hadc1.Init.TriggerFrequencyMode = ADC_TRIGGER_FREQ_HIGH;
    if (HAL_ADC_Init(&hadc1) != HAL_OK)
    {
        Error_Handler();
    }
    // Set the priority 
    HAL_NVIC_SetPriority(ADC_IRQn, 7, 0);
    HAL_NVIC_EnableIRQ(ADC_IRQn);

    // Configure the channel 7 for vibrating sensor
    sConfig.Channel = ADC_CHANNEL_7;
    sConfig.Rank = ADC_REGULAR_RANK_1;
    sConfig.SamplingTime = ADC_SAMPLETIME_160CYCLES_5;

    if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
    {
    Error_Handler();
    }
    // Perform a calibration
    if (HAL_ADCEx_Calibration_Start(&hadc1) != HAL_OK)
    {
        Error_Handler();
    }

    GPIO_InitTypeDef  gpio_init_structure = {0};

    /* Configure the Piezo Power pin */
    gpio_init_structure.Pin = OUT_PIEZO_PWR_Pin;
    gpio_init_structure.Mode = GPIO_MODE_OUTPUT_PP;
    gpio_init_structure.Pull = GPIO_NOPULL;
    gpio_init_structure.Speed = GPIO_SPEED_FREQ_LOW;
    // Disable the sensor power supply
    HAL_GPIO_Init(OUT_PIEZO_PWR_GPIO_Port, &gpio_init_structure);
    HAL_GPIO_WritePin(OUT_PIEZO_PWR_GPIO_Port, OUT_PIEZO_PWR_Pin, GPIO_PIN_SET);

}

/********************************************************************************
  * @brief ADC1 Start conversion with DMA
  * @param adc_buffer : pointer for the result
  * @param nb_data : Number of bytes to write
  * @retval None
********************************************************************************/
void mdw_adc_start(uint32_t* adc_buffer, uint32_t nb_data, void* callback)
{
    GPIO_InitTypeDef  gpio_init_structure = {0};
    // Configure GPIO in analog mode during the measure
    gpio_init_structure.Pin = ADC_PIEZO_Pin;
    gpio_init_structure.Mode = GPIO_MODE_ANALOG;
    gpio_init_structure.Pull = GPIO_NOPULL;
    gpio_init_structure.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    HAL_GPIO_Init(ADC_PIEZO_GPIO_Port, &gpio_init_structure);

    //mdw_dma_init();
    //mdw_adc_init();
    _isMeasuring = true;
    mdw_adc_callback = callback;
    // Start measure and sampling timer
    HAL_ADC_Start_DMA(&hadc1, adc_buffer, nb_data);
    HAL_TIM_Base_Start(&htim2);      // USE HAL_TIM_Base_Start_IT for debug purpose
}

/********************************************************************************
  * @brief Check if a measure is actually perfoming
  * @param None
  * @retval true if a measure is performing
********************************************************************************/
bool mdw_adc_is_measuring()
{
    return _isMeasuring;
}

/********************************************************************************
  * @brief Enable the vibration sensor supply
  * @param None
  * @retval None
********************************************************************************/
void mdw_adc_enable()
{
    // Enable power supply
    HAL_GPIO_WritePin(OUT_PIEZO_PWR_GPIO_Port, OUT_PIEZO_PWR_Pin, GPIO_PIN_RESET);
}

/********************************************************************************
  * @brief Disable the vibration sensor supply
  * @param None
  * @retval None
********************************************************************************/
void mdw_adc_disable()
{
    GPIO_InitTypeDef  gpio_init_structure = {0};
    // Set input with pulldown to discharge it
    gpio_init_structure.Pin = ADC_PIEZO_Pin;
    gpio_init_structure.Mode = GPIO_MODE_INPUT;
    gpio_init_structure.Pull = GPIO_PULLDOWN;
    gpio_init_structure.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(ADC_PIEZO_GPIO_Port, &gpio_init_structure);
    // Disable power supply
    HAL_GPIO_WritePin(OUT_PIEZO_PWR_GPIO_Port, OUT_PIEZO_PWR_Pin, GPIO_PIN_SET);
}

/********************************************************************************
  * @brief ADC MSP Initialization
  * This function configures the hardware resources used in this example
  * @param hadc: ADC handle pointer
  * @retval None
********************************************************************************/
void HAL_ADC_MspInit(ADC_HandleTypeDef *hadc)
{
    GPIO_InitTypeDef GPIO_InitStruct = {0};
    /* Enable clock of GPIO associated to the peripheral channels */
    __HAL_RCC_ADC_CLK_ENABLE();
    __HAL_RCC_GPIOA_CLK_ENABLE();

    GPIO_InitStruct.Pin = ADC_PIEZO_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(ADC_PIEZO_GPIO_Port, &GPIO_InitStruct);

    /* ADC1 DMA Init */
    mdw_dma_init();
    /* ADC1 Init */
    hdma_adc1.Instance = DMA1_Channel1;
    hdma_adc1.Init.Request = DMA_REQUEST_ADC;
    hdma_adc1.Init.Direction = DMA_PERIPH_TO_MEMORY;
    hdma_adc1.Init.PeriphInc = DMA_PINC_DISABLE;
    hdma_adc1.Init.MemInc = DMA_MINC_ENABLE;
    hdma_adc1.Init.PeriphDataAlignment = DMA_PDATAALIGN_HALFWORD;
    hdma_adc1.Init.MemDataAlignment = DMA_MDATAALIGN_HALFWORD;
    hdma_adc1.Init.Mode = DMA_CIRCULAR;
    hdma_adc1.Init.Priority = DMA_PRIORITY_LOW;
    if (HAL_DMA_Init(&hdma_adc1) != HAL_OK)
    {
      Error_Handler();
    }

    __HAL_LINKDMA(hadc,DMA_Handle,hdma_adc1);
}

/********************************************************************************
  * @brief ADC MSP De-Initialization
  * This function freeze the hardware resources used in this example
  * @param hadc: ADC handle pointer
  * @retval None
********************************************************************************/
void HAL_ADC_MspDeInit(ADC_HandleTypeDef *hadc)
{
    if (hadc->Instance == ADC)
    {
        /* Peripheral clock disable */
        __HAL_RCC_ADC_CLK_DISABLE();
        HAL_GPIO_DeInit(ADC_PIEZO_GPIO_Port, ADC_PIEZO_Pin);

        /* ADC1 DMA DeInit */
        HAL_DMA_DeInit(hadc->DMA_Handle);
    }

}

/********************************************************************************
* @brief TIM_Base MSP Initialization
* This function configures the hardware resources used in this example
* @param htim_base: TIM_Base handle pointer
* @retval None
********************************************************************************/
void HAL_TIM_Base_MspInit(TIM_HandleTypeDef* htim_base)
{
    if(htim_base->Instance==TIM2)
    {
        /* Peripheral clock enable */
        __HAL_RCC_TIM2_CLK_ENABLE();
        /* TIM2 interrupt Init */
        HAL_NVIC_SetPriority(TIM2_IRQn, 7, 0);
        HAL_NVIC_EnableIRQ(TIM2_IRQn);
    }
}

/********************************************************************************
* @brief TIM_Base MSP De-Initialization
* This function freeze the hardware resources used in this example
* @param htim_base: TIM_Base handle pointer
* @retval None
********************************************************************************/
void HAL_TIM_Base_MspDeInit(TIM_HandleTypeDef* htim_base)
{
    if(htim_base->Instance==TIM2)
    {
        /* Peripheral clock disable */
        __HAL_RCC_TIM2_CLK_DISABLE();
    }
}

/********************************************************************************
  * @brief ADC Conversion Complete Callback
  * @param AdcHandle: ADC handle pointer
  * @retval None
********************************************************************************/
void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* AdcHandle)
{
    if (AdcHandle->Instance == ADC)
    {
        // End of measure
        HAL_ADC_Stop_DMA(AdcHandle);
        HAL_TIM_Base_Stop(&htim2);      // USE HAL_TIM_Base_Stop_IT for debug purpose
        // Disable Power supply
        mdw_adc_disable();
        if(mdw_adc_callback != NULL)
        {
            // Call callback
            mdw_adc_callback();
            mdw_adc_callback = NULL;
        }
        _isMeasuring = false;
    }
}

/********************************************************************************
  * @brief TIM2 Period Elapsed Callback
  * @param htim: TIM handle pointer
  * @retval None
********************************************************************************/
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
    if (htim->Instance == TIM2)
    {
        // Used for debug purpose only
    }
}

