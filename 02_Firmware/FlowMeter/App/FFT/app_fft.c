/**
  ******************************************************************************
  * @file           : app_fft.c
  * @brief          : Create a thread to handle the fft
  *                   
  * @author         : Baptiste Solioz
  * @date           : 15. October 2020
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "app_fft.h"
#include "stm32_systime.h"

/* Private constants ---------------------------------------------------------*/
#define     FLAG_START_FFT          0x01
#define     FFT_SAMPLES_LENGTH      8192    // NB of FFT samples
#define     FFT_MIN_NN              16      // Neural network min FFT sample idx
#define     FFT_SIZE_NN             106     // Neural network max FFT sample idx

/* Private variables ---------------------------------------------------------*/
static App_FFT_Attr_t   _app_fft_data;

static int16_t*         _fft_data_ptr;
static int16_t*         _fft_result;
static int16_t          _fft_nn[FFT_SIZE_NN];

const int16_t           _test_data_low[FFT_SIZE_NN] = {23, 8, 14, 7, 14, 5, 16, 1, 11, 3, 19, 1, 15, 11, 12, 11, 15, 6, 4, 2, 11, 6, 10, 2, 6, 7, 8, 7, 9, 3, 13, 0, 8, 4, 9, 15, 9, 10, 1, 17, 9, 5, 14, 9, 8, 4, 10, 6, 2, 1, 5, 17, 12, 1, 45, 21, 14, 5, 6, 10, 13, 5, 1, 3, 6, 4, 6, 5, 5, 5, 6, 3, 4, 6, 7, 4, 7, 4, 5, 4, 5, 4, 5, 3, 5, 3, 4, 3, 4, 4, 5, 4, 5, 3, 5, 3, 5, 4, 3, 5, 4, 3, 4, 2, 3, 4};
const int16_t           _test_data_mid[FFT_SIZE_NN] = {10, 4, 19, 24, 22, 3, 31, 12, 13, 17, 17, 20, 27, 7, 29, 10, 12, 2, 23, 7, 10, 2, 11, 5, 32, 6, 15, 2, 9, 2, 23, 4, 34, 20, 17, 23, 19, 17, 14, 15, 40, 31, 9, 3, 3, 7, 6, 1, 21, 8, 13, 74, 39, 24, 58, 22, 35, 28, 7, 7, 10, 7, 3, 5, 4, 1, 2, 7, 0, 3, 6, 4, 0, 4, 1, 5, 1, 6, 5, 4, 5, 4, 1, 4, 3, 3, 5, 1, 1, 5, 1, 6, 3, 7, 4, 0, 1, 0, 0, 3, 5, 2, 2, 2, 4, 3};
const int16_t           _test_data_high[FFT_SIZE_NN] = {19, 12, 21, 18, 4, 16, 5, 20, 7, 7, 19, 56, 12, 14, 42, 79, 57, 19, 28, 122, 1, 9, 70, 6, 42, 19, 25, 4, 36, 6, 10, 26, 1, 34, 80, 39, 35, 67, 132, 46, 136, 19, 88, 206, 186, 13, 466, 119, 1085, 334, 378, 90, 189, 101, 213, 65, 1, 188, 297, 119, 356, 71, 104, 171, 94, 50, 114, 217, 124, 139, 7, 14, 43, 20, 53, 31, 175, 6, 162, 13, 140, 128, 22, 179, 28, 46, 174, 162, 54, 201, 65, 148, 89, 119, 105, 55, 187, 253, 11, 167, 10, 40, 90, 104, 26, 82};
/* Private functions ---------------------------------------------------------*/
void _app_fft_display(char* name, int16_t* data, uint16_t size);
void _app_fft_perform();
void _app_fft_thread(void *argument);

/********************************************************************************
  * @brief FFT Initialize
  * @param None
  * @retval Thread Id
********************************************************************************/
osThreadId_t app_fft_init()
{
    _app_fft_data.thread_attributes.name = "FFT Task";
    _app_fft_data.thread_attributes.priority = (osPriority_t) osPriorityNormal;
#if (APP_FFT_LOG_ENABLED == 1)
    _app_fft_data.thread_attributes.stack_size = 256 * 4;
#else
    _app_fft_data.thread_attributes.stack_size = 256 * 6;
#endif
    // Initalize the state
    _app_fft_data.state = APP_FFT_STATE_INIT;
    // Create a thread
    _app_fft_data.thread_id = osThreadNew(_app_fft_thread, NULL, &_app_fft_data.thread_attributes);
    if(_app_fft_data.thread_id == NULL)
    {
        APP_FFT_TLOG(LOGE, "%s : Thread not created\n\r", __FUNCTION__);
    }
    // Create the event queue
    _app_fft_data.queue = osMessageQueueNew(5, sizeof(EventMessage_t), NULL);
    if(_app_fft_data.queue == NULL)
    {
        APP_FFT_TLOG(LOGE, "%s : Queue not created\n\r", __FUNCTION__);
    }
    // Subscribe to the event
    event_handler_subscribe(EVENT_TYPE_FFT_START,  _app_fft_data.queue);

    APP_FFT_TLOG(LOGI, "%s : Initialization Done\n\r", __FUNCTION__);

    return _app_fft_data.thread_id;
}

/********************************************************************************
  * @brief FFT Thread
  * @param argument : not used, use static variable
  * @retval None
********************************************************************************/
void _app_fft_thread(void *argument)
{ 
    EventMessage_t  rx_event;
    int16_t result_nn = 0;
    int8_t idx_tst = 0;

    osDelay(MS_TO_TICKS(100));
    APP_FFT_TLOG(LOGI, "%s : Start Example for FFT\n\r", __FUNCTION__);

    for(;;)
    {
        // Thread stack analyzer
        app_fft_remaining_stack = osThreadGetStackSpace(_app_fft_data.thread_id);
        switch(_app_fft_data.state)
        {
            case APP_FFT_STATE_INIT:
            {
                APP_FFT_TLOG(LOGI,"%s : State Init\n\r", __FUNCTION__);
                _app_fft_data.state = APP_FFT_STATE_IDLE;
            }
            break;
            case APP_FFT_STATE_IDLE:
            {
#if (ALWAYS_MEASURE_VIB == 0)
                APP_FFT_TLOG(LOGI,"%s : State Idle\n\r", __FUNCTION__);
#endif
                // Wait until the ADC samples are available
                if(osMessageQueueGet(_app_fft_data.queue, &rx_event, NULL, osWaitForever) == osOK) 
                {
                    if(rx_event.type == EVENT_TYPE_FFT_START)
                    {
                        _app_fft_data.state = APP_FFT_STATE_PERFORM;
                    }
                }
                else
                {
                    //APP_FFT_TLOG(LOGE, "%s : Error when getting messsage from queue\n\r", __FUNCTION__);
                }
            }
            break;
            case APP_FFT_STATE_PERFORM:
            {
                // Get the memory pointer from shared memory
                _fft_result = (int16_t*)mdw_mem_get(FFT_SAMPLES_LENGTH);
                if(_fft_result != NULL)
                {
                     _fft_data_ptr = (int16_t*) rx_event.data;
                    _app_fft_perform();             // Perform FFT
                    // Cut the FFT to take only samples needed by the NN
                    for(int i=0; i<(FFT_SIZE_NN); i++)
                        _fft_nn[i] = (int16_t) _fft_result[i+FFT_MIN_NN];
                    mdw_mem_release(_fft_result);   // Release memory

                    APP_FFT_TLOG(LOGI, "%s : Prepare and send data\n\r", __FUNCTION__);
                    // Send Event to inform that the FFT is terminated
                    EventMessage_t message = {
                        .source = _app_fft_data.thread_id,
                        .type   = EVENT_TYPE_FFT_END,
                        .data   = NULL,
                        .size   = 0
                    };
                    event_handler_send(&message);
                }

                _app_fft_data.state = APP_FFT_AI;
            }
            break;
            case APP_FFT_AI:
            {
                APP_FFT_TLOG(LOGI, "%s : State APP_FFT_AI\n\r", __FUNCTION__);
                // switch(idx_tst)
                // {
                //     case 0:
                //         memcpy(_fft_nn, _test_data_low, sizeof(_fft_nn));
                //         idx_tst = 1;
                //         break;
                //     case 1:
                //         memcpy(_fft_nn, _test_data_mid, sizeof(_fft_nn));
                //         idx_tst = 2;
                //         break;
                //     case 2:
                //         memcpy(_fft_nn, _test_data_high, sizeof(_fft_nn));
                //         idx_tst = 0;
                //         break;
                // }
                
                // Run the Neural network
                result_nn = mdw_nn_run(_fft_nn);
                // Result format Q6.9
                int16_t result_m = (result_nn >> 9);
                int16_t result_n = ((result_nn - (result_m << 9)) * 100) >> 9;
                APP_FFT_TLOG(LOGI, "%s : Prediction: %d.%d\n\r", __FUNCTION__, result_m, result_n);
                SysTime_t timestamp = SysTimeGet();     // Get measure timestamp
                // Send NN result through an event
                EventMessage_t message = {
                    .source = _app_fft_data.thread_id,
                    .type   = EVENT_TYPE_NN_END,
                    .data_int   = result_nn,
                    .timeout = timestamp.Seconds,
                    .size   = 2
                };
                event_handler_send(&message);

                _app_fft_data.state = APP_FFT_STATE_IDLE;
            }
            break;
            default:
                APP_FFT_TLOG(LOGI, "%s : State Error\n\r", __FUNCTION__);
        }
    }
}

/********************************************************************************
  * @brief FFT Perform
  * @param None
  * @retval None
********************************************************************************/
void _app_fft_perform()
{
    arm_rfft_instance_q15 fftInstance;
    arm_status status = ARM_MATH_SUCCESS;

    int16_t mean = 0;
    int16_t max = 0;
    uint32_t max_pos = 0;

#if (ALWAYS_MEASURE_VIB == 0) && (APP_FFT_LOG_ENABLED == 1)
    uint32_t startTime = HAL_GetTick();
    uint32_t stopTime = 0;
#endif

    // Get the mean value to remove the continue composant
    arm_mean_q15(_fft_data_ptr, FFT_SAMPLES_LENGTH, &mean);

#if (ALWAYS_MEASURE_VIB == 0)
    //_app_fft_display("ADC_Input", _fft_data_ptr, FFT_SAMPLES_LENGTH);
#endif
    // Center data to 0 and multiply by 10 to imporve FFT
    for(int i=0; i<FFT_SAMPLES_LENGTH; i++)
    {
        _fft_data_ptr[i] = 10 * (_fft_data_ptr[i]-mean);
    }
    //init FFT instance
    status = arm_rfft_init_q15(&fftInstance, FFT_SAMPLES_LENGTH/2, 0, 1);
    if(status == ARM_MATH_SUCCESS)
    {
        /* Process the data through the CFFT/CIFFT module */
        arm_rfft_q15(&fftInstance, _fft_data_ptr, _fft_result);
    }
    // Get only the absolute FFT value
    arm_abs_q15(_fft_result, _fft_result, FFT_SAMPLES_LENGTH);
    // Get max value
    arm_max_q15(_fft_result, FFT_SAMPLES_LENGTH/2, &max, &max_pos);
#if (ALWAYS_MEASURE_VIB == 0) && (APP_FFT_LOG_ENABLED == 1)
    stopTime = HAL_GetTick();
#endif
    //if(max > 50 && max_pos > 3)
    {
        APP_FFT_TLOG(LOGI, "%s : Mean : %d, Max : %d @ %d \n\r", __FUNCTION__, mean, max, max_pos);
    }

#if (ALWAYS_MEASURE_VIB == 0)
    APP_FFT_TLOG(LOGI, "%s : Interval Time = %d tick\n\r", __FUNCTION__, stopTime - startTime);
    //_app_fft_display("FFT",_fft_result, FFT_SAMPLES_LENGTH/8);
#endif

}

/********************************************************************************
  * @brief FFT Display data
  * @param data : pointer to the data to display
  * @param size : data size to display
  * @retval None
********************************************************************************/
void _app_fft_display(char* name, int16_t* data, uint16_t size)
{
    APP_PPRINTF(LOGI, "%s = [", name);
    for(int i=0;i<size-1;i++)
    {
        APP_PPRINTF(LOGI, "%d,", (data[i]));
    }
    APP_PPRINTF(LOGI, "%d]\n\r", data[size-1]);
}