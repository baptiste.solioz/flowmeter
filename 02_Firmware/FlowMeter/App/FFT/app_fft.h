/**
  ******************************************************************************
  * @file           : app_fft.h
  * @brief          : Header for app_fft.c file.
  *                   This file contains the definitions for app_fft.h
  * @author         : Baptiste Solioz
  * @date           : 15. October 2020
  ******************************************************************************
  */

#ifndef __APP_FFT_H
#define __APP_FFT_H

#ifdef __cplusplus
extern "C" {
#endif

/* Private includes ----------------------------------------------------------*/
#include "app_conf.h"
#include "event_handler.h"
#include "cmsis_os2.h"
#include "stdio.h"

#include "arm_math.h"
#include "arm_const_structs.h"
#include "mdw_nn.h"
#include "mdw_mem.h"

/* LOG Definition ------------------------------------------------------------*/
#if defined (APP_FFT_LOG_ENABLED) && (APP_FFT_LOG_ENABLED == 1)
#define APP_FFT_LOG(VL,...)     do{ {UTIL_ADV_TRACE_COND_FSend(VL, T_REG_OFF, TS_OFF, __VA_ARGS__);} }while(0);
#define APP_FFT_TLOG(VL,...)    do{ {UTIL_ADV_TRACE_COND_FSend(VL, T_REG_OFF, TS_ON, __VA_ARGS__);} }while(0); //with timestamp
#define APP_FFT_PLOG(VL,...)    do{ } while( UTIL_ADV_TRACE_OK != UTIL_ADV_TRACE_COND_FSend(VL, T_REG_OFF, TS_OFF, __VA_ARGS__) ) //Polling Mode
#elif defined (APP_FFT_LOG_ENABLED) && (APP_FFT_LOG_ENABLED == 0) /* APP_LOG disabled */
#define APP_FFT_LOG(...)
#define APP_FFT_TLOG(...)
#define APP_FFT_PLOG(...)
#else
#error "APP_FFT_LOG_ENABLED not defined or out of range <0,1>"
#endif

/* Private typedef -----------------------------------------------------------*/
typedef enum 
{ 
    APP_FFT_STATE_INIT, 
    APP_FFT_STATE_IDLE, 
    APP_FFT_STATE_PERFORM,
    APP_FFT_AI
} App_FFT_State;

typedef struct 
{
    osThreadId_t        thread_id;          ///< Thread Id
    osThreadAttr_t      thread_attributes;  ///< Thread Attributes 
    App_FFT_State       state;              ///< State Machine
    osMessageQueueId_t  queue;
} App_FFT_Attr_t;

/* Exported functions prototypes ---------------------------------------------*/
osThreadId_t app_fft_init();

/* Exported variables --------------------------------------------------------*/
uint16_t app_fft_remaining_stack;

#ifdef __cplusplus
}
#endif

#endif // __APP_FFT_H