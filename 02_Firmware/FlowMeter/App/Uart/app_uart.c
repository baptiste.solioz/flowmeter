/**
  ******************************************************************************
  * @file           : app_uart.c
  * @brief          : Create a thread to handle the Uart application
  *                   
  * @author         : Baptiste Solioz
  * @date           : 08 January 2021
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "app_uart.h"

/* Private constants ---------------------------------------------------------*/
#define     FLAG_RX_START       0x0001
#define     FLAG_RX_STOP        0x0002
#define     FLAG_RX_ADC         0x0004

/* Private variables ---------------------------------------------------------*/
static App_Uart_Attr_t              _app_uart_data;

static uint8_t                      _rx_data[10];

static uint8_t                      _meas_timeout;

/* Private functions ---------------------------------------------------------*/
static void _app_uart_thread(void *argument);
static void _app_uart_rx_callback(uint8_t *PData, uint16_t Size, uint8_t Error);

/********************************************************************************
  * @brief Uart App Initialize
  * @param None
  * @retval Thread Id
********************************************************************************/
osThreadId_t app_uart_init()
{
    _app_uart_data.thread_attributes.name = "Uart App Task";
    _app_uart_data.thread_attributes.priority = (osPriority_t) osPriorityNormal;
#if (APP_UART_LOG_ENABLED == 1)
    _app_uart_data.thread_attributes.stack_size = 256 * 4;
#else
    _app_uart_data.thread_attributes.stack_size = 256 * 2;
#endif
    // Initialize state
    _app_uart_data.state = APP_UART_STATE_INIT;
    // Create thread
    _app_uart_data.thread_id = osThreadNew(_app_uart_thread, NULL, &_app_uart_data.thread_attributes);
    if(_app_uart_data.thread_id == NULL)
    {
        APP_UART_TLOG(LOGE, "%s : Thread not created\n\r", __FUNCTION__);
    }
    // Create Event Queue
    _app_uart_data.queue = osMessageQueueNew(5, sizeof(EventMessage_t), NULL);
    if(_app_uart_data.queue == NULL)
    {
        APP_UART_TLOG(LOGE, "%s : Queue not created\n\r", __FUNCTION__);
    }
    // Subscribe to the events
    event_handler_subscribe(EVENT_TYPE_FFT_END,  _app_uart_data.queue);

    APP_UART_TLOG(LOGI, "%s : Initialization Done\n\r", __FUNCTION__);

    return _app_uart_data.thread_id;
}

/********************************************************************************
  * @brief Uart App Thread
  * @param argument : Not used
  * @retval None
********************************************************************************/
void _app_uart_thread(void *argument)
{    
    EventMessage_t  message;

    for(;;)
    {
        // Stack analyzer
        app_uart_remaining_stack = osThreadGetStackSpace(_app_uart_data.thread_id);
        switch(_app_uart_data.state)
        {
            case APP_UART_STATE_INIT:
            {
                APP_UART_TLOG(LOGI, "%s : State Init\n\r", __FUNCTION__);
                // Enable UART Rx with callback
                UTIL_ADV_TRACE_StartRxProcess((void*)_app_uart_rx_callback);
                _app_uart_data.state = APP_UART_STATE_IDLE;
            }
            break;
            case APP_UART_STATE_IDLE:
            {
                _app_uart_data.state = APP_UART_STATE_IDLE;
                APP_UART_TLOG(LOGI, "%s : State IDLE\n\r", __FUNCTION__);
                // Wait until a message arrives
                osThreadFlagsWait(FLAG_RX_START|FLAG_RX_ADC|FLAG_RX_STOP, osFlagsWaitAny | osFlagsNoClear, osWaitForever);
                if((osThreadFlagsGet() & FLAG_RX_STOP) == FLAG_RX_STOP)
                {
                    // Stop accelerometer measurements
                    osThreadFlagsClear(FLAG_RX_STOP);
                    APP_UART_TLOG(LOGI, "%s : FLAG_RX_STOP\n\r", __FUNCTION__);
                    EventMessage_t message = {
                        .source = _app_uart_data.thread_id,
                        .type   = EVENT_TYPE_ACC_STOP
                    };
                    event_handler_send(&message);
                    osThreadFlagsClear(0xFFFF);
                }
                else if((osThreadFlagsGet() & FLAG_RX_START) == FLAG_RX_START)
                {
                    // Start accelerometer acquisition
                    osThreadFlagsClear(FLAG_RX_START);
                    APP_UART_TLOG(LOGI, "%s : FLAG_RX_START\n\r", __FUNCTION__);
                    EventMessage_t message = {
                        .source = _app_uart_data.thread_id,
                        .type   = EVENT_TYPE_ACC_START,
                        .data   = (void *) &_meas_timeout
                    };
                    event_handler_send(&message);
                    osThreadFlagsClear(0xFFFF);
                }
                else if((osThreadFlagsGet() & FLAG_RX_ADC) == FLAG_RX_ADC)
                {
                    // Start ADC acquisition
                    osThreadFlagsClear(FLAG_RX_ADC);
                    APP_UART_TLOG(LOGI, "%s : FLAG_RX_ADC\n\r", __FUNCTION__);
                    EventMessage_t message = {
                        .source = _app_uart_data.thread_id,
                        .type   = EVENT_TYPE_ADC_START
                    };
                    event_handler_send(&message);
                    _app_uart_data.state = APP_UART_STATE_WAIT_FFT;
                    osThreadFlagsClear(0xFFFF);
                }
            }
            break;
            case APP_UART_STATE_WAIT_FFT:
            {
                APP_UART_TLOG(LOGI, "%s : State Wait FFT\n\r", __FUNCTION__);
                // Wait until the end of the FFT to process next messages
                if(osMessageQueueGet(_app_uart_data.queue, &message, NULL, osWaitForever) == osOK) 
                {
                    if(message.type == EVENT_TYPE_FFT_END)
                    {
                        _app_uart_data.state = APP_UART_STATE_IDLE;
                        osThreadFlagsClear(0xFFFF);
                    }
                }
                else
                {
                    APP_UART_TLOG(LOGE, "%s : Error when getting messsage from queue\n\r", __FUNCTION__);
                }
            }
            break;
            default:
            break;
        }
    }
}

/********************************************************************************
  * @brief Callback from UART
  * @param PData : Data received
  * @param Size : Nb of bytes
  * @param Error : Error code
  * @retval None
********************************************************************************/
void _app_uart_rx_callback(uint8_t *PData, uint16_t Size, uint8_t Error)
{
    static uint16_t nbBytes = 0;
    // Check the end of line
    if(PData[0] != '\n' && PData[0] != '\r')
    {
        _rx_data[nbBytes] = PData[0];
        nbBytes++;
        if(nbBytes == 10)
            nbBytes = 0;
    }
    else
    {
        nbBytes = 0;
        if(_rx_data[0] == 'A' && _rx_data[1] == 'C' && _rx_data[2] == 'C' && _rx_data[3] == '=')
        {
            // ACC=1 message
            _meas_timeout = _rx_data[4] - 0x30;
            osThreadFlagsSet(_app_uart_data.thread_id, FLAG_RX_START);
        }
        else if(_rx_data[0] == 'S' && _rx_data[1] == 'T' && _rx_data[2] == 'O' && _rx_data[3] == 'P')
        {
            // STOP message
            osThreadFlagsSet(_app_uart_data.thread_id, FLAG_RX_STOP);
        }
        else if(_rx_data[0] == 'A' && _rx_data[1] == 'D' && _rx_data[2] == 'C')
        {
            // ADC message
            osThreadFlagsSet(_app_uart_data.thread_id, FLAG_RX_ADC);
        }
    }
}
