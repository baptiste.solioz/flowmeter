/**
  ******************************************************************************
  * @file           : app_uart.h
  * @brief          : Header for app_uart.c file.
  *                   This file contains the definitions for app_uart.h
  * @author         : Baptiste Solioz
  * @date           : 08 January 2021
  ******************************************************************************
  */

#ifndef __APP_UART_H
#define __APP_UART_H

#ifdef __cplusplus
extern "C" {
#endif

/* Private includes ----------------------------------------------------------*/
#include "app_conf.h"
#include "event_handler.h"
#include "cmsis_os2.h"
#include "mdw_gpio.h"

/* LOG Definition ------------------------------------------------------------*/
#if defined (APP_UART_LOG_ENABLED) && (APP_UART_LOG_ENABLED == 1)
#define APP_UART_LOG(VL,...)     do{ {UTIL_ADV_TRACE_COND_FSend(VL, T_REG_OFF, TS_OFF, __VA_ARGS__);} }while(0);
#define APP_UART_TLOG(VL,...)    do{ {UTIL_ADV_TRACE_COND_FSend(VL, T_REG_OFF, TS_ON, __VA_ARGS__);} }while(0); //with timestamp
#define APP_UART_PLOG(VL,...)    do{ } while( UTIL_ADV_TRACE_OK != UTIL_ADV_TRACE_COND_FSend(VL, T_REG_OFF, TS_OFF, __VA_ARGS__) ) //Polling Mode
#elif defined (APP_UART_LOG_ENABLED) && (APP_UART_LOG_ENABLED == 0) /* APP_LOG disabled */
#define APP_UART_LOG(...)
#define APP_UART_TLOG(...)
#define APP_UART_PLOG(...)
#else
#error "APP_UART_LOG_ENABLED not defined or out of range <0,1>"
#endif


/* Private typedef -----------------------------------------------------------*/
typedef enum 
{ 
    APP_UART_STATE_INIT, 
    APP_UART_STATE_IDLE, 
    APP_UART_STATE_WAIT_FFT
} App_Uart_State;

typedef struct {
  osThreadId_t      thread_id;      ///< Thread Id
  osThreadAttr_t    thread_attributes; ///< Thread Attributes 
  App_Uart_State    state;              ///< State Machine
  osMessageQueueId_t  queue;
} App_Uart_Attr_t;

/* Exported functions prototypes ---------------------------------------------*/
osThreadId_t app_uart_init();

/* Exported variables --------------------------------------------------------*/
uint16_t    app_uart_remaining_stack;

#ifdef __cplusplus
}
#endif

#endif // __APP_UART_H