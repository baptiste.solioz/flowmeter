/**
  ******************************************************************************
  * @file           : app_lorawan.c
  * @brief          : Create a thread to handle the LoRaWAN application
  *                   
  * @author         : Baptiste Solioz
  * @date           : 30. October 2020
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "app_lorawan.h"
#include "lora_info.h"
#include "lorawan_version.h"
#include "subghz_phy_version.h"

/* Private typedef -----------------------------------------------------------*/
static uint8_t tx_data_buf[LORAWAN_APP_DATA_BUFF_SIZE];
static volatile bool IsMcSessionStarted = false;
static volatile bool IsClockSynched = false;

extern uint32_t FileRxCrc;

/* Private variables ---------------------------------------------------------*/
static App_LoRaWAN_Attr_t      app_lorawan_data;
// LoRaMAC handler callback pointers
static LmHandlerCallbacks_t LmHandlerCallbacks =
{
  .GetBatteryLevel =           app_lorawan_GetBatteryLevel,
  .GetTemperature =            app_lorawan_GetTemperatureLevel,
  .OnMacProcess =              app_lorawan_LoraMacProcessNotify,
  .OnJoinRequest =             app_lorawan_HasJoined,
  .OnTxData =                  app_lorawan_TxData,
  .OnRxData =                  app_lorawan_RxData
};
// LoRaMAC handler parameters
static LmHandlerParams_t LmHandlerParams =
{
  .ActiveRegion =             ACTIVE_REGION,
  .DefaultClass =             LORAWAN_DEFAULT_CLASS,
  .AdrEnable =                LORAWAN_ADR_STATE,
  .TxDatarate =               LORAWAN_DEFAULT_DATA_RATE,
  .PingPeriodicity =          LORAWAN_DEFAULT_PING_SLOT_PERIODICITY,
  .DutyCycleEnabled =         LORAWAN_DUTYCYCLE_ON
};
// LoRaMAC Fragment Package callbacks
static LmhpFragmentationParams_t FragmentationParams =
{
    .DecoderCallbacks =
    {
        .FragDecoderErase = app_lorawan_FragDecoderErase,
        .FragDecoderWrite = app_lorawan_FragDecoderWrite,
        .FragDecoderRead = app_lorawan_FragDecoderRead,
    },
    .OnProgress = app_lorawan_OnFragProgress,
    .OnDone = app_lorawan_OnFragDone
};
// LoRaWAN Complicance parameters
static LmhpComplianceParams_t LmhpComplianceParams =
{
    .AdrEnabled = LORAWAN_ADR_STATE,
    .DutyCycleEnabled = LORAWAN_DUTYCYCLE_ON,
    .StopPeripherals = NULL,
    .StartPeripherals = NULL,
};

/* Private functions ---------------------------------------------------------*/
void _app_lorawan_thread(void *argument);
uint8_t _app_lorawan_send(LmHandlerAppData_t* appData);
void _app_lorawan_tmr_send_callback(void *argument);

/********************************************************************************
  * @brief Main App Initialize
  * @param None
  * @retval Thread Id
********************************************************************************/
osThreadId_t app_lorawan_init()
{
    app_lorawan_data.thread_attributes.name = "LoRaWAN App Task";
    app_lorawan_data.thread_attributes.priority = (osPriority_t) osPriorityHigh;
#if (APP_LORAWAN_LOG_ENABLED == 1)
    app_lorawan_data.thread_attributes.stack_size = 256 * 5;
#else
    app_lorawan_data.thread_attributes.stack_size = 256 * 5;
#endif
    // Initialize the state
    app_lorawan_data.state = APP_LORAWAN_STATE_INIT;
    // Initialize the tx buffer
    app_lorawan_data.tx_data.Buffer = tx_data_buf;
    // Create the thread
    app_lorawan_data.thread_id = osThreadNew(_app_lorawan_thread, NULL, &app_lorawan_data.thread_attributes);
    if(app_lorawan_data.thread_id == NULL)
    {
        APP_LORAWAN_TLOG(LOGI, "%s : Thread not created\r\n", __FUNCTION__);
    }
    // Create the event queue
    app_lorawan_data.queue = osMessageQueueNew(5, sizeof(EventMessage_t), NULL);
    if(app_lorawan_data.queue == NULL)
    {
        APP_LORAWAN_TLOG(LOGI, "%s : Queue not created\r\n", __FUNCTION__);
    }
    // Subscribe to the event
    event_handler_subscribe(EVENT_TYPE_NN_END,  app_lorawan_data.queue);

    // creates a periodic timer:
    app_lorawan_data.tmr_send = osTimerNew(_app_lorawan_tmr_send_callback, osTimerPeriodic, NULL, NULL);
    osTimerStart(app_lorawan_data.tmr_send, MS_TO_TICKS(APP_TX_JOIN_DUTYCYCLE));

    APP_LORAWAN_TLOG(LOGI, "APP_VERSION= %02X.%02X.%02X.%02X\r\n", (uint8_t)(__APP_VERSION >> 24), (uint8_t)(__APP_VERSION >> 16), (uint8_t)(__APP_VERSION >> 8), (uint8_t)__APP_VERSION);
    //APP_LORAWAN_TLOG(LOGI, "MAC_VERSION= %02X.%02X.%02X.%02X\r\n", (uint8_t)(__LORA_MAC_VERSION >> 24), (uint8_t)(__LORA_MAC_VERSION >> 16), (uint8_t)(__LORA_MAC_VERSION >> 8), (uint8_t)__LORA_MAC_VERSION);

    APP_LORAWAN_TLOG(LOGI, "%s : Initialization Done, GetCurrentTime = %d\r\n", __FUNCTION__, TimerGetCurrentTime());

    return app_lorawan_data.thread_id;
}

/********************************************************************************
  * @brief Main App Initialize
  * @param flag : Flag to set
  * @retval None
********************************************************************************/
void app_lorawan_process_cb(uint32_t flag)
{
    // Process the callback and send a Flag to the thread
    switch(flag)
    {
        case FLAG_LORAWAN_MCPSDATACONFIRM:
            APP_LORAWAN_TLOG(LOGI, "Network Server \"ack\" an uplink data confirmed message transmission\n\r");
            break;
        case FLAG_LORAWAN_CONFIRMCLASS:
            APP_LORAWAN_TLOG(LOGI, "switch to class done\n\r");
            break;
        case FLAG_LORAWAN_RX:
            //APP_LORAWAN_TLOG(LOGI, "PACKET RECEIVED\n\r");
            break;
        case FLAG_LORAWAN_JOINED:
            #if( OVER_THE_AIR_ACTIVATION != 0 )
            APP_LORAWAN_TLOG(LOGI, "JOINED \n\r");
            #endif
            break;
        case FLAG_LORAWAN_CLOCKSYNC:
            IsClockSynched = true;
            break;
        case FLAG_LORAWAN_FRAG_AUTH:
            break;
        default:
            break;
    }
       
    osThreadFlagsSet(app_lorawan_data.thread_id, flag);
}

/********************************************************************************
  * @brief LoRaWAN App Thread
  * @param None
  * @retval None
********************************************************************************/
void _app_lorawan_thread(void *argument)
{ 
    EventMessage_t  message;

    for(;;)
    {
        // Stack analyzer
        app_lorawan_remaining_stack = osThreadGetStackSpace(app_lorawan_data.thread_id);
        switch(app_lorawan_data.state)
        {
            case APP_LORAWAN_STATE_INIT:
            {
                APP_LORAWAN_TLOG(LOGI, "%s : State Init\r\n", __FUNCTION__);
                // Initalize the info for LoRa
                LoraInfo_Init();
                /* Configure the Lora Stack*/
                if(LmHandlerInit(&LmHandlerCallbacks) != LORAMAC_HANDLER_SUCCESS)
                {
                    APP_LORAWAN_TLOG(LOGE, "%s : LmHandlerInit not successful\r\n", __FUNCTION__);
                }

                /*The LoRa-Alliance Compliance protocol package should always be initialized and activated.*/
                if(LmHandlerPackageRegister(PACKAGE_ID_COMPLIANCE, &LmhpComplianceParams) == LORAMAC_HANDLER_ERROR)
                {
                    APP_LORAWAN_TLOG(LOGE, "Package Register Error (PACKAGE_ID_COMPLIANCE)\n\r");
                }
                // Register multicast package
                if(LmHandlerPackageRegister(PACKAGE_ID_REMOTE_MCAST_SETUP, NULL)  == LORAMAC_HANDLER_ERROR)
                {
                    APP_LORAWAN_TLOG(LOGE, "Package Register Error (PACKAGE_ID_REMOTE_MCAST_SETUP)\n\r");
                }
                // Register Clock synchronisation package
                if(LmHandlerPackageRegister(PACKAGE_ID_CLOCK_SYNC, NULL)  == LORAMAC_HANDLER_ERROR)
                {
                    APP_LORAWAN_TLOG(LOGE, "Package Register Error (PACKAGE_ID_CLOCK_SYNC)\n\r");
                }
                // Register Fragmentation package
                if(LmHandlerPackageRegister(PACKAGE_ID_FRAGMENTATION, &FragmentationParams)  == LORAMAC_HANDLER_ERROR)
                {
                    APP_LORAWAN_TLOG(LOGE, "Package Register Error (PACKAGE_ID_FRAGMENTATION)\n\r");
                }
                // Configure the LoRaMAC
                LmHandlerConfigure(&LmHandlerParams);
                // Try to join the network
                LmHandlerJoin(ACTIVATION_TYPE_OTAA);
                app_lorawan_data.state = APP_LORAWAN_STATE_IDLE;
            }
            break;
            case APP_LORAWAN_STATE_IDLE:
            {
                //APP_LORAWAN_TLOG(LOGI, "%s :State IDLE\r\n", __FUNCTION__);
                // Wait until the send timer elapsed or an event process is requested
                osThreadFlagsWait(FLAG_LORAWAN_SEND|FLAG_LORAWAN_PROCESS|FLAG_LORAWAN_FRAG_AUTH, osFlagsWaitAny | osFlagsNoClear, osWaitForever);
                if((osThreadFlagsGet() & FLAG_LORAWAN_SEND) == FLAG_LORAWAN_SEND)
                {
                    osThreadFlagsClear(FLAG_LORAWAN_SEND);
                    app_lorawan_data.state = APP_LORAWAN_STATE_SEND;
                    osTimerStop(app_lorawan_data.tmr_send);
                    osTimerStart(app_lorawan_data.tmr_send, MS_TO_TICKS(APP_TX_DUTYCYCLE));
                }
                else if((osThreadFlagsGet() & FLAG_LORAWAN_PROCESS) == FLAG_LORAWAN_PROCESS)
                {
                    osThreadFlagsClear(FLAG_LORAWAN_PROCESS);
                    app_lorawan_data.state = APP_LORAWAN_STATE_PROCESS;
                }
                else if((osThreadFlagsGet() & FLAG_LORAWAN_FRAG_AUTH) == FLAG_LORAWAN_FRAG_AUTH)
                {
                    osThreadFlagsClear(FLAG_LORAWAN_FRAG_AUTH);
                    app_lorawan_data.state = APP_LORAWAN_STATE_FRAG_AUTH;
                }
            }
            break;
            case APP_LORAWAN_STATE_SEND:
            {
                //APP_LORAWAN_TLOG(LOGI, "%s : State Send\r\n", __FUNCTION__);
                // Prepare all messages contain in the queue
                uint8_t nbMessage = osMessageQueueGetCount(app_lorawan_data.queue);
                if(nbMessage)
                {
                    uint8_t* ptr = tx_data_buf;
                    uint8_t size = 0;
                    for(int i=0; i<nbMessage; i++)
                    {
                        if(osMessageQueueGet(app_lorawan_data.queue, &message, NULL, 0) == osOK) 
                        {
                            if(message.type == EVENT_TYPE_NN_END)
                            {
                                memcpy(ptr, &message.data_int, message.size);
                                ptr = ptr + message.size;
                                size += message.size;
                                memcpy(ptr, &message.timeout, 4);
                                ptr = ptr + 4;
                                size += 4;
                            }
                        }
                        else
                        {
                            APP_LORAWAN_TLOG(LOGE, "%s : Message Queue error\n\r", __FUNCTION__);
                        }
                    }
                    // Send data on Port 1
                    LmHandlerAppData_t appData =
                    {
                        .Buffer = tx_data_buf,
                        .BufferSize = size,
                        .Port = 1
                    };
                    APP_LORAWAN_TLOG(LOGI, "%s : New Message To Send\n\r", __FUNCTION__);
                    if(_app_lorawan_send(&appData))
                    {
                        //osTimerStop(app_lorawan_data.tmr_send);
                        //osTimerStart(app_lorawan_data.tmr_send, MS_TO_TICKS(APP_TX_DUTYCYCLE));
                    }
                }
                app_lorawan_data.state = APP_LORAWAN_STATE_IDLE;
            }
            break;
            case APP_LORAWAN_STATE_PROCESS:
            {
                //APP_LORAWAN_TLOG(LOGI, "%s : State Process\r\n", __FUNCTION__);
                // Process event
                LmHandlerProcess();
                app_lorawan_data.state = APP_LORAWAN_STATE_IDLE;
            }
            break;
            case APP_LORAWAN_STATE_FRAG_AUTH:
            {
                // After an update, the CRC of the received packet must be transmitted
                tx_data_buf[0] = 0x05; // FragDataBlockAuthReq
                tx_data_buf[1] = FileRxCrc & 0x000000FF;
                tx_data_buf[2] = (FileRxCrc >> 8) & 0x000000FF;
                tx_data_buf[3] = (FileRxCrc >> 16) & 0x000000FF;
                tx_data_buf[4] = (FileRxCrc >> 24) & 0x000000FF;

                /* Send FragAuthReq */
                LmHandlerAppData_t appData =
                {
                    .Buffer = tx_data_buf,
                    .BufferSize = 5,
                    .Port = 201
                };
                LmHandlerSend(&appData, LORAMAC_HANDLER_UNCONFIRMED_MSG, NULL, false);

                app_lorawan_data.state = APP_LORAWAN_STATE_IDLE;
            }
            break;
        }
    }
}

/********************************************************************************
  * @brief LoRaWAN Send function
  * @param appData : Data to send
  * @retval Success
********************************************************************************/
uint8_t _app_lorawan_send(LmHandlerAppData_t* appData)
{
    LmHandlerErrorStatus_t status = LORAMAC_HANDLER_ERROR;
    DeviceClass_t deviceClass = CLASS_A;

    if (IsClockSynched == false)    /* we request AppTimeReq to allow FUOTA */
    {
        status = LmhpClockSyncAppTimeReq();
        return 1;
    }
    LmHandlerGetCurrentClass(&deviceClass);
    if (deviceClass == CLASS_C)
    {
        APP_LORAWAN_TLOG(LOGI, "%s : CLASS C \r\n", __FUNCTION__);
        return 1;
    }
    /* Send random packet */
    status = LmHandlerSend(appData, LORAMAC_HANDLER_UNCONFIRMED_MSG, NULL, false);
    switch(status)
    {
        case LORAMAC_HANDLER_ERROR: 
            APP_LORAWAN_TLOG(LOGI, "%s : LORAMAC_HANDLER_ERROR \r\n", __FUNCTION__, status);
            break;
        case LORAMAC_HANDLER_BUSY_ERROR: 
            APP_LORAWAN_TLOG(LOGI, "%s : LORAMAC_HANDLER_BUSY_ERROR \r\n", __FUNCTION__, status);
            break;
        case LORAMAC_HANDLER_NO_NETWORK_JOINED: 
            APP_LORAWAN_TLOG(LOGI, "%s : LORAMAC_HANDLER_NO_NETWORK_JOINED \r\n", __FUNCTION__, status);
            break;
        case LORAMAC_HANDLER_COMPLIANCE_RUNNING: 
            APP_LORAWAN_TLOG(LOGI, "%s : LORAMAC_HANDLER_COMPLIANCE_RUNNING \r\n", __FUNCTION__, status);
            break;
        case LORAMAC_HANDLER_CRYPTO_ERROR: 
            APP_LORAWAN_TLOG(LOGI, "%s : LORAMAC_HANDLER_CRYPTO_ERROR \r\n", __FUNCTION__, status);
            break;
        case LORAMAC_HANDLER_DUTYCYCLE_RESTRICTED: 
            APP_LORAWAN_TLOG(LOGI, "%s : LORAMAC_HANDLER_DUTYCYCLE_RESTRICTED \r\n", __FUNCTION__, status);
            break;
        default:
            break;
    }
    return 1;
}

/********************************************************************************
  * @brief Send Timer callback
  * @param argument : Not used
  * @retval None
********************************************************************************/
void _app_lorawan_tmr_send_callback(void *argument)
{
    osThreadFlagsSet(app_lorawan_data.thread_id, FLAG_LORAWAN_SEND); 
}

