/**
  ******************************************************************************
  * @file           : app_lorawan_cb.h
  * @brief          : Header for app_lorawan_cb.c file.
  *                   This file contains the definitions for app_lorawan_cb.h
  * @author         : Baptiste Solioz
  * @date           : 20. November 2020
  ******************************************************************************
  */

#ifndef __APP_LORAWAN_CB_H
#define __APP_LORAWAN_CB_H

#ifdef __cplusplus
extern "C" {
#endif

/* Private includes ----------------------------------------------------------*/
#include "app_lorawan.h"
#include "lorawan_version.h"
#include "lorawan_conf.h"


#include "LmHandler.h"
#include "LmhpCompliance.h"
#include "LmhpClockSync.h"
#include "LmhpRemoteMcastSetup.h"
#include "LmhpFragmentation.h"
#include "Commissioning.h"

// Specific callback related to the Basic/Advanced LoraMac stack
void app_lorawan_HasJoined(LmHandlerJoinParams_t *params);
void app_lorawan_RxData(LmHandlerAppData_t *AppData, LmHandlerRxParams_t *params);
void app_lorawan_TxData(LmHandlerTxParams_t *params);

// Common callback
void app_lorawan_ConfirmClass(DeviceClass_t Class);
uint8_t app_lorawan_GetBatteryLevel(void);
uint16_t app_lorawan_GetTemperatureLevel(void);
void app_lorawan_LoraMacProcessNotify(void);
void CERTIF_ProcessNotify(void);

// Callback related to the Fragmented package
uint8_t app_lorawan_FragDecoderErase(uint32_t addr, uint32_t size);
uint8_t app_lorawan_FragDecoderWrite(uint32_t addr, uint8_t *data, uint32_t size);
uint8_t app_lorawan_FragDecoderRead(uint32_t addr, uint8_t *data, uint32_t size);
void app_lorawan_OnFragProgress(uint16_t fragCounter, uint16_t fragNb, uint8_t fragSize, uint16_t fragNbLost);
void app_lorawan_OnFragDone(int32_t status, uint32_t size);

/* Exported variables --------------------------------------------------------*/

#ifdef __cplusplus
}
#endif

#endif // __APP_LORAWAN_CB_H