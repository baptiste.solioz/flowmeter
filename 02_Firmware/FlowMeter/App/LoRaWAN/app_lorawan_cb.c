/**
  ******************************************************************************
  * @file           : app_lorawan_cb.c
  * @brief          : Handle the callback from the LoRaWAN middleware
  *                   
  * @author         : Baptiste Solioz
  * @date           : 30. October 2020
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "app_lorawan_cb.h"
#include "mdw_mem.h"
#include "mdw_flash.h"

/* Private typedef -----------------------------------------------------------*/
static volatile bool IsMcSessionStarted = false;
static volatile bool IsClockSynched = false;

static uint8_t* _fuota_data;
static uint8_t* _fuota_data_buf1;
static uint8_t* _fuota_data_buf2;
uint32_t FileRxCrc = 0;

/* Private function -----------------------------------------------------------*/
static uint32_t Crc32(uint8_t *buffer, uint16_t length);

/********************************************************************************
  * @brief Callback when the server is joined
  * @param params : Join parameters
  * @retval None
********************************************************************************/
void app_lorawan_HasJoined(LmHandlerJoinParams_t *params)
{
    if (params != NULL)
    {
        if (params->Status == LORAMAC_HANDLER_SUCCESS)
        {

            APP_LORAWAN_TLOG(LOGI, "\r\n###### = JOINED = ");
            if (params->Mode == ACTIVATION_TYPE_ABP)
            {
                APP_LORAWAN_LOG(LOGI, "ABP ======================\r\n");
            }
            else
            {
                APP_LORAWAN_LOG(LOGI, "OTAA =====================\r\n");
            }
        }
        else
        {
            APP_LORAWAN_TLOG(LOGI, "\r\n###### = JOIN FAILED\r\n");
        }
    }
}

/********************************************************************************
  * @brief Callback when a message is received
  * @param AppData : Data received
  * @param params  : Rx parameters
  * @retval None
********************************************************************************/
void app_lorawan_RxData(LmHandlerAppData_t *AppData, LmHandlerRxParams_t *params)
{
    // Process callback in thread
    app_lorawan_process_cb(FLAG_LORAWAN_RX);
    APP_LORAWAN_TLOG(LOGI, "New Packet received on port %d\n\r",AppData->Port);
    switch (AppData->Port)
    {
        // Message in Port 3 -> switch class
        case 3:
            /*this port switches the class*/
            if (AppData->BufferSize == 1)
            {
                switch (AppData->Buffer[0])
                {
                    case 0: LmHandlerRequestClass(CLASS_A); break;
                    case 1: LmHandlerRequestClass(CLASS_B); break;
                    case 2: LmHandlerRequestClass(CLASS_C); break;
                    default:
                        break;
                }
            }
            break;
        case LORAWAN_APP_PORT:
            // App port -> do nothing
            if (AppData->BufferSize == 1)
            {
            }
            break;
        case 202:
            // Clock synchronisation port
            app_lorawan_process_cb(FLAG_LORAWAN_CLOCKSYNC);
            break;
        default:
            break;
    }
}

/********************************************************************************
  * @brief Callback when the class is switched
  * @param Class : New class
  * @retval None
********************************************************************************/
void app_lorawan_ConfirmClass(DeviceClass_t Class)
{
    APP_LORAWAN_TLOG(LOGI, "%s called\r\n", __FUNCTION__);
    // Process callback in thread
    app_lorawan_process_cb(FLAG_LORAWAN_CONFIRMCLASS);
    switch (Class)
    {
        default:
        case CLASS_A:
        {
            // End of transmission
            IsMcSessionStarted = false;
            break;
        }
        case CLASS_B:
        {
            /* Inform the server as soon as possible that the end-device has switched to ClassB */
            LmHandlerAppData_t appData =
            {
                .Buffer = NULL,
                .BufferSize = 0,
                .Port = 0
            };
            LmHandlerSend(&appData, LORAMAC_HANDLER_UNCONFIRMED_MSG, NULL, true);
            IsMcSessionStarted = true;
            break;
        }
        case CLASS_C:
        {
            // Start of transmission
            IsMcSessionStarted = true;
            break;
        }
    }
}

/********************************************************************************
  * @brief Callback when a message is transmitted
  * @param params : LoraMac handler parameter
  * @retval None
********************************************************************************/
void app_lorawan_TxData(LmHandlerTxParams_t *params)
{
    //APP_LORAWAN_TLOG(LOGI, "%s called\r\n", __FUNCTION__);
    // Display debug messages
    if (params->IsMcpsConfirm == 0)
    {
        APP_LORAWAN_TLOG(LOGI, "\r\n....... OnTxData (Mlme) .......\r\n");
        return;
    }
    if(params->AppData.Port != 1)
    {
        APP_LORAWAN_TLOG(LOGI, "OnTxData : Port = %d, Type = ", params->AppData.Port);
    }
    switch(params->AppData.Port)
    {
        case 0 : APP_LORAWAN_LOG(LOGI, "ACK"); break;
        case 1 : /*APP_LORAWAN_TLOG(LOGI, "APP MSG");*/ break;
        case 3 : APP_LORAWAN_LOG(LOGI, "CLASS CHG"); break;
        case 200 : APP_LORAWAN_LOG(LOGI, "PACK MULTICAST SETUP"); break;
        case 201 : APP_LORAWAN_LOG(LOGI, "PACK FRAG DATA"); break;
        case 202 : APP_LORAWAN_LOG(LOGI, "PACK CLK SYNC"); break;
        default : APP_LORAWAN_LOG(LOGI, "UNKNOWN"); break;
    }
    if(params->AppData.Port != 1)
        APP_LORAWAN_LOG(LOGI, "\n\r");
}
/********************************************************************************
  * @brief This function return the battery level (Must be implemented but not used)
  * @param none
  * @retval the battery level  1 (very low) to 254 (fully charged)
********************************************************************************/
uint8_t app_lorawan_GetBatteryLevel(void)
{
    uint8_t batteryLevel = 0;

    batteryLevel = 254;

    return batteryLevel;  /* 1 (very low) to 254 (fully charged) */
}

/********************************************************************************
  * @brief This function return the temperature (Must be implemented but not used)
  * @param none
  * @retval the temperature
********************************************************************************/
uint16_t app_lorawan_GetTemperatureLevel(void)
{
  return (uint16_t) 25;
}

/********************************************************************************
  * @brief Callback when it is necessary to process
  * @param none
  * @retval none
********************************************************************************/
void app_lorawan_LoraMacProcessNotify(void)
{
    app_lorawan_process_cb(FLAG_LORAWAN_PROCESS);
}

/********************************************************************************
  * @brief Callback called to erase memory
  * @param addr : Addresse of the fragment
  * @param size : Size of data
  * @retval Success or Not
********************************************************************************/
uint8_t app_lorawan_FragDecoderErase(uint32_t addr, uint32_t size)
{
    APP_LORAWAN_TLOG(LOGI, "FragDecoderErase : Try\r\n");

    /* GET MEMORY BUFFER FROM SHARED MEMORY */
    _fuota_data_buf1 = (uint8_t*) mdw_mem_get(16384);
    _fuota_data_buf2 = (uint8_t*) mdw_mem_get(16384);

    if(_fuota_data_buf1 < _fuota_data_buf2)
    {
        _fuota_data = _fuota_data_buf1;
    }
    else
    {
        _fuota_data = _fuota_data_buf2;
    }
    APP_LORAWAN_TLOG(LOGI, "FragDecoderErase : buffer obtained\r\n");

    return 0;
}

/********************************************************************************
  * @brief Callback called a fragement is ready to be written in memory
  * @param addr : Addresse of the fragment
  * @param data : Data received
  * @param size : Size of data
  * @retval Success or Not
********************************************************************************/
uint8_t app_lorawan_FragDecoderWrite(uint32_t addr, uint8_t *data, uint32_t size)
{
    APP_LORAWAN_TLOG(LOGI, "FragDecoderWrite size = %d\r\n", size);
    // Copy fragment in buffer
    if(addr+size <= (16384 + 512)*2)
	    memcpy((_fuota_data + addr), data, size);
    else
        APP_LORAWAN_TLOG(LOGE, "FragDecoderWrite Size too high\r\n");
	return 0; // Success
}

/********************************************************************************
  * @brief Callback that allows to retrieve a fragment already written in memory
  * @param addr : Addresse of the fragment to retrieve
  * @param data : Data to retrieve
  * @param size : Size of data
  * @retval Success or Not
********************************************************************************/
uint8_t app_lorawan_FragDecoderRead(uint32_t addr, uint8_t *data, uint32_t size)
{
	APP_LORAWAN_TLOG(LOGI, "FragDecoderRead size = %d\r\n", size);
    // Retrieve fragment from buffer
    memcpy(data, (_fuota_data + addr), size);
	return 0; // Success
}

/********************************************************************************
  * @brief Callback called when a new fragment is received
  * @param fragCounter : No of the fragment
  * @param fragNb : Total number of fragment
  * @param fragSize : Size of fragment
  * @param fragNbLost : Nb of fragment lost
  * @retval None
********************************************************************************/
void app_lorawan_OnFragProgress(uint16_t fragCounter, uint16_t fragNb, uint8_t fragSize, uint16_t fragNbLost)
{
	APP_LORAWAN_TLOG(LOGI, "\r\n");
	APP_LORAWAN_LOG(LOGI, "....... FRAG_DECODER in Progress .......\r\n");
	APP_LORAWAN_LOG(LOGI, "RECEIVED    : %5d / %5d Fragments\r\n", fragCounter, fragNb);
	APP_LORAWAN_LOG(LOGI, "              %5d / %5d Bytes\r\n", fragCounter * fragSize, fragNb * fragSize);
	APP_LORAWAN_LOG(LOGI, "LOST        :       %7d Fragments\r\n\r\n", fragNbLost);
}

/********************************************************************************
  * @brief Callback called when all fragments are received
  * @param status : Result of the fragmentation
  * @param size : Size of the data
  * @retval None
********************************************************************************/
void app_lorawan_OnFragDone(int32_t status, uint32_t size)
{
    HAL_StatusTypeDef ret;
    
	FileRxCrc = Crc32(_fuota_data, size);
    // Erase flash memory (OLD MODEL)
    ret = mdw_flash_erase((void*)0x8031800, 32768);
    if(ret != HAL_OK)
    {
        APP_LORAWAN_TLOG(LOGI, "Error Erase\r\n");
    }
    // Write the new model received in flash
    ret = mdw_flash_write((void*)0x8031800, (const void *) _fuota_data, 32768);
    if(ret != HAL_OK)
    {
        APP_LORAWAN_TLOG(LOGI, "Error Write\r\n");
    }
    APP_LORAWAN_TLOG(LOGI, "Memory write completed\r\n");
    app_lorawan_process_cb(FLAG_LORAWAN_FRAG_AUTH);
    // Finalize transmission by returning in class A
    LmHandlerRequestClass(CLASS_A);

    _fuota_data = NULL;
    /* RELEASE MEMORY BLOC */
    mdw_mem_release(_fuota_data_buf2);
    mdw_mem_release(_fuota_data_buf1);

	APP_LORAWAN_TLOG(LOGI, "\r\n....... FRAG_DECODER Finished .......\r\n");
	APP_LORAWAN_TLOG(LOGI, "STATUS      : %d\r\n", status);
	APP_LORAWAN_TLOG(LOGI, "CRC         : %08X\r\n\r\n", FileRxCrc);
}

/********************************************************************************
  * @brief Certif Callback (NOT USED but must be implemented)
  * @param None
  * @retval None
********************************************************************************/
void CERTIF_ProcessNotify(void)
{
    APP_LORAWAN_TLOG(LOGI, "CERTIF PROCESS NOTIFIY CALLBACK\r\n");
}

/********************************************************************************
  * @brief CRC calculation
  * @param buffer data used for the CRC calculation
  * @param length data length
  * @retval CRC value
********************************************************************************/
static uint32_t Crc32(uint8_t *buffer, uint16_t length)
{
  // The CRC calculation follows CCITT - 0x04C11DB7
  const uint32_t reversedPolynom = 0xEDB88320;

  // CRC initial value
  uint32_t crc = 0xFFFFFFFF;

  if (buffer == NULL)
  {
    return 0;
  }

  for (uint16_t i = 0; i < length; ++i)
  {
    crc ^= (uint32_t)buffer[i];
    for (uint16_t i = 0; i < 8; i++)
    {
      crc = (crc >> 1) ^ (reversedPolynom & ~((crc & 0x01) - 1));
    }
  }

  return ~crc;
}
