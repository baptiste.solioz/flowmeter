/**
  ******************************************************************************
  * @file           : app_lorawan.h
  * @brief          : Header for app_lorawan.c file.
  *                   This file contains the definitions for app_lorawan.h
  * @author         : Baptiste Solioz
  * @date           : 30. October 2020
  ******************************************************************************
  */

#ifndef __APP_LORAWAN_H
#define __APP_LORAWAN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Private includes ----------------------------------------------------------*/
#include "app_conf.h"
#include "event_handler.h"
#include "cmsis_os2.h"
#include "lorawan_version.h"
#include "lorawan_conf.h"
#include "app_lorawan_cb.h"

#include "LmHandler.h"
#include "LmhpCompliance.h"
#include "LmhpClockSync.h"
#include "LmhpRemoteMcastSetup.h"
#include "LmhpFragmentation.h"
#include "Commissioning.h"

/* LOG Definition ------------------------------------------------------------*/
#if defined (APP_LORAWAN_LOG_ENABLED) && (APP_LORAWAN_LOG_ENABLED == 1)
#define APP_LORAWAN_LOG(VL,...)     do{ {UTIL_ADV_TRACE_COND_FSend(VL, T_REG_OFF, TS_OFF, __VA_ARGS__);} }while(0);
#define APP_LORAWAN_TLOG(VL,...)    do{ {UTIL_ADV_TRACE_COND_FSend(VL, T_REG_OFF, TS_ON, __VA_ARGS__);} }while(0); //with timestamp
#define APP_LORAWAN_PLOG(VL,...)    do{ } while( UTIL_ADV_TRACE_OK != UTIL_ADV_TRACE_COND_FSend(VL, T_REG_OFF, TS_OFF, __VA_ARGS__) ) //Polling Mode
#elif defined (APP_LORAWAN_LOG_ENABLED) && (APP_LORAWAN_LOG_ENABLED == 0) /* APP_LOG disabled */
#define APP_LORAWAN_LOG(...)
#define APP_LORAWAN_TLOG(...)
#define APP_LORAWAN_PLOG(...)
#else
#error "APP_LORAWAN_LOG_ENABLED not defined or out of range <0,1>"
#endif

#define     LORAWAN_DEFAULT_CLASS   CLASS_A     //LoRaWAN default endNode class port

/* Private constants ---------------------------------------------------------*/
#define     FLAG_LORAWAN_SEND               0x0001
#define     FLAG_LORAWAN_PROCESS            0x0002
#define     FLAG_LORAWAN_MCPSDATACONFIRM    0x0004
#define     FLAG_LORAWAN_CONFIRMCLASS       0x0008
#define     FLAG_LORAWAN_RX                 0x0010
#define     FLAG_LORAWAN_JOINED             0x0020
#define     FLAG_LORAWAN_CLOCKSYNC          0x0040
#define     FLAG_LORAWAN_FRAG_AUTH          0x0080

/* Private definitions -------------------------------------------------------*/
typedef enum 
{ 
    APP_LORAWAN_STATE_INIT, 
    APP_LORAWAN_STATE_IDLE,
    APP_LORAWAN_STATE_SEND,
    APP_LORAWAN_STATE_PROCESS,
    APP_LORAWAN_STATE_FRAG_AUTH
} App_LoRaWAN_State;

typedef struct {
    osThreadId_t          thread_id;            ///< Thread Id
    osThreadAttr_t        thread_attributes;    ///< Thread Attributes 
    App_LoRaWAN_State     state;                ///< State Machine
    osMessageQueueId_t    queue;
    LmHandlerAppData_t    tx_data;
    osTimerId_t           tmr_send;
} App_LoRaWAN_Attr_t;

/* Exported functions prototypes ---------------------------------------------*/
osThreadId_t app_lorawan_init();
void app_lorawan_process_cb(uint32_t flag);

/* Exported variables --------------------------------------------------------*/
uint16_t    app_lorawan_remaining_stack;

#ifdef __cplusplus
}
#endif

#endif // __APP_LORAWAN_H