/**
  ******************************************************************************
  * @file           : event_handler.h
  * @brief          : Header for event_handler.c file.
  *                   This file contains the definitions for event_handler.h
  * @author         : Baptiste Solioz
  * @date           : 16. October 2020
  ******************************************************************************
  */

#ifndef __EVENT_HANDLER_H
#define __EVENT_HANDLER_H

#ifdef __cplusplus
extern "C" {
#endif

/* Private includes ----------------------------------------------------------*/
#include "app_conf.h"
#include "cmsis_os2.h"

/* LOG Definition ------------------------------------------------------------*/
#if defined (EVENT_LOG_ENABLED) && (EVENT_LOG_ENABLED == 1)
#define EVENT_LOG(VL,...)     do{ {UTIL_ADV_TRACE_COND_FSend(VL, T_REG_OFF, TS_OFF, __VA_ARGS__);} }while(0);
#define EVENT_TLOG(VL,...)    do{ {UTIL_ADV_TRACE_COND_FSend(VL, T_REG_OFF, TS_ON, __VA_ARGS__);} }while(0); //with timestamp
#define EVENT_PLOG(VL,...)    do{ } while( UTIL_ADV_TRACE_OK != UTIL_ADV_TRACE_COND_FSend(VL, T_REG_OFF, TS_OFF, __VA_ARGS__) ) //Polling Mode
#elif defined (EVENT_LOG_ENABLED) && (EVENT_LOG_ENABLED == 0) /* APP_LOG disabled */
#define EVENT_LOG(...)
#define EVENT_TLOG(...)
#define EVENT_PLOG(...)
#else
#error "EVENT_LOG_ENABLED not defined or out of range <0,1>"
#endif

/* Private macro -------------------------------------------------------------*/
#define     IS_IRQ()             (__get_IPSR() != 0U)

/* Exported types ------------------------------------------------------------*/
typedef enum
{
    EVENT_TYPE_NOTHING      = 0,
    EVENT_TYPE_ADC_START    = 10,
    EVENT_TYPE_ADC_END      = 11,
    EVENT_TYPE_ACC_MEAS     = 21,
    EVENT_TYPE_ACC_INT      = 22,
    EVENT_TYPE_ACC_START    = 23,
    EVENT_TYPE_ACC_STOP     = 24,
    EVENT_TYPE_FFT_START    = 31,
    EVENT_TYPE_FFT_END      = 32,
    EVENT_TYPE_TIMER_START  = 41,
    EVENT_TYPE_TIMER_STOP   = 42,
    EVENT_TYPE_NN_END       = 51,
} EventType_t;
typedef struct
{
    osThreadId_t        source;     // Thread source, 0 if from IRQ
    EventType_t         type;       // Event Type
    void*               data;       // Pointer on data
    int16_t             data_int;   // Direct data transmit
    uint16_t            size;       // Size of the data
    osTimerId_t         timer_id;   // Timer ID used by TimerHandler
    uint32_t            timeout;    // Timeout value or Timestamp of the measures
} EventMessage_t;

/* Exported functions prototypes ---------------------------------------------*/
void event_handler_init();
int8_t event_handler_send(EventMessage_t* message);
int8_t event_handler_subscribe(EventType_t eventType, osMessageQueueId_t queue);

#ifdef __cplusplus
}
#endif

#endif // __EVENT_HANDLER_H