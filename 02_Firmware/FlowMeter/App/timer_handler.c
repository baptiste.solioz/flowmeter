/**
  ******************************************************************************
  * @file           : timer_handler.c
  * @brief          : Create a thread to handle the Timer Start/Stop by ISR
  *                   
  * @author         : Baptiste Solioz
  * @date           : 12. November 2020
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "timer_handler.h"

/* Private typedef -----------------------------------------------------------*/
typedef struct {
  osThreadId_t          thread_id;      ///< Thread Id
  osThreadAttr_t        thread_attributes; ///< Thread Attributes 
  Timer_Handler_State   state;              ///< State Machine
  osMessageQueueId_t    queue;
} Timer_Handler_Attr_t;

/* Private variables ---------------------------------------------------------*/
Timer_Handler_Attr_t      timer_handler_data;


/********************************************************************************
  * @brief Timer Handler Initialize
  * @param None
  * @retval Thread Id
********************************************************************************/
osThreadId_t timer_handler_init()
{
    timer_handler_data.thread_attributes.name = "Timer Handler Task";
    timer_handler_data.thread_attributes.priority = (osPriority_t) osPriorityRealtime;
    timer_handler_data.thread_attributes.stack_size = 256 * 4;
    // Initialize state
    timer_handler_data.state = TIMER_HANDLER_STATE_INIT;
    // Create thread
    timer_handler_data.thread_id = osThreadNew(timer_handler_thread, NULL, &(timer_handler_data.thread_attributes));
    if(timer_handler_data.thread_id == NULL)
    {
        TIMER_TLOG(LOGE, "%s : Thread not created\n\r", __FUNCTION__);
    }
    // Create Event queue
    timer_handler_data.queue = osMessageQueueNew(10, sizeof(EventMessage_t), NULL);
    if(timer_handler_data.queue == NULL)
    {
        TIMER_TLOG(LOGE, "%s : Queue not created\n\r", __FUNCTION__);
    }
    // Subscribe to the events
    event_handler_subscribe(EVENT_TYPE_TIMER_START,  timer_handler_data.queue);
    event_handler_subscribe(EVENT_TYPE_TIMER_STOP,  timer_handler_data.queue);

    TIMER_TLOG(LOGI, "%s : Initialization Done\n\r", __FUNCTION__);

    return timer_handler_data.thread_id;
}

/********************************************************************************
  * @brief Timer Handler Thread
  * @param argument : not used
  * @retval None
********************************************************************************/
void timer_handler_thread(void *argument)
{ 
    EventMessage_t  message;
    for(;;)
    {
        switch(timer_handler_data.state)
        {
            case TIMER_HANDLER_STATE_INIT:
            {
                TIMER_TLOG(LOGI, "%s : State Init\n\r", __FUNCTION__);
                timer_handler_data.state = TIMER_HANDLER_STATE_IDLE;
            }
            break;
            case TIMER_HANDLER_STATE_IDLE:
            {
                // Wait on an event to start timer or stop it
                if(osMessageQueueGet(timer_handler_data.queue, &message, NULL, osWaitForever) == osOK) 
                {
                    if(message.type == EVENT_TYPE_TIMER_START)
                    {
                        timer_handler_data.state = TIMER_HANDLER_STATE_START;
                    }
                    else if(message.type == EVENT_TYPE_TIMER_STOP)
                    {
                        timer_handler_data.state = TIMER_HANDLER_STATE_STOP;
                    }
                }
            }
            break;
            case TIMER_HANDLER_STATE_START:
            {
                // Start timer
                TIMER_TLOG(LOGI, "Start Timer from ISR %d\n\r",message.timer_id);   
                osTimerStart(message.timer_id, MS_TO_TICKS(message.timeout));
                timer_handler_data.state = TIMER_HANDLER_STATE_IDLE;
            }
            break;
            case TIMER_HANDLER_STATE_STOP:
            {
                // Start timer
                TIMER_TLOG(LOGI, "Stop Timer from ISR %d\n\r",message.timer_id);
                osTimerStop(message.timer_id);
                timer_handler_data.state = TIMER_HANDLER_STATE_IDLE;
            }
            break;
        }
    }
}
