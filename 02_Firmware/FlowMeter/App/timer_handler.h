/**
  ******************************************************************************
  * @file           : timer_handler.h
  * @brief          : Header for timer_handler.c file.
  *                   This file contains the definitions for timer_handler.h
  * @author         : Baptiste Solioz
  * @date           : 12. November 2020
  ******************************************************************************
  */

#ifndef __TIMER_HANDLER_H
#define __TIMER_HANDLER_H

#ifdef __cplusplus
extern "C" {
#endif

/* Private includes ----------------------------------------------------------*/
#include "app_conf.h"
#include "event_handler.h"
#include "cmsis_os2.h"

/* LOG Definition ------------------------------------------------------------*/
#if defined (TIMER_LOG_ENABLED) && (TIMER_LOG_ENABLED == 1)
#define TIMER_LOG(VL,...)     do{ {UTIL_ADV_TRACE_COND_FSend(VL, T_REG_OFF, TS_OFF, __VA_ARGS__);} }while(0);
#define TIMER_TLOG(VL,...)    do{ {UTIL_ADV_TRACE_COND_FSend(VL, T_REG_OFF, TS_ON, __VA_ARGS__);} }while(0); //with timestamp
#define TIMER_PLOG(VL,...)    do{ } while( UTIL_ADV_TRACE_OK != UTIL_ADV_TRACE_COND_FSend(VL, T_REG_OFF, TS_OFF, __VA_ARGS__) ) //Polling Mode
#elif defined (TIMER_LOG_ENABLED) && (TIMER_LOG_ENABLED == 0) /* APP_LOG disabled */
#define TIMER_LOG(...)
#define TIMER_TLOG(...)
#define TIMER_PLOG(...)
#else
#error "TIMER_LOG_ENABLED not defined or out of range <0,1>"
#endif

/* Exported types ------------------------------------------------------------*/


/* Exported constants --------------------------------------------------------*/
#define     FLAG_TIMER_HANDLER_START        0x01
#define     FLAG_TIMER_HANDLER_STOP         0x02

/* Exported macro ------------------------------------------------------------*/


/* Exported functions prototypes ---------------------------------------------*/
osThreadId_t timer_handler_init();
void timer_handler_thread(void *argument);

/* Private defines -----------------------------------------------------------*/
typedef enum 
{ 
    TIMER_HANDLER_STATE_INIT, 
    TIMER_HANDLER_STATE_IDLE, 
    TIMER_HANDLER_STATE_START,
    TIMER_HANDLER_STATE_STOP 
} Timer_Handler_State;

#ifdef __cplusplus
}
#endif

#endif // __TIMER_HANDLER_H