/**
  ******************************************************************************
  * @file           : app_vib.c
  * @brief          : Create a thread to handle the fft
  *                   
  * @author         : Baptiste Solioz
  * @date           : 15. October 2020
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "app_vib.h"

/* Private constants ---------------------------------------------------------*/
#define     FLAG_END_ADC  0x0001

#define     ADC_SAMPLES     8192

/* Private variables ---------------------------------------------------------*/
static App_Vib_Attr_t      _app_vib_data;

static uint16_t*            _adc_buffer;

/* Private functions ---------------------------------------------------------*/
static void _app_vib_thread(void *argument);
static void _app_vib_adc_callback();

/********************************************************************************
  * @brief Vibration Sensor Initialize
  * @param None
  * @retval Thread Id
********************************************************************************/
osThreadId_t app_vib_init()
{
    _app_vib_data.thread_attributes.name = "Vibration Sensor Task";
    _app_vib_data.thread_attributes.priority = (osPriority_t) osPriorityNormal;
#if (APP_VIB_LOG_ENABLED == 1)
    _app_vib_data.thread_attributes.stack_size = 256 * 5;
#else
    _app_vib_data.thread_attributes.stack_size = 256 * 5;
#endif
    // Initalize state
    _app_vib_data.state = APP_VIB_STATE_INIT;
    // Create thread
    _app_vib_data.thread_id = osThreadNew(_app_vib_thread, NULL, &_app_vib_data.thread_attributes);
    if(_app_vib_data.thread_id == NULL)
    {
        APP_VIB_TLOG(LOGE, "%s : Thread not created\n\r", __FUNCTION__);
    }
    // Create Event queue
    _app_vib_data.queue = osMessageQueueNew(5, sizeof(EventMessage_t), NULL);
    if(_app_vib_data.queue == NULL)
    {
        APP_VIB_TLOG(LOGE, "%s : Queue not created\n\r", __FUNCTION__);
    }
    // Subscribe to the event
    event_handler_subscribe(EVENT_TYPE_ADC_START,  _app_vib_data.queue);
    event_handler_subscribe(EVENT_TYPE_FFT_END,  _app_vib_data.queue);

    APP_VIB_TLOG(LOGI, "%s : Initialization Done %d\n\r", __FUNCTION__, _app_vib_data.thread_id);
    return _app_vib_data.thread_id;
}

/********************************************************************************
  * @brief Vibration sensor Thread
  * @param argument : Not used
  * @retval None
********************************************************************************/
void _app_vib_thread(void *argument)
{ 
    EventMessage_t  message;
    osDelay(MS_TO_TICKS(200));
    for (;;)
    {
        // Stack analyzer
        app_vib_remaining_stack = osThreadGetStackSpace(_app_vib_data.thread_id);
        switch(_app_vib_data.state)
        {
            case APP_VIB_STATE_INIT:
            {
                APP_VIB_TLOG(LOGI, "%s : State Init\n\r", __FUNCTION__);
#if (ALWAYS_MEASURE_VIB == 1)
                // Mode always measure
                _app_vib_data.state = APP_VIB_STATE_ALWAYS;
#else
                // Normal mode
                _app_vib_data.state = APP_VIB_STATE_IDLE;
#endif
            }
            break;
            case APP_VIB_STATE_IDLE:
            {
                APP_VIB_TLOG(LOGI, "%s : State Idle\n\r", __FUNCTION__);
                // Wait until a message arrives
                if(osMessageQueueGet(_app_vib_data.queue, &message, NULL, osWaitForever) == osOK) 
                {
                    if(message.type == EVENT_TYPE_ADC_START)
                    {
                        osDelay(MS_TO_TICKS(1000));
                        APP_VIB_TLOG(LOGI, "%s : Start conv at %d\n\r", __FUNCTION__ , HAL_GetTick());
                        // Retrieve memory pointer for shared memory
                        _adc_buffer = (uint16_t*) mdw_mem_get((ADC_SAMPLES+5)*2); // Need 16384 + 10 Bytes
                        if(_adc_buffer != NULL)
                        {
                            mdw_adc_enable();               // Enable sensor supply
                            osDelay(MS_TO_TICKS(1000));     // Wait 1s for stabilization
                            // Start measure with callback
                            mdw_adc_start((uint32_t*)_adc_buffer, ADC_SAMPLES+5, (void*)_app_vib_adc_callback);
                            _app_vib_data.state = APP_VIB_STATE_START_ADC;
                        }
                        else
                        {
                            APP_VIB_TLOG(LOGE, "%s : Error when getting memory\n\r", __FUNCTION__);
                        }
                    }
                }
                else
                {
                    //APP_VIB_TLOG(LOGE, "%s : Error when getting messsage from queue\n\r", __FUNCTION__);
                }
            }
            break;
            case APP_VIB_STATE_START_ADC:
            {
                APP_VIB_TLOG(LOGI, "%s : State Start ADC\n\r", __FUNCTION__);
                // Wait until the end of the measure
                if(osThreadFlagsWait(FLAG_END_ADC, osFlagsWaitAny, osWaitForever) == 0x01) 
                {
                    APP_VIB_TLOG(LOGI, "%s : Conv Complet\n\r", __FUNCTION__);
                    
                    _app_vib_data.state = APP_VIB_STATE_END_ADC;
                }
                else
                {
                    _app_vib_data.state = APP_VIB_STATE_IDLE;
                    APP_VIB_TLOG(LOGE, "%s : Event Flag error\n\r", __FUNCTION__);
                }
            }
            break;
            case APP_VIB_STATE_END_ADC:
            {
                APP_VIB_TLOG(LOGI, "%s : State End ADC\n\r", __FUNCTION__);

                uint16_t* ptr_buffer = _adc_buffer+5;
                // Send message in order to start FFT
                EventMessage_t message = {
                    .source = _app_vib_data.thread_id,
                    .type   = EVENT_TYPE_FFT_START,
                    .data   = ptr_buffer
                };
                event_handler_send(&message);
                _app_vib_data.state = APP_VIB_STATE_WAIT_FFT;
            }
            break;
            case APP_VIB_STATE_WAIT_FFT:
            {
                APP_VIB_TLOG(LOGI, "%s : State Wait FFT\n\r", __FUNCTION__);
                // Wait the end of the calculation
                if(osMessageQueueGet(_app_vib_data.queue, &message, NULL, osWaitForever) == osOK) 
                {
                    if(message.type == EVENT_TYPE_FFT_END)
                    {
#if (ALWAYS_MEASURE_VIB == 1)
                        _app_vib_data.state = APP_VIB_STATE_ALWAYS;
#else
                        _app_vib_data.state = APP_VIB_STATE_IDLE;
#endif
                        // Release memory
                        mdw_mem_release((uint8_t*)_adc_buffer);
                    }
                }
                else
                {
                    //APP_VIB_TLOG(LOGE, "%s : Error when getting messsage from queue\n\r", __FUNCTION__);
                }
            }
            case APP_VIB_STATE_ALWAYS:
            {
                // Always measure every 5s.
#if (ALWAYS_MEASURE_VIB == 1)
                osDelay(MS_TO_TICKS(5000));
                APP_VIB_TLOG(LOGI, "%s : New Measure\n\r", __FUNCTION__);
                _adc_buffer = (uint16_t*) mdw_mem_get((ADC_SAMPLES+5)*2); // Need 16384 + 10 Bytes
                if(_adc_buffer != NULL)
                {
                    mdw_adc_enable();
                    osDelay(MS_TO_TICKS(1000));
                    mdw_adc_start((uint32_t*)_adc_buffer, ADC_SAMPLES+5, (void*)_app_vib_adc_callback);
                    if(osThreadFlagsWait(FLAG_END_ADC, osFlagsWaitAny, osWaitForever) == 0x01)
                    {
                        APP_VIB_TLOG(LOGI, "%s : Prepare and send message\n\r", __FUNCTION__);
                        uint16_t* ptr_buffer = _adc_buffer+5;
                        EventMessage_t message = {
                            .source = _app_vib_data.thread_id,
                            .type   = EVENT_TYPE_FFT_START,
                            .data   = ptr_buffer
                        };
                        event_handler_send(&message);
                    }
                    
                    _app_vib_data.state = APP_VIB_STATE_WAIT_FFT;
                }
                else
                {
                    APP_VIB_TLOG(LOGE, "%s : Error when getting memory\n\r", __FUNCTION__);
                }
#endif
            }
            break;
            default:
                APP_VIB_TLOG(LOGI, "%s : State Error\n\r", __FUNCTION__);
        }
    }
}

/********************************************************************************
  * @brief Vibration sensor Conversion Callback
  * @param None
  * @retval None
********************************************************************************/
void _app_vib_adc_callback()
{
    osThreadFlagsSet(_app_vib_data.thread_id, FLAG_END_ADC);
}

