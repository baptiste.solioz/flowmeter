/**
  ******************************************************************************
  * @file           : app_vib.h
  * @brief          : Header for app_vib.c file.
  *                   This file contains the definitions for app_vib.h
  * @author         : Baptiste Solioz
  * @date           : 15. October 2020
  ******************************************************************************
  */

#ifndef __APP_VIB_H
#define __APP_VIB_H

#ifdef __cplusplus
extern "C" {
#endif

/* Private includes ----------------------------------------------------------*/
#include "app_conf.h"
#include "event_handler.h"
#include "cmsis_os2.h"
#include "mdw_adc.h"
#include "mdw_mem.h"

/* LOG Definition ------------------------------------------------------------*/
#if defined (APP_VIB_LOG_ENABLED) && (APP_VIB_LOG_ENABLED == 1)
#define APP_VIB_LOG(VL,...)     do{ {UTIL_ADV_TRACE_COND_FSend(VL, T_REG_OFF, TS_OFF, __VA_ARGS__);} }while(0);
#define APP_VIB_TLOG(VL,...)    do{ {UTIL_ADV_TRACE_COND_FSend(VL, T_REG_OFF, TS_ON, __VA_ARGS__);} }while(0); //with timestamp
#define APP_VIB_PLOG(VL,...)    do{ } while( UTIL_ADV_TRACE_OK != UTIL_ADV_TRACE_COND_FSend(VL, T_REG_OFF, TS_OFF, __VA_ARGS__) ) //Polling Mode
#elif defined (APP_VIB_LOG_ENABLED) && (APP_VIB_LOG_ENABLED == 0) /* APP_LOG disabled */
#define APP_VIB_LOG(...)
#define APP_VIB_TLOG(...)
#define APP_VIB_PLOG(...)
#else
#error "APP_VIB_LOG_ENABLED not defined or out of range <0,1>"
#endif

/* Private typedef -----------------------------------------------------------*/
typedef enum 
{ 
    APP_VIB_STATE_INIT, 
    APP_VIB_STATE_IDLE, 
    APP_VIB_STATE_START_ADC,
    APP_VIB_STATE_END_ADC,
    APP_VIB_STATE_WAIT_FFT,
    APP_VIB_STATE_ALWAYS
} App_Vib_State;

typedef struct {
    osThreadId_t        thread_id;      ///< Thread Id
    osThreadAttr_t      thread_attributes; ///< Thread Attributes 
    App_Vib_State       state;
    osMessageQueueId_t  queue;
} App_Vib_Attr_t;

/* Exported functions prototypes ---------------------------------------------*/
osThreadId_t app_vib_init();

/* Exported variables --------------------------------------------------------*/
uint16_t app_vib_remaining_stack;

#ifdef __cplusplus
}
#endif

#endif // __APP_VIB_H