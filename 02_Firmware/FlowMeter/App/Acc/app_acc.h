/**
  ******************************************************************************
  * @file           : app_acc.h
  * @brief          : Header for app_acc.c file.
  *                   This file contains the definitions for app_acc.h
  * @author         : Baptiste Solioz
  * @date           : 16. October 2020
  ******************************************************************************
  */

#ifndef __APP_ACC_H
#define __APP_ACC_H

#ifdef __cplusplus
extern "C" {
#endif

/* Private includes ----------------------------------------------------------*/
#include "app_conf.h"
#include "event_handler.h"
#include "cmsis_os2.h"
#include "mdw_spi.h"
#include "bma400.h"
#include "mdw_gpio.h"
#include "mdw_mem.h"

/* LOG Definition ------------------------------------------------------------*/
#if defined (APP_ACC_LOG_ENABLED) && (APP_ACC_LOG_ENABLED == 1)
#define APP_ACC_LOG(VL,...)     do{ {UTIL_ADV_TRACE_COND_FSend(VL, T_REG_OFF, TS_OFF, __VA_ARGS__);} }while(0);
#define APP_ACC_TLOG(VL,...)    do{ {UTIL_ADV_TRACE_COND_FSend(VL, T_REG_OFF, TS_ON, __VA_ARGS__);} }while(0); //with timestamp
#define APP_ACC_PLOG(VL,...)    do{ } while( UTIL_ADV_TRACE_OK != UTIL_ADV_TRACE_COND_FSend(VL, T_REG_OFF, TS_OFF, __VA_ARGS__) ) //Polling Mode
#elif defined (APP_ACC_LOG_ENABLED) && (APP_ACC_LOG_ENABLED == 0) /* APP_LOG disabled */
#define APP_ACC_LOG(...)
#define APP_ACC_TLOG(...)
#define APP_ACC_PLOG(...)
#else
#error "APP_ACC_LOG_ENABLED not defined or out of range <0,1>"
#endif

/* Private typedef -----------------------------------------------------------*/
typedef enum 
{ 
    APP_ACC_STATE_INIT, 
    APP_ACC_STATE_IDLE_RX, 
    APP_ACC_STATE_FIFO_WAIT,
    APP_ACC_STATE_FIFO_READ,
    APP_ACC_STATE_IDLE, 
    APP_ACC_STATE_WAKEUP,
    APP_ACC_STATE_WAIT_FFT,
    APP_ACC_STATE_CHECK_TMR
} App_Acc_State;

typedef struct {
  osThreadId_t          thread_id;      ///< Thread Id
  osThreadAttr_t        thread_attributes; ///< Thread Attributes 
  App_Acc_State         state;              ///< State Machine
  osMessageQueueId_t    queue;
  osMemoryPoolId_t      mem_pool;
  osTimerId_t           tmr_next;
} App_Acc_Attr_t;

/* Exported functions prototypes ---------------------------------------------*/
osThreadId_t app_acc_init();

/* Exported variables --------------------------------------------------------*/
uint16_t app_acc_remaining_stack;


#ifdef __cplusplus
}
#endif

#endif // __APP_ACC_H