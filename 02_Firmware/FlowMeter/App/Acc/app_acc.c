/**
  ******************************************************************************
  * @file           : app_acc.c
  * @brief          : Create a thread to handle the Acc application
  *                   
  * @author         : Baptiste Solioz
  * @date           : 16. October 2020
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "app_acc.h"
#include "stm32_systime.h"

/* Private constants ---------------------------------------------------------*/
#define     FLAG_INT1           0x0001
#define     FLAG_INT2           0x0002
#define     FLAG_MEAS_TIMEOUT   0x0004
#define     FLAG_CHECK_TIMEOUT  0x0008

#define N_FRAMES_FULL   1024        /* Total number of frames */
/* Add extra bytes to get complete fifo data */
#define FIFO_SIZE_FULL  (N_FRAMES_FULL + BMA400_FIFO_BYTES_OVERREAD)

#define APP_ACC_NEXT_TIMEOUT    20000

/* Private variables ---------------------------------------------------------*/
static App_Acc_Attr_t              _app_acc_data;

static struct bma400_sensor_data *      _accel_data;

/* Private functions ---------------------------------------------------------*/
static void _app_acc_thread(void *argument);
static void _app_acc_acc_power_down(struct bma400_dev* bma400_conf);
#if (USE_UART_RX == 0)
static int8_t _app_acc_init_acc_wakeup(struct bma400_dev* bma400_conf);
static void _app_acc_int1_callback();
#endif
static int8_t _app_acc_init_acc_fifo(struct bma400_dev* bma400_conf);
static void _app_acc_int2_callback();
#if (USE_UART_RX == 1)
static void _app_acc_meas_timeout();
#endif
static void _app_acc_tmr_next_callback(void *argument);

/********************************************************************************
  * @brief Acc App Initialize
  * @param None
  * @retval Thread Id
********************************************************************************/
osThreadId_t app_acc_init()
{
    _app_acc_data.thread_attributes.name = "Acc App Task";
    _app_acc_data.thread_attributes.priority = (osPriority_t) osPriorityNormal;
#if (APP_UART_LOG_ENABLED == 1)
    _app_acc_data.thread_attributes.stack_size = 256 * 4;
#else
    _app_acc_data.thread_attributes.stack_size = 256 * 4;
#endif
    // Initalize the state
    _app_acc_data.state = APP_ACC_STATE_INIT;
    // Create new thread
    _app_acc_data.thread_id = osThreadNew(_app_acc_thread, NULL, &_app_acc_data.thread_attributes);
    if(_app_acc_data.thread_id == NULL)
    {
        APP_ACC_TLOG(LOGE, "%s : Thread not created\n\r", __FUNCTION__);
    }
    // Create Event Queue
    _app_acc_data.queue = osMessageQueueNew(5, sizeof(EventMessage_t), NULL);
    if(_app_acc_data.queue == NULL)
    {
        APP_ACC_TLOG(LOGE, "%s : Queue not created\n\r", __FUNCTION__);
    }
    // Create Interval timer for measures
    _app_acc_data.tmr_next = osTimerNew(_app_acc_tmr_next_callback, osTimerOnce, NULL, NULL);
    if(_app_acc_data.tmr_next == NULL)
    {
        APP_ACC_TLOG(LOGE, "%s : Timer not created\n\r", __FUNCTION__);
    }
    // Subscribe to the events
    event_handler_subscribe(EVENT_TYPE_FFT_END,  _app_acc_data.queue);
    event_handler_subscribe(EVENT_TYPE_ACC_START,  _app_acc_data.queue);
    event_handler_subscribe(EVENT_TYPE_ACC_STOP,  _app_acc_data.queue);

    APP_ACC_TLOG(LOGI, "%s : Initialization Done\n\r", __FUNCTION__);

    return _app_acc_data.thread_id;
}

 /********************************************************************************
  * @brief Delay function used by the BMA400 driver
  * @param period : time in us
  * @param intf_ptr : pointer to the interface
  * @retval None
********************************************************************************/
void bma400_delay_us(uint32_t period, void *intf_ptr)
{
    if(period < 1000000/osKernelGetTickFreq())
        osDelay(MS_TO_TICKS(1000/osKernelGetTickFreq()));
    else
        osDelay(MS_TO_TICKS(period * osKernelGetTickFreq() / 1000000));
}

/********************************************************************************
  * @brief Acc App Thread
  * @param argument : Not used
  * @retval None
********************************************************************************/
void _app_acc_thread(void *argument)
{    
    struct bma400_dev       bma400_conf;
    struct bma400_fifo_data fifo_frame;
    uint16_t                accel_frames_req = N_FRAMES_FULL;
    uint16_t                int_status = 0;
    int8_t                  status;
    osTimerId_t             measTimer = 0;

    EventMessage_t  message;
    osDelay(MS_TO_TICKS(300));

    APP_ACC_TLOG(LOGI, "%s : Prepare BMA400\n\r", __FUNCTION__);

    for(;;) // Inifinite Loop
    {
        // Stack size analyzer
        app_acc_remaining_stack = osThreadGetStackSpace(_app_acc_data.thread_id);
        switch(_app_acc_data.state)
        {
            case APP_ACC_STATE_INIT:
            {
                APP_ACC_TLOG(LOGI, "%s : State Init\n\r", __FUNCTION__);
#if (USE_UART_RX == 1)
                // UART Mode selected
                _app_acc_data.state = APP_ACC_STATE_IDLE_RX;
                fifo_frame.data = NULL;
                fifo_frame.length = FIFO_SIZE_FULL;
                // Create timer for the measure timeout
                measTimer = osTimerNew(_app_acc_meas_timeout, osTimerOnce, NULL, NULL);
#else
                // Normal mode selected -> Initalize the wake-up interrupt
                if(_app_acc_init_acc_wakeup(&bma400_conf))
                {
                    osDelay(MS_TO_TICKS(10000));
                }
                else
                {
                    // Start Interval timer + subscribe to the GPIO interrupt
                    _app_acc_data.state = APP_ACC_STATE_IDLE;
                    osTimerStart(_app_acc_data.tmr_next, MS_TO_TICKS(APP_ACC_NEXT_TIMEOUT));
                    mdw_gpio_exti_subscribe(INT1_BMA400_Pin, (void*)_app_acc_int1_callback);
                }
#endif
            }
            break;
            case APP_ACC_STATE_IDLE_RX:
            {
                APP_ACC_TLOG(LOGI, "%s : State IDLE RX\n\r", __FUNCTION__);
                // Wait until a message comes from APP_UART
                if(osMessageQueueGet(_app_acc_data.queue, &message, NULL, osWaitForever) == osOK) 
                {
                    if(message.type == EVENT_TYPE_ACC_START)
                    {
                        // Start Measuring
                        uint8_t* meas_timeout = (uint8_t*) message.data;
                        // Intialize the FIFO mode
                        if(_app_acc_init_acc_fifo(&bma400_conf))
                        {
                            _app_acc_data.state = APP_ACC_STATE_IDLE_RX;
                        }
                        else
                        {
                            // Subscribe to the GPIO interrupt pin and start timeout timer
                            mdw_gpio_exti_subscribe(INT2_BMA400_Pin, (void*)_app_acc_int2_callback);
                            osTimerStart(measTimer, MS_TO_TICKS((*meas_timeout)*1000));
                            osThreadFlagsClear(0xFFFF);
                            _app_acc_data.state = APP_ACC_STATE_FIFO_WAIT;
                        }
                    }
                    else if(message.type == EVENT_TYPE_ACC_STOP)
                    {
                        // Stop Measuring
                        _app_acc_data.state = APP_ACC_STATE_IDLE;
                        osThreadFlagsClear(0xFFFF);
                    }
                }
                else
                {
                    APP_ACC_TLOG(LOGE, "%s : Error when getting messsage from queue\n\r", __FUNCTION__);
                }
            }
            break;
            case APP_ACC_STATE_FIFO_WAIT:
            {
                APP_ACC_TLOG(LOGI, "%s : State APP_ACC_STATE_FIFO_WAIT\n\r", __FUNCTION__);
                // Wait until the FIFO is full or a timeout occurs
                osThreadFlagsWait(FLAG_INT2, osFlagsWaitAny | osFlagsNoClear, osWaitForever);
                if((osThreadFlagsGet() & FLAG_MEAS_TIMEOUT) == FLAG_MEAS_TIMEOUT)
                {
                    // Timeout -> accelerometer in power down
                    osThreadFlagsClear(FLAG_MEAS_TIMEOUT);
                    APP_PPRINTF(LOGI, "END_ACC\n");
                    APP_ACC_TLOG(LOGI, "%s : FLAG_TIMEOUT\n\r", __FUNCTION__);
                    _app_acc_data.state = APP_ACC_STATE_IDLE_RX;
                    _app_acc_acc_power_down(&bma400_conf);
                    osThreadFlagsClear(0xFFFF);
                }
                if((osThreadFlagsGet() & FLAG_INT2) == FLAG_INT2)
                {
                    // FIFO Full interrupt
                    osThreadFlagsClear(FLAG_INT2);
                    APP_ACC_TLOG(LOGI, "%s : FLAG_INT2\n\r", __FUNCTION__);
                    _app_acc_data.state = APP_ACC_STATE_FIFO_READ;
                }
            }
            break;
            case APP_ACC_STATE_FIFO_READ:
            {
                //APP_ACC_TLOG(LOGI, "%s : State FIFO FULL\n\r", __FUNCTION__);
                // Read interrupt status
                status = bma400_get_interrupt_status(&int_status, &bma400_conf);
                if(status != BMA400_OK)
                {
                    APP_ACC_TLOG(LOGI, "%s : BMA Get Interrupt Status : Error %d\n\r", __FUNCTION__, status);
                }
                else
                {
                    //APP_ACC_TLOG(LOGI, "%s : BMA Int Status %d\n\r", __FUNCTION__, int_status);
                    if(int_status & BMA400_ASSERTED_FIFO_FULL_INT)
                    {
                        // Get the memory pointer from shared memory
                        fifo_frame.data = (uint8_t *) mdw_mem_get(FIFO_SIZE_FULL);
                        if(fifo_frame.data != NULL)
                        {
                            // Recover FIFO data
                            status = bma400_get_fifo_data(&fifo_frame, &bma400_conf);
                            if(status != BMA400_OK)
                            {
                                APP_ACC_TLOG(LOGE, "%s : BMA Set Power mode : Error %d\n\r", __FUNCTION__, status);
                            }
                            accel_frames_req = N_FRAMES_FULL;
                            // Get memory pointer for acceleration data
                            _accel_data = (struct bma400_sensor_data *) mdw_mem_get(sizeof(struct bma400_sensor_data)*N_FRAMES_FULL);
                            if(_accel_data != NULL)
                            {   
                                status = bma400_extract_accel(&fifo_frame, _accel_data, &accel_frames_req, &bma400_conf);
                                if(status != BMA400_OK)
                                {
                                    APP_ACC_TLOG(LOGE, "%s : bma400_extract_accel : Error %d\n\r", __FUNCTION__, status);
                                }

                                if (accel_frames_req)
                                {
                                    //APP_ACC_TLOG(LOGI, "Extracted FIFO frames : %d\n", accel_frames_req);
                                    uint16_t index;
                                    for (index = 0; index < accel_frames_req; index++)
                                    {
                                        // 12-bit accelerometer at range 2G 
                                        APP_PPRINTF(LOGI, "X: %d Y: %d Z: %d\n",
                                            _accel_data[index].x,
                                            _accel_data[index].y,
                                            _accel_data[index].z);
                                    }
                                }
                                mdw_mem_release((uint8_t*)_accel_data);            // free mem block
                            }
                            mdw_mem_release((uint8_t*)fifo_frame.data);            // free mem block
                        }
                    }
                }
                _app_acc_data.state = APP_ACC_STATE_IDLE_RX;
            }
            break;
            case APP_ACC_STATE_IDLE:
            {
                APP_ACC_TLOG(LOGI, "%s : State IDLE\n\r", __FUNCTION__);
                // Wait on Wake-up interrupt or interval timer
                osThreadFlagsWait(FLAG_INT1|FLAG_CHECK_TIMEOUT, osFlagsWaitAny | osFlagsNoClear, osWaitForever);
                if((osThreadFlagsGet() & FLAG_INT1) == FLAG_INT1)
                {
                    osThreadFlagsClear(FLAG_INT1);
                    APP_ACC_TLOG(LOGI, "%s : FLAG_INT1\n\r", __FUNCTION__);
                    _app_acc_data.state = APP_ACC_STATE_WAKEUP;
                }
                else if((osThreadFlagsGet() & FLAG_CHECK_TIMEOUT) == FLAG_CHECK_TIMEOUT)
                {
                    // Interval timer appends -> send new flow rate with 0
                    osThreadFlagsClear(FLAG_CHECK_TIMEOUT);
                    osTimerStop(_app_acc_data.tmr_next);
                    osTimerStart(_app_acc_data.tmr_next, MS_TO_TICKS(APP_ACC_NEXT_TIMEOUT));
                    APP_ACC_TLOG(LOGI, "%s : NO FLOW\n\r", __FUNCTION__);
                    SysTime_t timestamp = SysTimeGet();
                    EventMessage_t message = {
                        .source = _app_acc_data.thread_id,
                        .type   = EVENT_TYPE_NN_END,
                        .data_int   = 0,
                        .timeout = timestamp.Seconds,
                        .size   = 2
                    };
                    event_handler_send(&message);
                }
            }
            break;
            case APP_ACC_STATE_WAKEUP:
            {
                APP_ACC_TLOG(LOGI, "%s : State WAKEUP\n\r", __FUNCTION__);
                // Wake-up interrupt status
                status = bma400_get_interrupt_status(&int_status, &bma400_conf);
                if(status != BMA400_OK)
                {
                    APP_ACC_TLOG(LOGI, "%s : BMA Get Interrupt Status : Error %d\n\r", __FUNCTION__, status);
                }
                else
                {
                    APP_ACC_TLOG(LOGI, "%s : BMA Int Status %d\n\r", __FUNCTION__, int_status);
                    if(int_status & BMA400_ASSERTED_WAKEUP_INT)
                    {
                        // BMA400 in sleep mode during the measure
                        status = bma400_set_power_mode(BMA400_MODE_SLEEP, &bma400_conf);
                        if(status != BMA400_OK)
                        {
                            APP_ACC_TLOG(LOGI, "%s : BMA Set Power mode : Error %d\n\r", __FUNCTION__, status);
                        }
                    }
                }
                // Send Event to start the vibrating measure
                EventMessage_t message = {
                    .source = _app_acc_data.thread_id,
                    .type   = EVENT_TYPE_ADC_START
                };
                event_handler_send(&message);
                // Start delay timer until next measure
                osTimerStop(_app_acc_data.tmr_next);
                osTimerStart(_app_acc_data.tmr_next, MS_TO_TICKS(APP_ACC_NEXT_TIMEOUT));
                _app_acc_data.state = APP_ACC_STATE_WAIT_FFT;
            }
            break;
            case APP_ACC_STATE_WAIT_FFT:
            {
                APP_ACC_TLOG(LOGI, "%s : State Wait FFT\n\r", __FUNCTION__);
                // Wait until the end of the measure and calculation
                if(osMessageQueueGet(_app_acc_data.queue, &message, NULL, osWaitForever) == osOK) 
                {
                    if(message.type == EVENT_TYPE_FFT_END)
                    {
                        _app_acc_data.state = APP_ACC_STATE_CHECK_TMR;
                    }
                }
                else
                {
                    //APP_ACC_TLOG(LOGE, "%s : Error when getting messsage from queue\n\r", __FUNCTION__);
                }
            }
            break;
            case APP_ACC_STATE_CHECK_TMR:
            {
                APP_ACC_TLOG(LOGI, "%s : State Check Timer\n\r", __FUNCTION__);
                // Wait until the interval timer elapsed
                if(osThreadFlagsWait(FLAG_CHECK_TIMEOUT, osFlagsWaitAny, osWaitForever) == 0x01) 
                {
                    _app_acc_data.state = APP_ACC_STATE_IDLE;
                    osThreadFlagsClear(0xFFFF);
                    status = bma400_set_power_mode(BMA400_MODE_LOW_POWER, &bma400_conf);
                    if(status != BMA400_OK)
                    {
                        APP_ACC_TLOG(LOGI, "%s : BMA Set Power mode : Error %d\n\r", __FUNCTION__, status);
                    }
                }
            }
            break;
        }
    }
}

/********************************************************************************
  * @brief Accelerometer in power down
  * @param bma400_conf : accelerometer configuration
  * @retval None
********************************************************************************/
void _app_acc_acc_power_down(struct bma400_dev* bma400_conf)
{
    int8_t status;
    status = bma400_set_power_mode(BMA400_MODE_SLEEP, bma400_conf);
    if(status != BMA400_OK)
    {
        APP_ACC_TLOG(LOGE, "%s : BMA Set Power mode : Error %d\n\r", __FUNCTION__, status);
    }
}

/********************************************************************************
  * @brief Init accelerometer with Wake-up interrupt
  * @param bma400_conf : accelerometer configuration
  * @retval None
********************************************************************************/
#if (USE_UART_RX == 0)
int8_t _app_acc_init_acc_wakeup(struct bma400_dev* bma400_conf)
{
    struct bma400_device_conf dev_conf;
    struct bma400_sensor_conf conf;
    struct bma400_int_enable int_en;
    static uint8_t dev_addr;
    int8_t status;

    osDelay(MS_TO_TICKS(300));
    // Prepare configuration
    bma400_conf->read = mdw_spi_read;
    bma400_conf->write = mdw_spi_write;
    bma400_conf->intf = BMA400_SPI_INTF;
    bma400_conf->intf_ptr = &dev_addr;
    bma400_conf->delay_us = bma400_delay_us;
    bma400_conf->read_write_len = 46;
    // Reset the BMA400
    bma400_soft_reset(bma400_conf);
    // Initalization 
    status = bma400_init(bma400_conf);
    if(status != BMA400_OK)
    {
        APP_ACC_TLOG(LOGE, "%s : BMA Init : Error %d\n\r", __FUNCTION__, status);
        return status;
    }
    conf.type = BMA400_ACCEL;
    // Recover sensor configuration
    status = bma400_get_sensor_conf(&conf, 1, bma400_conf);
    if(status != BMA400_OK)
    {
        APP_ACC_TLOG(LOGE, "%s : BMA Get Sensor conf : Error %d\n\r", __FUNCTION__, status);
    }
    conf.param.accel.odr = BMA400_ODR_25HZ;
    conf.param.accel.range = BMA400_RANGE_2G;
    conf.param.accel.data_src = BMA400_DATA_SRC_ACCEL_FILT_1;
    conf.param.accel.osr = BMA400_ACCEL_OSR_SETTING_0;
    conf.param.accel.osr_lp = BMA400_ACCEL_OSR_SETTING_0;
    // Set the configuration in low power mode
    status = bma400_set_sensor_conf(&conf, 1, bma400_conf);
    if(status != BMA400_OK)
    {
        APP_ACC_TLOG(LOGE, "%s : BMA Set Sensor conf : Error %d\n\r", __FUNCTION__, status);
    }
    // Configure the interrupt
    dev_conf.type = BMA400_AUTOWAKEUP_INT;
    status = bma400_get_device_conf(&dev_conf, 1, bma400_conf);
    if(status != BMA400_OK)
    {
        APP_ACC_TLOG(LOGE, "%s : BMA Get Device conf : Error %d\n\r", __FUNCTION__, status);
    }
    dev_conf.param.wakeup.wakeup_ref_update = BMA400_UPDATE_EVERY_TIME;
    dev_conf.param.wakeup.sample_count = BMA400_SAMPLE_COUNT_4;
    dev_conf.param.wakeup.wakeup_axes_en = BMA400_AXIS_XYZ_EN;
    dev_conf.param.wakeup.int_wkup_threshold = 3;
    dev_conf.param.wakeup.int_wkup_ref_x = 0;
    dev_conf.param.wakeup.int_wkup_ref_y = 0;
    dev_conf.param.wakeup.int_wkup_ref_z = 32;
    dev_conf.param.wakeup.int_chan = BMA400_INT_CHANNEL_1;
    APP_ACC_TLOG(LOGI, "%s : BMA Set Device conf\n\r", __FUNCTION__);
    status = bma400_set_device_conf(&dev_conf, 1, bma400_conf);
    if(status != BMA400_OK)
    {
        APP_ACC_TLOG(LOGE, "%s : BMA Set Device conf : Error %d\n\r", __FUNCTION__, status);
    }
    // Start in low-power mode
    status = bma400_set_power_mode(BMA400_MODE_LOW_POWER, bma400_conf);
    if(status != BMA400_OK)
    {
        APP_ACC_TLOG(LOGE, "%s : BMA Set Power mode : Error %d\n\r", __FUNCTION__, status);
    }
    // Enable interrupt
    int_en.type = BMA400_AUTO_WAKEUP_EN;
    int_en.conf = BMA400_ENABLE;
    status = bma400_enable_interrupt(&int_en, 1, bma400_conf);
    if(status != BMA400_OK)
    {
        APP_ACC_TLOG(LOGE, "%s : BMA Enable Interrupt : Error %d\n\r", __FUNCTION__, status);
    }
    return status;
}
#endif
/********************************************************************************
  * @brief Init accelerometer with FIFO interrupt
  * @param bma400_conf : Accelerometer configuration
  * @retval None
********************************************************************************/
int8_t _app_acc_init_acc_fifo(struct bma400_dev* bma400_conf)
{
    struct bma400_device_conf dev_conf;
    struct bma400_sensor_conf conf;
    struct bma400_int_enable int_en;
    static uint8_t dev_addr;

    osDelay(MS_TO_TICKS(300));

    bma400_conf->read = mdw_spi_read;
    bma400_conf->write = mdw_spi_write;
    bma400_conf->intf = BMA400_SPI_INTF;
    bma400_conf->intf_ptr = &dev_addr;
    bma400_conf->delay_us = bma400_delay_us;
    bma400_conf->read_write_len = 46;

    int8_t status;
    // Reset BMA400
    bma400_soft_reset(bma400_conf);
    // Initialize accelerometer
    status = bma400_init(bma400_conf);
    if(status != BMA400_OK)
    {
        APP_ACC_TLOG(LOGE, "%s : BMA Init : Error %d\n\r", __FUNCTION__, status);
        return status;
    }
    conf.type = BMA400_ACCEL;
    // Retrieve configuration
    status = bma400_get_sensor_conf(&conf, 1, bma400_conf);
    if(status != BMA400_OK)
    {
        APP_ACC_TLOG(LOGE, "%s : BMA Get Sensor conf : Error %d\n\r", __FUNCTION__, status);
    }
    conf.param.accel.odr = BMA400_ODR_800HZ;
    conf.param.accel.range = BMA400_RANGE_2G;
    conf.param.accel.data_src = BMA400_DATA_SRC_ACCEL_FILT_1;
    conf.param.accel.osr = BMA400_ACCEL_OSR_SETTING_0;
    conf.param.accel.osr_lp = BMA400_ACCEL_OSR_SETTING_0;
    // Set Accelerometer in FIFO mode with max ODR
    status = bma400_set_sensor_conf(&conf, 1, bma400_conf);
    if(status != BMA400_OK)
    {
        APP_ACC_TLOG(LOGE, "%s : BMA Set Sensor conf : Error %d\n\r", __FUNCTION__, status);
    }

    dev_conf.type = BMA400_FIFO_CONF;
    // Retrieve Device configuration
    status = bma400_get_device_conf(&dev_conf, 1, bma400_conf);
    if(status != BMA400_OK)
    {
        APP_ACC_TLOG(LOGE, "%s : BMA Get Device conf : Error %d\n\r", __FUNCTION__, status);
    }
    dev_conf.param.fifo_conf.conf_regs = BMA400_FIFO_X_EN | BMA400_FIFO_Y_EN | BMA400_FIFO_Z_EN;
    dev_conf.param.fifo_conf.conf_status = BMA400_ENABLE;
    dev_conf.param.fifo_conf.fifo_full_channel = BMA400_INT_CHANNEL_2;
    APP_ACC_TLOG(LOGI, "%s : BMA Set Device conf\n\r", __FUNCTION__);
    // Set the device configuration
    status = bma400_set_device_conf(&dev_conf, 1, bma400_conf);
    if(status != BMA400_OK)
    {
        APP_ACC_TLOG(LOGE, "%s : BMA Set Device conf : Error %d\n\r", __FUNCTION__, status);
    }
    // Power mode normal in FIFO mode
    status = bma400_set_power_mode(BMA400_MODE_NORMAL, bma400_conf);
    if(status != BMA400_OK)
    {
        APP_ACC_TLOG(LOGE, "%s : BMA Set Power mode : Error %d\n\r", __FUNCTION__, status);
    }
    // Enable interrupt
    int_en.type = BMA400_FIFO_FULL_INT_EN;
    int_en.conf = BMA400_ENABLE;
    status = bma400_enable_interrupt(&int_en, 1, bma400_conf);
    if(status != BMA400_OK)
    {
        APP_ACC_TLOG(LOGE, "%s : BMA Enable Interrupt : Error %d\n\r", __FUNCTION__, status);
    }
    return status;
}

/********************************************************************************
  * @brief Accelerometer Interrupt Callback
  * @param None
  * @retval None
********************************************************************************/
#if (USE_UART_RX == 0)
void _app_acc_int1_callback()
{
    osThreadFlagsSet(_app_acc_data.thread_id, FLAG_INT1);
}
#endif
/********************************************************************************
  * @brief Accelerometer Interrupt Callback 2
  * @param None
  * @retval None
********************************************************************************/
void _app_acc_int2_callback()
{
    osThreadFlagsSet(_app_acc_data.thread_id, FLAG_INT2);
}

/********************************************************************************
  * @brief Measurement timeout
  * @param None
  * @retval None
********************************************************************************/
#if (USE_UART_RX == 1)
void _app_acc_meas_timeout()
{
    osThreadFlagsSet(_app_acc_data.thread_id, FLAG_MEAS_TIMEOUT);
}
#endif

/********************************************************************************
  * @brief Check Timer callback
  * @param None
  * @retval None
********************************************************************************/
void _app_acc_tmr_next_callback(void *argument)
{
    osThreadFlagsSet(_app_acc_data.thread_id, FLAG_CHECK_TIMEOUT); 
}