/******************************************************************************
 * @file    timer.h
 * @author  MCD Application Team
 * @brief   wrapper to timer server
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under Ultimate Liberty license
 * SLA0044, the "License"; You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *                             www.st.com/SLA0044
 *
 ******************************************************************************
 */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __TIMER_H__
#define __TIMER_H__

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "cmsis_os2.h"
#include "app_conf.h"
#include "event_handler.h"
#include "timer_handler.h"

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
#define TIMER_MAX_VAL 0xFFFFFFFFU
/**
  * @brief Max timer mask
  */
#define TIMERTIME_T_MAX ( ( uint32_t )~0 )

/* External variables --------------------------------------------------------*/
/* Exported macros -----------------------------------------------------------*/
#if (USE_FREERTOS_TIMER == 1)
    #define TimerTime_t uint32_t
    #define TimerEvent_t osTimerId_t
    #define TimerInit(HANDLE, CB)       do {\
                                            (*HANDLE) = osTimerNew( CB, osTimerOnce, NULL, NULL); \
                                            /*APP_LOG(TS_ON, VLEVEL_H, "%s : NEW TIMER CREATED %d\r\n",__FUNCTION__, (*HANDLE));*/ \
                                        } while(0)
    #define TimerStart(HANDLE, TIMEOUT) do {\
                                            if(IS_IRQ()) \
                                            {\
                                                EventMessage_t mess = { \
                                                    .source = NULL, \
                                                    .type = EVENT_TYPE_TIMER_START, \
                                                    .timer_id = (*HANDLE), \
                                                    .timeout = TIMEOUT \
                                                }; \
                                                event_handler_send(&mess); \
                                            } \
                                            else \
                                            { \
                                                osTimerStart((*HANDLE), MS_TO_TICKS(TIMEOUT)); \
                                            } \
                                            /*APP_LOG(TS_ON, VLEVEL_H, "%s : START TIMER at %d [ms] %d \r\n",__FUNCTION__, TIMEOUT, (*HANDLE));*/ \
                                        } while(0)
    #define TimerStop(HANDLE)           do {\
                                            if(IS_IRQ()) \
                                            {\
                                                EventMessage_t mess = { \
                                                    .source = NULL, \
                                                    .type = EVENT_TYPE_TIMER_STOP, \
                                                    .timer_id = (*HANDLE) \
                                                }; \
                                                event_handler_send(&mess); \
                                            } \
                                            else \
                                            { \
                                                osTimerStop((*HANDLE)); \
                                            } \
                                            /*APP_LOG(TS_ON, VLEVEL_H, "%s : STOP TIMER %d\r\n",__FUNCTION__, (*HANDLE));*/ \
                                        } while(0)
    #define TimerGetCurrentTime     osKernelGetTickCount
    #define TimerGetElapsedTime(PAST) (osKernelGetTickCount () - PAST)
#else 
    #define TimerTime_t UTIL_TIMER_Time_t
    #define TimerEvent_t UTIL_TIMER_Object_t
    #define TimerInit(HANDLE, CB) do {\
                                    UTIL_TIMER_Create( HANDLE, TIMER_MAX_VAL, UTIL_TIMER_ONESHOT, CB, NULL);\
                                    } while(0)
    #define TimerSetValue(HANDLE, TIMEOUT) do{ \
                                        UTIL_TIMER_SetPeriod(HANDLE, TIMEOUT);\
                                        } while(0)
    #define TimerStart(HANDLE)   do {\
                                    UTIL_TIMER_Start(HANDLE);\
                                    } while(0)
    #define TimerStop(HANDLE)   do {\
                                UTIL_TIMER_Stop(HANDLE);\
                                } while(0)
    #define TimerGetCurrentTime  UTIL_TIMER_GetCurrentTime
    #define TimerGetElapsedTime UTIL_TIMER_GetElapsedTime
#endif

/* Exported functions ------------------------------------------------------- */

#ifdef __cplusplus
}
#endif

#endif /* __TIMER_H__*/

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
