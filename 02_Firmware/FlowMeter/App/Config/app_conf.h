/**
  ******************************************************************************
  * @file   app_conf.h
  * @author MCD Application Team
  * @brief  applic configuration, e.g. : debug, trace, low power, sensors
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __APP_CONF_H__
#define __APP_CONF_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "stm32_adv_trace.h"

/******************************************************************************
 * PCB
 ******************************************************************************/
#define PCB_NB              4

/******************************************************************************
 * TEST AND ANALYZER
 ******************************************************************************/
//#define TEST_STACK_ANALYZER
//#define TEST_RADIO_TX_CONTINUOUS
//#define TEST_NEURAL_NETWORK

#define ALWAYS_MEASURE_VIB  0       /* Always Measure vibrating sensor + FFT */
#define USE_UART_RX         0       /* Use UART to launch ACC and ADC */

/******************************************************************************
 * LORA APP
 ******************************************************************************/

/* LoraWAN application configuration (Mw is configured by lorawan_conf.h) */
#ifndef ACTIVE_REGION
#define ACTIVE_REGION        LORAMAC_REGION_EU868
#endif
#define HYBRID_ENABLED       0

#define LORAMAC_CLASSB_ACTIVE 0  /* 0 if not active, 1 if active */

/* Temperature and pressure values are retrived from sensors shield  */
/* (instead of sending dummy values). It requires MEMS IKS shield */
#define SENSOR_ENABLED  0

/******************************************************************************
 * DEBUGGING
 ******************************************************************************/
#define LOGE   1  // Log only error
#define LOGW   2  // Log Warning + Error
#define LOGI   3  // Log All

#define VERBOSE_LEVEL   LOGI
/* Enable trace logs. Note verbose level can be selected in app_system.h */
#define APP_LOG_ENABLED         0

#define APP_FFT_LOG_ENABLED     0
#define APP_VIB_LOG_ENABLED     0
#define APP_LORAWAN_LOG_ENABLED 0
#define APP_ACC_LOG_ENABLED     0
#define APP_UART_LOG_ENABLED    0
#define TIMER_LOG_ENABLED       0
#define EVENT_LOG_ENABLED       0

#define APP_PPRINTF(VL,...)  do{ } while( UTIL_ADV_TRACE_OK != UTIL_ADV_TRACE_COND_FSend(VL, T_REG_OFF, TS_OFF, __VA_ARGS__) ) //Polling Mode
#define APP_TPRINTF(VL,...)   do{ {UTIL_ADV_TRACE_COND_FSend(VL, T_REG_OFF, TS_ON, __VA_ARGS__);} }while(0); //with timestamp
#define APP_PRINTF(VL,...)   do{ {UTIL_ADV_TRACE_COND_FSend(VL, T_REG_OFF, TS_OFF, __VA_ARGS__);} }while(0);


#if defined (APP_LOG_ENABLED) && (APP_LOG_ENABLED == 1)
#define APP_LOG(TS,VL,...)   do{ {UTIL_ADV_TRACE_COND_FSend(VL, T_REG_OFF, TS, __VA_ARGS__);} }while(0);
#elif defined (APP_LOG_ENABLED) && (APP_LOG_ENABLED == 0) /* APP_LOG disabled */
#define APP_LOG(TS,VL,...)
#else
#error "APP_LOG_ENABLED not defined or out of range <0,1>"
#endif

// Use of FREERTOS instead of STM sequencer
#ifdef USE_FREERTOS
#define USE_FREERTOS_TIMER  1
#define USE_FREERTOS_LPM    1
#else
#define USE_FREERTOS_TIMER  0
#define USE_FREERTOS_LPM    0
#endif

#ifdef __cplusplus
}
#endif

#endif /* __APP_CONF_H__ */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
