/*******************************************************************************
 * @file    radio_conf.h
 * @author  MCD Application Team
 * @brief   Header of radio configuration
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under Ultimate Liberty license
 * SLA0044, the "License"; You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *                             www.st.com/SLA0044
 *
 ******************************************************************************
 */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __RADIO_CONF_H__
#define __RADIO_CONF_H__

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "platform.h"
#include "mw_log_conf.h"
#include "stm32wlxx_board_radio.h"
#include "app_conf.h"
#include "radio_driver.h"
  
#define DBG_GPIO_RADIO_RX(set_rst) /*DBG_GPIO_##set_rst##_LINE(DGB_LINE1_PORT, DGB_LINE1_PIN);*/
#define DBG_GPIO_RADIO_TX(set_rst) /*DBG_GPIO_##set_rst##_LINE(DGB_LINE2_PORT, DGB_LINE2_PIN);*/
  
#ifndef CRITICAL_SECTION_BEGIN
#define CRITICAL_SECTION_BEGIN( )      UTILS_ENTER_CRITICAL_SECTION( )  
#endif
#ifndef CRITICAL_SECTION_END  
#define CRITICAL_SECTION_END( )        UTILS_EXIT_CRITICAL_SECTION( )  
#endif
/* Exported types ------------------------------------------------------------*/
/* Defines   ------------------------*/
#define RADIO_RX_BUF_SIZE       255

#define LET_SUBGHZ_MW_USING_DGB_LINE1_PIN
#define LET_SUBGHZ_MW_USING_DGB_LINE2_PIN

/* Function mapping */
#define RADIO_DELAY_MS                          HAL_Delay
#define RADIO_MEMSET8( dest, value, size )      do{ \
                                                    uint8_t* dst8= (uint8_t *) dest;\
                                                    uint8_t size_d = size;\
                                                    while( size_d-- )\
                                                        *dst8++ = value;\
                                                }while(0);

/**
  * @brief drive value used anytime radio is NOT in TX low power mode
  */
#define SMPS_DRIVE_SETTING_DEFAULT  SMPS_DRV_40

/**
  * @brief drive value used anytime radio is in TX low power mode
  *        TX low power mode is the worst case because the PA sinks from SMPS
  *        while in high power mode, current is sunk directly from the battery
  */
#define SMPS_DRIVE_SETTING_MAX      SMPS_DRV_60

/**
  * @brief in XO mode, set internal capacitor (from 0x00 to 0x2F starting 11.2pF with 0.47pF steps)
  */
#define XTAL_DEFAULT_CAP_VALUE      0x20

/**
  * @brief Frequency error (in Hz) can be compensated here.
  *        warning XO frequency error generates (de)modulator sampling time error which can not be compensated
  */
#define RF_FREQUENCY_ERROR          ((int32_t) 0)

/**
  * @brief voltage of vdd tcxo.
  */
#define TCXO_CTRL_VOLTAGE           TCXO_CTRL_1_7V


/* Exported constants --------------------------------------------------------*/
/* External variables --------------------------------------------------------*/
/* Exported macros -----------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */


#ifdef __cplusplus
}
#endif

#endif /* __RADIO_CONF_H__*/

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
