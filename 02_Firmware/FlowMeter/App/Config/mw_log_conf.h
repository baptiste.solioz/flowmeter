/**
 ******************************************************************************
 * @file    mw_log_conf.h
 * @author  MCD Application Team
 * @brief   Interface layer CM4 System to MBMUX (Mailbox Multiplexer)
 *******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under Ultimate Liberty license
 * SLA0044, the "License"; You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *                             www.st.com/SLA0044
 *
 ******************************************************************************
 */


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MW_LOG_CONF_H__
#define __MW_LOG_CONF_H__

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include "stm32_adv_trace.h"

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
#define MW_LOG_ENABLED  0
/* Exported macros -----------------------------------------------------------*/

#if defined (MW_LOG_ENABLED) && (MW_LOG_ENABLED == 1)
#define MW_LOG(TS,VL, ...)   do{ {UTIL_ADV_TRACE_COND_FSend(VL, T_REG_OFF, TS, __VA_ARGS__);} }while(0);
#elif defined (MW_LOG_ENABLED) && (MW_LOG_ENABLED == 0) /* APP_LOG disabled */
#define MW_LOG(TS,VL, ...)
#else
#error "APP_LOG_ENABLED not defined or out of range <0,1>"
#endif

/* Defines -------------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */



#ifdef __cplusplus
}
#endif

#endif /*__MW_LOG_CONF_H__ */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
