/**
  ******************************************************************************
  * @file           : stm32wlxx_freertos_tim.c
  * @brief          : Use LPTIM as systick for FreeRTOS
  *                   
  * @author         : Baptiste Solioz
  * @date           : 12. November 2020
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "stm32wlxx_freertos_tim.h"
#include "stm32wlxx_board.h"

/* FreeRTOS includes. */
#include "FreeRTOS.h"
#include "task.h"

#include "app_conf.h"
#include "cmsis_os2.h"

/* Private variables ---------------------------------------------------------*/
LPTIM_HandleTypeDef hlptim1;

static uint8_t compTimeout;

/********************************************************************************
  * @brief vPortSetupTimerInterrupt called by freertos at the initialization
  * @param None
  * @retval None
********************************************************************************/
void vPortSetupTimerInterrupt( void )
//void mdw_lptim1_init()
{
    hlptim1.Instance = LPTIM1;
    hlptim1.Init.Clock.Source = LPTIM_CLOCKSOURCE_APBCLOCK_LPOSC;
    hlptim1.Init.Clock.Prescaler = LPTIM_PRESCALER_DIV1;
    hlptim1.Init.UltraLowPowerClock.Polarity = LPTIM_CLOCKPOLARITY_RISING;
    hlptim1.Init.UltraLowPowerClock.SampleTime = LPTIM_CLOCKSAMPLETIME_DIRECTTRANSITION;
    hlptim1.Init.Trigger.Source = LPTIM_TRIGSOURCE_SOFTWARE;
    hlptim1.Init.OutputPolarity = LPTIM_OUTPUTPOLARITY_HIGH;
    hlptim1.Init.UpdateMode = LPTIM_UPDATE_IMMEDIATE;
    hlptim1.Init.CounterSource = LPTIM_COUNTERSOURCE_INTERNAL;
    if (HAL_LPTIM_Init(&hlptim1) != HAL_OK)
    {
        Error_Handler();
    }
    
    if (HAL_LPTIM_Counter_Start_IT(&hlptim1, (32768 / osKernelGetTickFreq())) != HAL_OK)
    {
        Error_Handler();
    }
    compTimeout = 0;
}

/********************************************************************************
* @brief LPTIM MSP Initialization
* This function configures the hardware resources used in this example
* @param hlptim: LPTIM handle pointer
* @retval None
********************************************************************************/
void HAL_LPTIM_MspInit(LPTIM_HandleTypeDef* hlptim)
{
    RCC_PeriphCLKInitTypeDef PeriphClkInitStruct = {0};
    if(hlptim->Instance==LPTIM1)
    {

        /* Force the LPTIM1 Periheral Clock Reset */
        __HAL_RCC_LPTIM1_FORCE_RESET();

        /* Release the LPTIM1 Periheral Clock Reset */  
        __HAL_RCC_LPTIM1_RELEASE_RESET();

        /** Initializes the peripherals clocks 
        */
        PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_LPTIM1;
        PeriphClkInitStruct.Lptim1ClockSelection = RCC_LPTIM1CLKSOURCE_LSE;
        if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK)
        {
            Error_Handler();
        }

        /* Peripheral clock enable */
        __HAL_RCC_LPTIM1_CLK_ENABLE();

        /* LPTIM1 interrupt Init */
        HAL_NVIC_SetPriority(LPTIM1_IRQn, configLIBRARY_LOWEST_INTERRUPT_PRIORITY, 0);
        HAL_NVIC_EnableIRQ(LPTIM1_IRQn);

        /* LPTIM1 EXTI Enable */
        LL_EXTI_EnableIT_0_31(EXTI_LPTIM1_WAKEUP);
    }

}

/********************************************************************************
* @brief LPTIM MSP De-Initialization
* This function freeze the hardware resources used in this example
* @param hlptim: LPTIM handle pointer
* @retval None
********************************************************************************/
void HAL_LPTIM_MspDeInit(LPTIM_HandleTypeDef* hlptim)
{
    if(hlptim->Instance==LPTIM1)
    {
        /* USER CODE BEGIN LPTIM1_MspDeInit 0 */
        RCC_OscInitTypeDef RCC_OscInitStruct;
        RCC_PeriphCLKInitTypeDef RCC_PeriphCLKInitStruct;

        /* Disable LSE clock */
        RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_LSE;
        RCC_OscInitStruct.LSEState = RCC_LSE_OFF;
        RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
        if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
        {
            Error_Handler();
        }

        /* Select the PCLK clock as LPTIM1 peripheral clock */
        RCC_PeriphCLKInitStruct.PeriphClockSelection = RCC_PERIPHCLK_LPTIM1;
        RCC_PeriphCLKInitStruct.Lptim1ClockSelection = RCC_LPTIM1CLKSOURCE_PCLK1;
        HAL_RCCEx_PeriphCLKConfig(&RCC_PeriphCLKInitStruct);
        /* Force the LPTIM1 Periheral Clock Reset */
        __HAL_RCC_LPTIM1_FORCE_RESET();

        /* Release the LPTIM1 Periheral Clock Reset */  
        __HAL_RCC_LPTIM1_RELEASE_RESET();

        /* USER CODE END LPTIM1_MspDeInit 0 */
        /* Peripheral clock disable */
        __HAL_RCC_LPTIM1_CLK_DISABLE();

        /* LPTIM1 interrupt DeInit */
        HAL_NVIC_DisableIRQ(LPTIM1_IRQn);
    }
}

/********************************************************************************
  * @brief  Autoreload match callback in non blocking mode 
  * @param  hlptim : LPTIM handle
  * @retval None
********************************************************************************/
void HAL_LPTIM_AutoReloadMatchCallback(LPTIM_HandleTypeDef *hlptim)
{
    compTimeout++;
    if(compTimeout == 25)
    {
        compTimeout = 0;
        __HAL_LPTIM_AUTORELOAD_SET(hlptim, (32768 / osKernelGetTickFreq() + 17));
    }
    else
    {
        __HAL_LPTIM_AUTORELOAD_SET(hlptim, (32768 / osKernelGetTickFreq()));
    }
    
    /* The next block of code is from the standard FreeRTOS tick interrupt
	handler.  The standard handler is not called directly in case future
	versions contain changes that make it no longer suitable for calling
	here. */
	( void ) portSET_INTERRUPT_MASK_FROM_ISR();
	{
		if( xTaskIncrementTick() != pdFALSE )
		{

			portNVIC_INT_CTRL_REG = portNVIC_PENDSVSET_BIT;
		}
		/* Just completely clear the interrupt mask on exit by passing 0 because
		it is known that this interrupt will only ever execute with the lowest
		possible interrupt priority. */
	}
	portCLEAR_INTERRUPT_MASK_FROM_ISR( 0 );
}

    