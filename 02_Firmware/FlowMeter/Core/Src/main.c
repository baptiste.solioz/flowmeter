/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "cmsis_os2.h"
#include "app_conf.h"
#include "stdio.h"
#if (USE_FREERTOS_LPM == 0)
#include "stm32_lpm.h"
#endif
#include "stm32wlxx_board.h"

/* Private includes ----------------------------------------------------------*/
#include "app_fft.h"
#include "app_vib.h"
#include "app_acc.h"
#include "app_uart.h"
#include "app_lorawan.h"
#include "mdw_adc.h"
#include "mdw_dma.h"
#include "timer_handler.h"
#include "radio.h"
#ifdef TEST_NEURAL_NETWORK
#include "test_data.h"
#endif

/* Private typedef -----------------------------------------------------------*/

/* Private define ------------------------------------------------------------*/
#ifdef TEST_RADIO_TX_CONTINUOUS
    #define REGION_EU868
    #define TX_TIMEOUT                                  65535     // seconds (MAX value)
    #define RF_FREQUENCY                                868000000 // Hz
    #define TX_OUTPUT_POWER                             14        // 14 dBm
#endif

/* Extern variables ----------------------------------------------------------*/
extern uint16_t app_vib_remaining_stack;
extern uint16_t app_fft_remaining_stack;
extern uint16_t app_lorawan_remaining_stack;
extern uint16_t app_acc_remaining_stack;
extern uint16_t app_uart_remaining_stack;

/* Private variables ---------------------------------------------------------*/
#ifdef TEST_RADIO_TX_CONTINUOUS
    /* Definitions for radioTxTask */
    osThreadId_t radioTxTaskHandle;
    const osThreadAttr_t radioTxTask_attributes = {
        .name = "RadioTx",
        .priority = (osPriority_t) osPriorityNormal,
        .stack_size = 256 * 4
    };
    static RadioEvents_t RadioEvents;
    void RadioTxTask(void *argument);
#endif
#ifdef TEST_STACK_ANALYZER
    /* Definitions for analyzerTask */
    osThreadId_t analyzerTaskHandle;
    const osThreadAttr_t analyzerTask_attributes = {
        .name = "analyzer",
        .priority = (osPriority_t) osPriorityNormal,
        .stack_size = 256 * 4
    };
    void AnalyzerTask(void *argument);
#endif

#ifdef TEST_NEURAL_NETWORK
    /* Definitions for analyzerTask */
    osThreadId_t nnTaskHandle;
    const osThreadAttr_t nnTask_attributes = {
        .name = "analyzer",
        .priority = (osPriority_t) osPriorityNormal,
        .stack_size = 256 * 8
    };
    void NeuralNetworkTask(void *argument);
#endif

/* Private function prototypes -----------------------------------------------*/
static void SystemClock_Config(void);
static void Gpio_PreInit(void);
#if (MW_LOG_ENABLED == 1) || (APP_LOG_ENABLED == 1)
static void TimestampNow(uint8_t *buff, uint16_t* size);
#endif
/********************************************************************************
  * @brief  The application entry point.
  * @retval int
********************************************************************************/
int main(void)
{
    /* MCU Configuration--------------------------------------------------------*/
    /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
    HAL_Init();
    /* Configure the system clock */
    SystemClock_Config();

    Gpio_PreInit();

    BSP_LED_Init(LED_GREEN);
    BSP_LED_Init(LED_RED);

    BSP_LED_Off(LED_RED);
    BSP_LED_Off(LED_GREEN);


    /* Initialize Trace if needed */
#if (MW_LOG_ENABLED == 1) || (APP_LOG_ENABLED == 1)
    HAL_SetTickFreq(HAL_TICK_FREQ_1KHZ);
    HAL_ResumeTick();
    /*Initialize the terminal */
    UTIL_ADV_TRACE_Init( );
    UTIL_ADV_TRACE_RegisterTimeStampFunction( TimestampNow ); 
    /*Set verbose LEVEL*/
    UTIL_ADV_TRACE_SetVerboseLevel(VERBOSE_LEVEL);
#else
    HAL_SetTickFreq(HAL_TICK_FREQ_1KHZ);
    HAL_ResumeTick();
#endif

    /* Initialize all configured peripherals */
    mdw_rtc_init();
    mdw_dma_init();
    mdw_adc_init();
    mdw_spi_init();
    mdw_gpio_init();
    mdw_mem_init();

    /* Init scheduler */
    osKernelInitialize();

    /* Create the thread(s) */
#ifdef TEST_RADIO_TX_CONTINUOUS
    radioTxTaskHandle = osThreadNew(RadioTxTask, NULL, &radioTxTask_attributes);
#endif
#ifdef TEST_STACK_ANALYZER
    analyzerTaskHandle = osThreadNew(AnalyzerTask, NULL, &analyzerTask_attributes);
#endif
#ifdef TEST_NEURAL_NETWORK
    nnTaskHandle = osThreadNew(NeuralNetworkTask, NULL, &nnTask_attributes);
#endif
    event_handler_init();       // Handle event between thread
    timer_handler_init();       // Handle timer from interrupt source (osTimer not callable from interrupt)
#if !defined(TEST_RADIO_TX_CONTINUOUS) && !defined(TEST_NEURAL_NETWORK) 
    app_fft_init();             // Calculate the FFT
    app_vib_init();             // Measure the vibration sensor
#if (ALWAYS_MEASURE_VIB == 0)
    app_acc_init();             // Handle the accelerometer
#endif
#if (USE_UART_RX == 1)
    app_uart_init();            // Allow RX message from UART
#endif
    app_lorawan_init();         // Handle LoRa communication
#endif
    APP_LOG(TS_ON, VLEVEL_H, "Start Kernel\r\n");
    osKernelStart();

    /* We should never get here as control is now taken by the scheduler */
    /* Infinite loop */
    while (1)
    {
    }
}

#ifdef TEST_RADIO_TX_CONTINUOUS
/********************************************************************************
  * @brief  Radio Tx timeout callback
  * @param  None
  * @retval None
********************************************************************************/
void OnRadioTxTimeout( void )
{
    // Restarts continuous wave transmission when timeout expires
    Radio.SetTxContinuousWave( RF_FREQUENCY, TX_OUTPUT_POWER, TX_TIMEOUT );
}

/********************************************************************************
  * @brief  Function implementing the Radio Tx thread.
  * @param  argument: Not used
  * @retval None
********************************************************************************/
void RadioTxTask(void *argument)
{
    APP_LOG(TS_ON, VLEVEL_H, "Start Radio Test\r\n");
    // Radio initialization
    RadioEvents.TxTimeout = OnRadioTxTimeout;
    Radio.Init( &RadioEvents );

    Radio.SetTxContinuousWave( RF_FREQUENCY, TX_OUTPUT_POWER, TX_TIMEOUT );
    for(;;)
    {
        osDelay(MS_TO_TICKS(5000));
    }
}
#endif
#ifdef TEST_STACK_ANALYZER
/********************************************************************************
  * @brief  Function implementing the Analyzer thread.
  * @param  argument: Not used
  * @retval None
********************************************************************************/
void AnalyzerTask(void *argument)
{
    for(;;)
    {
        osDelay(MS_TO_TICKS(5000));
        APP_LOG(TS_OFF, VLEVEL_H, "############# MEMORY ANALYZER #############\r\n");
        APP_LOG(TS_OFF, VLEVEL_H, "## Heap remaining    : %d Bytes\r\n", xPortGetFreeHeapSize());
        APP_LOG(TS_OFF, VLEVEL_H, "## Stack Thread vib  : %d Bytes\r\n", app_vib_remaining_stack*4);
        APP_LOG(TS_OFF, VLEVEL_H, "## Stack Thread fft  : %d Bytes\r\n", app_fft_remaining_stack*4);
        APP_LOG(TS_OFF, VLEVEL_H, "## Stack Thread acc  : %d Bytes\r\n", app_acc_remaining_stack*4);
        APP_LOG(TS_OFF, VLEVEL_H, "## Stack Thread lora : %d Bytes\r\n", app_lorawan_remaining_stack*4);
        APP_LOG(TS_OFF, VLEVEL_H, "## Stack Thread uart : %d Bytes\r\n", app_uart_remaining_stack*4);
    }
}
#endif

#ifdef TEST_NEURAL_NETWORK
/********************************************************************************
  * @brief  Function implementing the NeuralNetwork thread.
  * @param  argument: Not used
  * @retval None
********************************************************************************/
void NeuralNetworkTask(void *argument)
{
    int16_t input_data[82];
    int16_t result = 0;
    for(;;)
    {
        APP_LOG(TS_ON, VLEVEL_H, "Perform NN check\r\n");
        APP_LOG(TS_ON, VLEVEL_H, "result = [");
        for(int i=0; i<428; i++)
        {
            memcpy(input_data, input_ex[i], sizeof(input_data));
            result = mdw_nn_run(input_data);
            // Result format Q6.9
            int16_t result_m = (result >> 9);
            int16_t result_n = ((result - (result_m << 9)) * 100) >> 9;
            APP_LOG(TS_OFF, VLEVEL_H, "%d.%d, ", result_m, result_n);

            //APP_LOG(TS_OFF, VLEVEL_H, "%d, ", result);
        }
        APP_LOG(TS_OFF, VLEVEL_H, "]\r\n");
        APP_LOG(TS_ON, VLEVEL_H, "Finish\r\n");
        osDelay(MS_TO_TICKS(50000));
    }
}
#endif

/********************************************************************************
  * @brief  Callback to handle the Idle state
  * @param  None
  * @retval None
********************************************************************************/
void vApplicationIdleHook( void )
{
#if (MW_LOG_ENABLED == 1) || (APP_LOG_ENABLED == 1)
    // Debug mode -> sleep mode for UART
    HAL_PWR_EnterSLEEPMode(PWR_MAINREGULATOR_ON, PWR_SLEEPENTRY_WFI);
#else
    if(mdw_adc_is_measuring())
    {
        // Sleep mode if ADC performs a measure
        HAL_PWR_EnterSLEEPMode(PWR_MAINREGULATOR_ON, PWR_SLEEPENTRY_WFI);
    }
    else
    {
        LL_PWR_ClearFlag_C1STOP_C1STB();
        // Stay in STOP 1 to retain DMA and ADC data
        HAL_PWREx_EnterSTOP1Mode(PWR_STOPENTRY_WFI);
    }
#endif
}

/********************************************************************************
  * @brief  GPIO pre initalization
  * @param  None
  * @retval None
********************************************************************************/
static void Gpio_PreInit(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* Configure all IOs in analog input              */
  /* Except PA143 and PA14 (SWCLK and SWD) for debug*/
  /* PA13 and PA14 are configured in debug_init     */
  /* Configure all GPIO as analog to reduce current consumption on non used IOs */
  /* Enable GPIOs clock */
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();

  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  /* All GPIOs except debug pins (SWCLK and SWD) */
  GPIO_InitStruct.Pin = GPIO_PIN_All & (~(GPIO_PIN_13 | GPIO_PIN_14));
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /* All GPIOs */
  GPIO_InitStruct.Pin = GPIO_PIN_All;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
  HAL_GPIO_Init(GPIOH, &GPIO_InitStruct);

  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Pin = GPIO_PIN_4 | GPIO_PIN_12;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /* Disable GPIOs clock */
  __HAL_RCC_GPIOA_CLK_DISABLE();
  __HAL_RCC_GPIOB_CLK_DISABLE();
  __HAL_RCC_GPIOC_CLK_DISABLE();
  __HAL_RCC_GPIOH_CLK_DISABLE();
}

/********************************************************************************
  * @brief  Callback to retrieve the timestamp (Trace)
  * @param  buff : buffer to fill
  * @param  size : New string size
  * @retval None
********************************************************************************/
#if (MW_LOG_ENABLED == 1) || (APP_LOG_ENABLED == 1)
static void TimestampNow(uint8_t *buff, uint16_t* size)
{
    RTC_TimeTypeDef sTime;
    RTC_DateTypeDef sDate;
    mdw_rtc_getTime(&sTime, &sDate);
    sprintf((char *)buff, "%d:%02d:%02d.%d:", sTime.Hours,
                                            sTime.Minutes,
                                            sTime.Seconds,
                                            sTime.SubSeconds);
    *size = strlen((char *)buff);
}
#endif
/********************************************************************************
  * @brief  System Clock Configuration
  * @param  None
  * @retval None
********************************************************************************/
static void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};


  /* MSI is enabled after System reset, activate PLL with MSI as source */
  RCC_OscInitStruct.OscillatorType =  RCC_OSCILLATORTYPE_LSE | RCC_OSCILLATORTYPE_MSI;
  RCC_OscInitStruct.HSEState = RCC_HSE_OFF;
  RCC_OscInitStruct.LSEState = RCC_LSE_ON;
  RCC_OscInitStruct.MSIState = RCC_MSI_ON;
  RCC_OscInitStruct.HSIState = RCC_HSI_OFF;
  RCC_OscInitStruct.MSICalibrationValue = RCC_MSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.MSIClockRange = RCC_MSIRANGE_11;   /* System Clock set at 48 MHz */

  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /**Initializes the CPU, AHB and APB busses clocks
  */
  RCC_ClkInitStruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2 |  RCC_CLOCKTYPE_HCLK3);
  RCC_ClkInitStruct.SYSCLKSource   = RCC_SYSCLKSOURCE_MSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.AHBCLK3Divider = RCC_SYSCLK_DIV1;
  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/********************************************************************************
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
********************************************************************************/
void Error_Handler(void)
{
}

#ifdef  USE_FULL_ASSERT
/********************************************************************************
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
********************************************************************************/
void assert_failed(uint8_t *file, uint32_t line)
{

}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
