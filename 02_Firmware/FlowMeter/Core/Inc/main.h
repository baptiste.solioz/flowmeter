/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32wlxx_hal.h"
//#include "utilities_conf.h"


/* Private includes ----------------------------------------------------------*/

/* Exported types ------------------------------------------------------------*/

/* Exported constants --------------------------------------------------------*/

/* Exported macro ------------------------------------------------------------*/

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* Private defines -----------------------------------------------------------*/
/* RTC User Constant */
#define RTC_N_PREDIV_S             10  /* subsecond number of bits */
#define RTC_PREDIV_S              ((1<<RTC_N_PREDIV_S)-1)      /* RTC_SYNCH_PREDIV; */
#define RTC_PREDIV_A              (1<<(15-RTC_N_PREDIV_S))-1   /* RTC_ASYNCH_PREDIV; */
#define USART_BAUDRATE 230400

#define ADC_PIEZO_Pin GPIO_PIN_11
#define ADC_PIEZO_GPIO_Port GPIOA

#define OUT_PIEZO_PWR_Pin GPIO_PIN_12
#define OUT_PIEZO_PWR_GPIO_Port GPIOA

#define SWDIO_Pin GPIO_PIN_13
#define SWDIO_GPIO_Port GPIOA
#define SWCLK_Pin GPIO_PIN_14
#define SWCLK_GPIO_Port GPIOA

#define ACC_SCK_Pin GPIO_PIN_1
#define ACC_SCK_GPIO_Port GPIOA
#define ACC_MISO_Pin GPIO_PIN_6
#define ACC_MISO_GPIO_Port GPIOA
#define ACC_MOSI_Pin GPIO_PIN_7
#define ACC_MOSI_GPIO_Port GPIOA
#define ACC_NSS_Pin GPIO_PIN_4
#define ACC_NSS_GPIO_Port GPIOA

#define IN_PUSH_USER_Pin GPIO_PIN_2
#define IN_PUSH_USER_GPIO_Port GPIOB

#define INT1_BMA400_Pin GPIO_PIN_4
#define INT1_BMA400_GPIO_Port GPIOB
#define INT2_BMA400_Pin GPIO_PIN_5
#define INT2_BMA400_GPIO_Port GPIOB

#define USART_TX_Pin GPIO_PIN_2
#define USART_TX_GPIO_Port GPIOA
#define USART_RX_Pin GPIO_PIN_3
#define USART_RX_GPIO_Port GPIOA
#define USARTx_RX_GPIO_CLK_ENABLE()      __HAL_RCC_GPIOA_CLK_ENABLE()
#define USARTx_TX_GPIO_CLK_ENABLE()      __HAL_RCC_GPIOA_CLK_ENABLE()

#define USARTx_TX_AF                     GPIO_AF7_USART2
#define USARTx_RX_AF                     GPIO_AF7_USART2

#define EXTI_USART2_WAKEUP               LL_EXTI_LINE_27
#define EXTI_LPTIM1_WAKEUP               LL_EXTI_LINE_29

/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
