# Flowmeter

This repository contains all documents related to my master thesis "Low-Cost Sensor for flow rate measurements". 

## Project Tree

- 01_Hardware : Altium files of the PCB, including BOM, schematics, layout.
- 02_Firmware : Firmware of the flowmeter based on CMAKE
- 03_K2ARM : Script used to convert a Keras model to a CMSIS-NN model without FPU (Qm.n Format). Based on the repository https://github.com/InES-HPMM/k2arm
- 04_Notebook : Jupyter Python Notebook used during the test and measure
- 05_Labview : Acquisition's program made with labview.
- 06_QT : QT application to show the data acquired using a MQTT broker (ChirpStack).
- 07_Test : Video of the final measures.