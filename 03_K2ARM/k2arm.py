""" Class to parse a keras model into a .h and .c file such that the ARM CMSIS NN library
functions can be used to run the model on an embedded micro controller such as a Cortex-M4.

More information about the ARM CMSIS:
    https://www.keil.com/pack/doc/CMSIS/NN/html/index.html
    
More information about keras:
    https://keras.io/

References:
    [1]: KEIL CMSIS Documentation,
         https://www.keil.com/pack/doc/CMSIS/DSP/html/group__float__to__x.html

    [2]: Tensorflow documentation,
         https://www.tensorflow.org/api_docs/python/tf/quantization/fake_quant_with_min_max_args

    [3]: liangzhen-lai, ARM-CMSIS Github, https://github.com/ARM-software/CMSIS_5/issues/327

    [4]: Raphael Zingg, ZHAW / Institute of Embedded systems
         https://blog.zhaw.ch/high-performance/2018/12/21/machine-learning-on-cortex-m4-using-keras-and-arm-cmsis-nn/

@author: Baptiste Solioz <baptiste.solioz@gmail.com>
"""
import datetime
import numpy as np
import tensorflow as tf
import keras as k
import pandas as pd
from keras.layers import Input, Dense, Activation, Lambda, Conv1D, GlobalAveragePooling1D
from keras.models import Model
from math import log, ceil


class k2arm:
    def __init__(self, outputFilePath, fixPointBits=7, model=None, evalData=None):
        self.outputFilePath = outputFilePath
        self.fixPointBits = fixPointBits
        self.model = model
        self.evalData = evalData
        self.numberOfDenselayers = len(self.getDenseLayersInformation())
        self.numberOfConv1Dlayers = len(self.getConv1DLayersInformation())
        self.weightDecBitsDense = []
        self.weightDecBitsConv1D = []
        self.intWeightsAndBiasDense = []
        self.intWeightsAndBiasConv1D = []
        self.bestIntBitsDense = []
        self.bestIntBitsConv1D = []

        self.biasShift = []
        self.outputShift = []

        self.param_conv1d = []
        self.param_pool = []
        
        self.tanh_lut = []
        self.tanh_shift = 0

# --------------------------------------------------------------------------------------------------
#                                      functions to quantize weights
# --------------------------------------------------------------------------------------------------
    def quantizeWeights(self):
        """ Quantizes the weights originalWeights into 8bit/16bit using min/max
        Check the range of the weights and find the next power of 2 needed to
        represent the range.
        Stores the quantized weights into and intWeightsAndBias

        """
        print()
        print("#################################################################################")
        print("#                          QUANTIZE WEIGHTS                                     #")
        print("#################################################################################")
        self.weightDecBitsDense = []
        self.weightDecBitsConv1D = []

        self.intWeightsAndBiasDense = []
        self.intWeightsAndBiasConv1D = []

        self.param_conv1d = []
        self.param_pool = []

        self.dimActivation = []

        last_param_conv1d = []
        lastLayerOutputSize = 0
        lastLayerOutputChannel = 1

        # quantize layer by layer
        for layer in self.model.layers:
            if 'input' in layer.name:
                lastLayerOutputSize = layer.output_shape[0][1]
                lastLayerOutputChannel = 1
                continue
            if 'activation' in layer.name:
                self.dimActivation.append(lastLayerOutputSize)
                continue
            if 'global_average_pooling1d' in layer.name:
                param = [   last_param_conv1d[0],       # Input Dim
                            last_param_conv1d[2],       # Input Channel
                            last_param_conv1d[2],       # Output Dim
                            last_param_conv1d[3],       # Kernel Size
                            0,                          # Padding
                            1]                          # Stride
                self.param_pool.append(param)
                continue
            if 'conv1d' in layer.name:
                param = [   lastLayerOutputSize,
                            lastLayerOutputChannel,
                            layer.get_config().get('filters'),
                            layer.get_config().get('kernel_size')[0],
                            0,
                            1]
                self.param_conv1d.append(param)
                last_param_conv1d = param
                lastLayerOutputSize = layer.output_shape[1]
                lastLayerOutputChannel = layer.get_config().get('filters')
            if 'dense' in layer.name:
                lastLayerOutputSize = layer.output_shape[1]

            print('Quantisize weights of layer: ' + layer.name)
            weightsArray = layer.get_weights()

            # dense layer has 2 numpy arrays with weights, the matrix weights and the bias: [w, b]
            for weights in weightsArray:
                intBit = self.findQRangeOfWeights(weights)
                # if we have q15 we can buffer the int bits. This increases stability
                # in q7 mode this does sometimes decrease the accuracy to much
                if self.fixPointBits == 15:
                    intBit = intBit + 2

                elif (self.fixPointBits == 7) and (intBit == 0):
                    intBit = 1
                print("QRange of Weights : ", intBit)
                decBit = self.fixPointBits - intBit
                
                # convert to q format using same method as ARM [1]
                intWeights = np.round(weights * 2**decBit)

                # saturate range inside q7 or q15 to prevent sign cast in C code
                intWeights[intWeights > 2**self.fixPointBits -1] = 2**self.fixPointBits - 1
                intWeights[intWeights < -2**self.fixPointBits] = - 2**self.fixPointBits

                # store int16 or int8 values
                if 'dense' in layer.name:
                    self.weightDecBitsDense.append(decBit)
                    self.intWeightsAndBiasDense.append(intWeights)
                if 'conv1d' in layer.name:
                    self.weightDecBitsConv1D.append(decBit)
                    self.intWeightsAndBiasConv1D.append(intWeights)

    def findQRangeOfWeights(self, weights):
        """ Finds the Q Range which does not clip the weights

        weights: float values of which the ideal Q range is searched
        returns: number of integer bits required to represent "weights"
        """
        qRange = [None]*(self.fixPointBits + 1)
        for bit in range(0, self.fixPointBits + 1):
            qRange[bit] = self.findQR(bit)

        minValue = weights.min()
        maxValue = weights.max()
        highestVal = max(abs(minValue), abs(maxValue))
        for bit in range(0, self.fixPointBits + 1):
            if highestVal < qRange[bit][1]:
                # highestVal fits in the range
                # return number of integer bits required
                return bit

    def findQR(self, intBits):
        """ Calculate the range, given the number of integer bits QM.N

        intBits: number of bits used for integer part
        """
        M = intBits
        N = self.fixPointBits - M
        [minVal, maxVal] = [-2**(M - 1), (2**(M - 1) - 2**(-N))]
        return [minVal, maxVal]

# --------------------------------------------------------------------------------------------------
#                        functions to find ideal output shifts of CMSIS-NN layers
# --------------------------------------------------------------------------------------------------
    def findOutputFormat(self):
        """ Find the output format by checking the results of all layers.
        It is performed by creating a new model for each layers and by
        predicting the result. The min/max of the prediction will determine
        the minimal number of bits necessary for the output of the layer.
        Then the prediction will be the input next layer.
        """
        print()
        print("#################################################################################")
        print("#                          FIND OUTPUT FORMAT                                   #")
        print("#################################################################################")
        # get information about the dense layers
        denseLayers = self.getDenseLayersInformation()
        self.bestIntBitsDense = np.zeros([len(denseLayers)])

        # get information about the conv1d layers
        conv1dLayers = self.getConv1DLayersInformation()
        self.bestIntBitsConv1D = np.zeros([len(conv1dLayers)])

        pred = []
        idx_dense = 0
        idx_conv1d = 0
        max_power = 0
        for layer in self.model.layers:
            print("Handle layer : ", layer.name)
            if "input" in layer.name:
                input_layer = k.layers.Input(layer.get_config().get('batch_input_shape')[1:])
                pred = self.evalData[0]
            if "conv1d" in layer.name:
                output_layer = k.layers.Conv1D( filters=layer.get_config().get('filters'), 
                                                kernel_size=layer.get_config().get('kernel_size'), 
                                                padding='same')(input_layer)
                model = k.models.Model(inputs=input_layer, outputs=output_layer)
                model.layers[1].set_weights(layer.get_weights())
                pred = model.predict(pred)
                max_power = ceil(log(pred.max(), 2))
                print("    Max : ", pred.max(), "Min : ", pred.min(), "Pow : ", max_power)
                input_layer = k.layers.Input(pred.shape[1:])
                self.bestIntBitsConv1D[idx_conv1d] = max_power
                idx_conv1d = idx_conv1d + 1
            if "activation" in layer.name:
                output_layer = k.layers.Activation(layer.get_config().get('activation'))(input_layer)
                model = k.models.Model(inputs=input_layer, outputs=output_layer)
                pred = model.predict(pred)
                print("    Max : ", pred.max(), "Min : ", pred.min(), "Pow : ", ceil(log(pred.max(), 2)))
                input_layer = k.layers.Input(pred.shape[1:])
            if "global_average_pooling1d" in layer.name:
                output_layer = k.layers.GlobalAveragePooling1D()(input_layer)
                model = k.models.Model(inputs=input_layer, outputs=output_layer)
                pred = model.predict(pred)
                print("    Max : ", pred.max(), "Min : ", pred.min(), "Pow : ", ceil(log(pred.max(), 2)))
                input_layer = k.layers.Input(pred.shape[1:])
            if "dense" in layer.name:
                output_layer = k.layers.Dense(layer.get_config().get('units'))(input_layer)
                model = k.models.Model(inputs=input_layer, outputs=output_layer)
                model.layers[1].set_weights(layer.get_weights())
                pred = model.predict(pred)
                max_power = ceil(log(pred.max(), 2))
                print("    Max : ", pred.max(), "Min : ", pred.min(), "Pow : ", max_power)
                input_layer = k.layers.Input(pred.shape[1:])
                self.bestIntBitsDense[idx_dense] = max_power
                idx_dense = idx_dense + 1

        # print the output range
        for conv1dLayer in range(0, len(conv1dLayers)):
            print('Conv1D_'+str(conv1dLayer) + ' Output range: Q'
                  + str(int(self.bestIntBitsConv1D[conv1dLayer])) + '.'
                  + str(int(self.fixPointBits - self.bestIntBitsConv1D[conv1dLayer])))
        # print the output range
        for denseLayer in range(0, len(denseLayers)):
            print('Dense_'+str(denseLayer) + ' Output range: Q'
                  + str(int(self.bestIntBitsDense[denseLayer])) + '.'
                  + str(int(self.fixPointBits - self.bestIntBitsDense[denseLayer])))

    def getDenseLayersInformation(self):
        """ Returns a Layer depending on the input
        can parse activation and dense layers
        """
        denseLayers = []
        for layer in self.model.layers:
            if "dense" in layer.name:
                denseLayers.append(
                    Dense(layer.output_shape[1], name=layer.name))
        return denseLayers
    
    def getConv1DLayersInformation(self):
        """ Returns a Layer depending on the input
        can parse activation and conv1d layers
        """
        conv1dLayers = []
        for layer in self.model.layers:
            if "conv1d" in layer.name:
                conv1dLayers.append(
                    Conv1D(filters=3, kernel_size=3, name=layer.name))
        return conv1dLayers

# --------------------------------------------------------------------------------------------------
#                        functions to calculate the Bias and Output Shift
# --------------------------------------------------------------------------------------------------
    def calculateShift(self):
        '''   
        Shift parameters are calculated as following:
        bias_shift = inputFracBits + weightsFracBits - biasFracBits
        out_shift = inputFracBits + weightsFracBits - fixPointBits - bestIntBits

        For example:
        If the input is Q5.2, the weight is Q1.6, the bias is Q4.3
        and the output is Q3.4, the product of input and weight is Q23.8
        In this case, bias_shift is 5 (left shift Q4.3 bias by 5 bits to match Q23.8)
        and out_shift is 4 (i.e., right shift Q23.8 by 4 to match
        Q3.4 output) [3].
        '''
        print()
        print("#################################################################################")
        print("#                          CALCULATE SHIFT                                      #")
        print("#################################################################################")
        max_input_power = ceil(log(self.evalData[0].max(), 2)) + 2
        layerInputDecBits = self.fixPointBits - max_input_power # Input layer format Q13.2
        idx = 0
        for i in range(0, self.numberOfConv1Dlayers):
            print("Conv1D Layer ", i+1)
            inShift = layerInputDecBits + self.weightDecBitsConv1D[idx] - self.weightDecBitsConv1D[idx + 1]
            if inShift < 0:
                print('Warning inShift truncated!!')
                inShift = 0
            outShift = layerInputDecBits + self.weightDecBitsConv1D[idx] - (self.fixPointBits - self.bestIntBitsConv1D[i])
            if outShift < 0:
                print('Warning outShift truncated!!')
                outShift = 0
            self.biasShift.append(inShift)
            self.outputShift.append(outShift)
            print("    InputFracBits   = ", layerInputDecBits)
            print("    WeightsFracBits = ", self.weightDecBitsConv1D[idx])
            print("    biasFracBits    = ", self.weightDecBitsConv1D[idx+1])
            print("    OutputFracBits  = ", (self.fixPointBits - self.bestIntBitsConv1D[i]))
            layerInputDecBits = self.fixPointBits - self.bestIntBitsConv1D[i]
            idx = idx + 2
        idx = 0
        for i in range(0, self.numberOfDenselayers):
            print("Dense Layer ", i+1)
            inShift = layerInputDecBits + self.weightDecBitsDense[idx] - self.weightDecBitsDense[idx + 1]
            if inShift < 0:
                print('Warning inShift truncated!!')
                inShift = 0
            outShift = layerInputDecBits + self.weightDecBitsDense[idx] - (self.fixPointBits - self.bestIntBitsDense[i])
            if outShift < 0:
                print('Warning outShift truncated!!')
                outShift = 0
            self.biasShift.append(inShift)
            self.outputShift.append(outShift)
            print("    InputFracBits   = ", layerInputDecBits)
            print("    WeightsFracBits = ", self.weightDecBitsDense[idx])
            print("    biasFracBits    = ", self.weightDecBitsDense[idx+1])
            print("    OutputFracBits  = ", (self.fixPointBits - self.bestIntBitsDense[i]))
            layerInputDecBits = self.fixPointBits - self.bestIntBitsDense[i]
            idx = idx + 2
        self.outputShift.append(layerInputDecBits)

        # Reduce the output shift from first layer to improve accuracy
        self.outputShift[0] = self.outputShift[0] #- 2

        print("Summary shift calculated : ")
        print("    Bias Shift List   : ", self.biasShift)
        print("    Output Shift List : ", self.outputShift)

# --------------------------------------------------------------------------------------------------
#                        functions to verify the accuracy of the new model
# --------------------------------------------------------------------------------------------------
    def verifyAccuracy(self):
        """ Verify the accuracy of the new model by reformating all layers 
        with the parameters calculated above. Then compare the result with
        the accuracy of the reference model.
        """
        print()
        print("#################################################################################")
        print("#                          VERIFY ACCURACY                                      #")
        print("#################################################################################")
        pred = []
        idx_dense = 0
        idx_conv1d = 0
        idx_shift = 0
        for layer in self.model.layers:
            if "input" in layer.name:
                input_layer = k.layers.Input(layer.get_config().get('batch_input_shape')[1:])
                pred = self.evalData[0]
            if "conv1d" in layer.name:
                output_layer = k.layers.Conv1D( filters=layer.get_config().get('filters'), 
                                                kernel_size=layer.get_config().get('kernel_size'), 
                                                padding='same')(input_layer)
                model = k.models.Model(inputs=input_layer, outputs=output_layer)
                weights = model.layers[1].get_weights()
                weights[0] = self.intWeightsAndBiasConv1D[idx_conv1d]
                weights[1] = self.intWeightsAndBiasConv1D[idx_conv1d+1] * 2**(self.biasShift[idx_shift]) + self.outputShift[idx_shift]
                model.layers[1].set_weights(weights)
                pred = model.predict(pred) / 2**(self.outputShift[idx_shift])
                pred = pred.astype(int)
                input_layer = k.layers.Input(pred.shape[1:])
                idx_conv1d = idx_conv1d + 2
                idx_shift = idx_shift + 1
            if "activation" in layer.name:
                if layer.get_config().get('activation') == 'tanh':
                    # Create the tanh Look Up Table with previous layer data
                    self.tanh_shift = 3
                    tanh_x = np.arange(0, pred.max(), 2**(self.tanh_shift))
                    tanh_x = tanh_x / 2**(self.outputShift[idx_shift])
                    tanh_x = np.tanh(tanh_x)
                    self.tanh_lut = tanh_x * 2**(self.outputShift[idx_shift])
                    # Check the result
                    index = pred / 2**(self.tanh_shift)
                    index = index.astype(int)

                    index[index < 0] = 0
                    index[index >= len(self.tanh_lut)] = len(self.tanh_lut)-1
                    pred = self.tanh_lut[index]
                    
                else:
                    output_layer = k.layers.Activation(layer.get_config().get('activation'))(input_layer)
                    model = k.models.Model(inputs=input_layer, outputs=output_layer)
                    pred = model.predict(pred)
                    pred = pred.astype(int)
                input_layer = k.layers.Input(pred.shape[1:])
            if "global_average_pooling1d" in layer.name:
                output_layer = k.layers.GlobalAveragePooling1D()(input_layer)
                model = k.models.Model(inputs=input_layer, outputs=output_layer)
                pred = model.predict(pred)
                pred = pred.astype(int)
                input_layer = k.layers.Input(pred.shape[1:])
            if "dense" in layer.name:
                output_layer = k.layers.Dense(layer.get_config().get('units'))(input_layer)
                model = k.models.Model(inputs=input_layer, outputs=output_layer)
                weights = model.layers[1].get_weights()
                weights[0] = self.intWeightsAndBiasDense[idx_dense]
                weights[1] = self.intWeightsAndBiasDense[idx_dense+1] * 2**(self.biasShift[idx_shift]) + self.outputShift[idx_shift]
                model.layers[1].set_weights(weights)
                pred = model.predict(pred) / 2**(self.outputShift[idx_shift])
                pred = pred.astype(int)
                input_layer = k.layers.Input(pred.shape[1:])
                idx_dense = idx_dense + 2
                idx_shift = idx_shift + 1

        max_train = self.evalData[1].max()
        min_train = self.evalData[1].min()
        max_train_q = max_train * 2**9
        min_train_q = min_train * 2**9
        pred = pred * (max_train_q - min_train_q) + (min_train_q * 2**(self.outputShift[idx_shift]))
        pred = pred / 2**(self.outputShift[idx_shift])
        pred = pred / 2**9

        diff = pred.flatten() - self.evalData[1]
        mse = k.losses.MeanSquaredError()
        mse_model = mse(self.evalData[1], pred.flatten()).numpy()  
        mae = k.losses.MeanAbsoluteError()
        mae_ref = mae(self.evalData[1], pred.flatten()).numpy()    
        print("Result Model CMSIS-NN : ")
        print("    MSE = ", mse_model)
        print("    MAE = ", mae_ref)
        print("    Diff Max : ", diff.max(), "Min : ", diff.min())

        # Check the reference model
        pred_ref = self.model.predict(self.evalData[0])
        pred_ref = pred_ref * (max_train - min_train) + min_train
        diff_ref = pred_ref.flatten() - self.evalData[1]

        mse = k.losses.MeanSquaredError()
        mse_ref = mse(self.evalData[1], pred_ref.flatten()).numpy()    
        mae = k.losses.MeanAbsoluteError()
        mae_ref = mae(self.evalData[1], pred_ref.flatten()).numpy()   
        print("Result Model Keras : ")
        print("    MSE = ", mse_ref)
        print("    MAE = ", mae_ref)
        print("    Diff Max : ", diff_ref.max(), "Min : ", diff_ref.min())


# --------------------------------------------------------------------------------------------------
#                                     functions to write C files
# --------------------------------------------------------------------------------------------------
    def storeWeights(self):
        """ Store the weights and biases
        """
        print()
        print("#################################################################################")
        print("#                          Store Weights                                        #")
        print("#################################################################################")
        idx = 1
        fp = self.outputFilePath + 'weights.h'
        print('Write weights and net parameters into:' + fp)
        with open(fp, 'w') as f:
            f.write('#include "arm_nnfunctions.h"\n')
            for i in range(0, len(self.intWeightsAndBiasDense), 2):
                # weights of dense layer
                size = self.intWeightsAndBiasDense[i].shape[0]*self.intWeightsAndBiasDense[i].shape[1]
                if self.fixPointBits == 7:
                    f.write('const q7_t aq7_dense_' + str(idx) + '_weights[' + str(size) + '] = {')
                elif self.fixPointBits == 15:
                    f.write('const q15_t aq15_dense_' + str(idx) + '_weights[' + str(size) + '] = {')
                np.transpose(self.intWeightsAndBiasDense[i]).tofile(
                    f, sep=", ", format="%d")
                f.write('};\n')
                size = self.intWeightsAndBiasDense[i+1].shape[0]
                 # bias of dense layer
                if self.fixPointBits == 7:
                    f.write('const q7_t aq7_dense_' + str(idx) + '_bias[' + str(size) + '] = {')
                elif self.fixPointBits == 15:
                    f.write('const q15_t aq15_dense_' + str(idx) + '_bias[' + str(size) + '] = {')
                np.transpose(
                    self.intWeightsAndBiasDense[i + 1]).tofile(f, sep=", ", format="%d")
                f.write('};\n')
                idx = idx + 1
            idx = 1
            for i in range(0, len(self.intWeightsAndBiasConv1D), 2):
                size = self.intWeightsAndBiasConv1D[i].shape[0]*self.intWeightsAndBiasConv1D[i].shape[1]*self.intWeightsAndBiasConv1D[i].shape[2]
                # weights of conv1d layer
                if self.fixPointBits == 7:
                    f.write('const q7_t aq7_conv1d_' + str(idx) + '_weights[' + str(size) + '] = {')
                elif self.fixPointBits == 15:
                    f.write('const q15_t aq15_conv1d_' + str(idx) + '_weights[' + str(size) + '] = {')
                if(i==0):
                    np.transpose(self.intWeightsAndBiasConv1D[i]).tofile(f, sep=", ", format="%d")
                else:
                    np.transpose(self.intWeightsAndBiasConv1D[i], (2, 0, 1)).tofile(f, sep=", ", format="%d")
                f.write('};\n')
                size = self.intWeightsAndBiasConv1D[i+1].shape[0]
                # bias of conv1d layer
                if self.fixPointBits == 7:
                    f.write('const q7_t aq7_conv1d_' + str(idx) + '_bias[' + str(size) + '] = {')
                elif self.fixPointBits == 15:
                    f.write('const q15_t aq15_conv1d_' + str(idx) + '_bias[' + str(size) + '] = {')
                np.transpose(
                    self.intWeightsAndBiasConv1D[i + 1]).tofile(f, sep=", ", format="%d")
                f.write('};\n')
                idx = idx + 1
            if self.tanh_lut != []:
                f.write('const q15_t tanh_lut['+ str(len(self.tanh_lut)) + '] = {')
                self.tanh_lut.tofile(f, sep=", ", format="%d")
                f.write('};\n')
                idx = idx + 1

    def storeDimension(self):
        """ Store the input and output dimensions of each layer
        """
        print()
        print("#################################################################################")
        print("#                          Store Dimension                                      #")
        print("#################################################################################")
        idx = 1
        fp = self.outputFilePath + 'weights.h'
        with open(fp, 'a') as f:
            for i in range(0, len(self.intWeightsAndBiasDense), 2):
                f.write('#define DENSE_' + str(idx) + '_IN_DIM '
                        + str(self.intWeightsAndBiasDense[i].shape[0]) + '\n')
                f.write('#define DENSE_' + str(idx) + '_OU_DIM '
                        + str(self.intWeightsAndBiasDense[i + 1].shape[0]) + '\n')
                idx = idx + 1
            idx=1
            for i in range(0, len(self.intWeightsAndBiasConv1D), 2):
                print(self.param_conv1d[idx-1])
                f.write('#define CONV1D_' + str(idx) + '_IN_DIM '
                        + str(self.param_conv1d[idx-1][0]) + '\n')
                print(self.intWeightsAndBiasConv1D[i+1].shape)
                f.write('#define CONV1D_' + str(idx) + '_OU_DIM '
                        + str(self.param_conv1d[idx-1][0]) + '\n')
                idx = idx + 1

    def storeConvParam(self):
        """ Store the convolution layer parameters
        """
        idx = 1
        fp = self.outputFilePath + 'weights.h'
        with open(fp, 'a') as f:
            for i in range(0, len(self.param_conv1d)):
                f.write('#define CONV1D_' + str(idx) + '_IN_CH '
                        + str(self.param_conv1d[i][1]) + '\n')
                f.write('#define CONV1D_' + str(idx) + '_OU_CH '
                        + str(self.param_conv1d[i][2]) + '\n')
                f.write('#define CONV1D_' + str(idx) + '_KER_DIM '
                        + str(self.param_conv1d[i][3]) + '\n')
                f.write('#define CONV1D_' + str(idx) + '_PADDING '
                        + str(self.param_conv1d[i][4]) + '\n')
                f.write('#define CONV1D_' + str(idx) + '_STRIDE '
                        + str(self.param_conv1d[i][5]) + '\n')
                idx = idx + 1
        
    def storePoolParam(self):
        """ Store the pooling layer parameters
        """
        idx = 1
        fp = self.outputFilePath + 'weights.h'
        with open(fp, 'a') as f:
            for i in range(0, len(self.param_pool)):
                f.write('#define POOL_' + str(idx) + '_IN_DIM '
                        + str(self.param_pool[i][0]) + '\n')
                f.write('#define POOL_' + str(idx) + '_IN_CH '
                        + str(self.param_pool[i][1]) + '\n')
                f.write('#define POOL_' + str(idx) + '_OU_DIM '
                        + str(self.param_pool[i][2]) + '\n')
                f.write('#define POOL_' + str(idx) + '_KER_DIM '
                        + str(self.param_pool[i][3]) + '\n')
                f.write('#define POOL_' + str(idx) + '_PADDING '
                        + str(self.param_pool[i][4]) + '\n')
                f.write('#define POOL_' + str(idx) + '_STRIDE '
                        + str(self.param_pool[i][5]) + '\n')
                idx = idx + 1

    def storeOutShiftParams(self):
        """ Store the input and output shift
        """
        print()
        print("#################################################################################")
        print("#                          Store Shift parameters                               #")
        print("#################################################################################")
        fp = self.outputFilePath + 'weights.h'
        print("Weight Dec Conv1D : ", self.weightDecBitsConv1D, self.bestIntBitsConv1D)
        print("Weight Dec Dense : ", self.weightDecBitsDense, self.bestIntBitsDense)
        with open(fp, 'a') as f:
            idx = 0
            for i in range(0, self.numberOfConv1Dlayers):
                f.write('#define CONV1D_' + str(i + 1) + '_IN_SHIFT ' + str(int(self.biasShift[idx])) + '\n')
                f.write('#define CONV1D_' + str(i + 1) + '_OU_SHIFT ' + str(int(self.outputShift[idx])) + '\n')
                idx = idx + 1
            for i in range(0, self.numberOfDenselayers):
                
                f.write('#define DENSE_' + str(i + 1) + '_IN_SHIFT ' + str(int(self.biasShift[idx])) + '\n')
                f.write('#define DENSE_' + str(i + 1) + '_OU_SHIFT ' + str(int(self.outputShift[idx])) + '\n')
                idx = idx + 1
            f.write('#define OUTPUT_SHIFT ' + str(int(self.outputShift[idx])) + '\n')
            if self.tanh_lut != []:
                f.write('#define TANH_SHIFT ' + str(int(self.tanh_shift)) + '\n')

    def storeBitSize(self):
        """ Write header file with the FIXPOINT_MODE define such that the c compiler
        uses the correct functions.
        """
        print()
        print("#################################################################################")
        print("#                          Store Bit Size                                       #")
        print("#################################################################################")
        fp = self.outputFilePath + 'bit_size.h'
        print('Write FIXPOINT_MODE_ define into:' + fp)
        with open(fp, 'w') as f:
            # add a define depending on used function (q7 or q15)
            f.write('#define FIXPOINT_MODE_' + str(self.fixPointBits) + '\n')

    def storeNetFunction(self):
        """ Creates a c function which calls the required arm functions given a net
        Restrictions:
            only dense layers (fully connected)
            only relu, softmax, tanh, sigmoid activations
            softmax can only be the last activation function

        mc: classifier with a keras.engine.sequential.Sequential
        """
        print()
        print("#################################################################################")
        print("#                          Store Net Function                                   #")
        print("#################################################################################")
        currDense = 1
        currActivation = 1
        currPool = 1
        currConv1D = 1
        fp = self.outputFilePath + 'mdw_nn.h'
        print('Write mdw_nn header into:' + fp)

        # header file
        with open(fp, 'w') as f:

            # doxy description for module
            self.writeHeader(f, False)

            # inluce files
            self.writeInclude(f, False)

            # public function
            if self.fixPointBits == 7:
                f.write('int16_t mdw_nn_run(q7_t * q7_input_data);\n')
            elif self.fixPointBits == 15:
                f.write('int16_t mdw_nn_run(q15_t * q15_input_data);\n')

        # fully connected module
        fp = self.outputFilePath + 'mdw_nn.c'
        print('Write mdw_nn source into:' + fp)
        with open(fp, 'w') as f:

            # doxy description for module
            self.writeHeader(f, True)

            # inluce files
            self.writeInclude(f, True)

            # find layer with the largest output shape layer
            currHighest = 0
            for layer in self.model.layers[0:len(self.model.layers)]:
                if "dense" in layer.name:
                    if layer.output_shape[1] > currHighest:
                        currHighest = layer.output_shape[1]

            # create buffer, LAYER_1_IN_DIM is allways 756 with MNIST
            if self.fixPointBits == 7:
                f.write('q15_t aq15_in_Buf[LAYER_1_IN_DIM];\n')
                f.write('q7_t aq7_out_Buf[' + str(currHighest) + '];\n\n')
                f.write('int16_t mdw_nn_run(q7_t * aq7_input_data)\n{\n')
            elif self.fixPointBits == 15:
                f.write('#define MAX_BUFFER_SIZE CONV1D_1_OU_CH*CONV1D_1_IN_DIM\n\n')
                f.write('q15_t* aq15_input_Buf;\n')
                f.write('q15_t* aq15_output_Buf;\n')
                f.write('q15_t buffer[MAX_BUFFER_SIZE];\n\n')
                f.write('int16_t mdw_nn_run(q15_t * aq15_input_data)\n{\n')

            # local variables
            f.write('    q31_t prediction = 0;\n\n')

            # Memeory Get functions
            f.write('    /* GET MEMORY BUFFER FROM SHARED MEMORY */\n')
            f.write('    aq15_input_Buf = mdw_mem_get(MAX_BUFFER_SIZE*2);\n')
            f.write('    aq15_output_Buf = mdw_mem_get(MAX_BUFFER_SIZE*2);\n\n')
            f.write('    memset(aq15_input_Buf, 0, sizeof(q15_t)*MAX_BUFFER_SIZE);\n')
            f.write('    memset(aq15_output_Buf, 0, sizeof(q15_t)*MAX_BUFFER_SIZE);\n\n')

            lastLayer = self.model.layers[0].name
            idx_shift = 0
            # write the rest of the net
            for layer in self.model.layers[1:len(self.model.layers)]:
                if "dense" in layer.name:
                    print("ADD DENSE LAYER")
                    # Add a fully connected function call
                    self.writeLayer(f, currDense)
                    currDense = currDense + 1
                    idx_shift = idx_shift + 1
                elif "activation" in layer.name:
                    print("ADD ACTIVATION LAYER")
                    # Add a activation function call
                    act = layer.get_config().get('activation')
                    if "dense" in lastLayer:
                        self.writeActivation(f, act, currDense, idx_shift, lastLayer)
                    elif "conv1d" in lastLayer:
                        self.writeActivation(f, act, currConv1D, idx_shift, lastLayer)
                    currActivation = currActivation + 1
                elif "conv1d" in layer.name:
                    print("ADD CONV1D LAYER")
                    self.writeConv1D(f, currConv1D)
                    currConv1D = currConv1D + 1
                    idx_shift = idx_shift + 1
                elif "global_average_pooling1d" in layer.name:
                    print("ADD GLOBAL AVERAGE POOLING 1D")
                    self.writeGlobalAveragePooling1D(f, currDense)
                    currPool = currPool + 1

                lastLayer = layer.name

            # get the prediction (highest value of output buffer)
            f.write('\n')
            f.write('    /* GET THE PREDICTION */\n')
            f.write('    prediction = (q31_t) aq15_output_Buf[0];\n\n')
            f.write('    q31_t max_train_q = 17869;          // 34.9 converted in Q6.9 format\n')
            f.write('    q31_t min_train_q = 4710;           // 9.8 converted in Q6.9 format\n\n')
            f.write('    // Q2.13 * Q6.9 = Q8.23 => min_train_q << OUTPUT_SHIFT\n')
            f.write('    prediction = prediction * (max_train_q - min_train_q) + (min_train_q << OUTPUT_SHIFT);\n')
            f.write('    prediction = prediction >> OUTPUT_SHIFT;       // Q8.23 => Q6.9\n\n')
            f.write('    /* RELEASE MEMORY BLOC */\n')
            f.write('    mdw_mem_release(aq15_output_Buf);\n')
            f.write('    mdw_mem_release(aq15_input_Buf);\n\n')
            f.write('    return (q15_t) prediction;\n}\n')

    def writeHeader(self, f, sFile):
        """ Write module description to f

        f: file pointer
        sFile: True if f is the .c file
        """
        now = datetime.datetime.now()
        if sFile:
            f.write("/** \n")
            f.write(" * @brief       Autogenerated module by Keras2arm.py\n")
            f.write(" *              This module contains a function which can be used to\n")
            f.write(" *              classify MNIST images using the ARM-CMSIS-NN lib\n")
            if self.fixPointBits == 7:
                f.write(" * @Note        This module uses the q7 implementation\n")
            elif self.fixPointBits == 15:
                f.write(" * @Note        This module uses the q15 implementation\n")
            f.write(" * @date        " + now.strftime("%Y-%m-%d %H:%M") + '\n')
            f.write(" * @file        mdw_nn.c\n")
            f.write(" * @author      Raphael Zingg zing@zhaw.ch\n")
            f.write(" * @copyright   2018 ZHAW / Institute of Embedded Systems\n")
            f.write(" */\n")
        else:
            f.write("/**\n")
            f.write(
                " * @brief       Autogenerated header file of the mdw_nn module\n")
            if self.fixPointBits == 7:
                f.write(" * @Note        This module uses the q7 implementation\n")
            elif self.fixPointBits == 15:
                f.write(" * @Note        This module uses the q15 implementation\n")
            f.write(" * @date        " + now.strftime("%Y-%m-%d %H:%M") + '\n')
            f.write(" * @file        mdw_nn.h\n")
            f.write(" * @author      Raphael Zingg zing@zhaw.ch\n")
            f.write(" * @copyright   2018 ZHAW / Institute of Embedded Systems\n")
            f.write(" */\n")

    @staticmethod
    def writeInclude(f, sFile):
        """ Write includes for .h and .c file f

        f: file pointer
        sFile: True if f is the .c file
        """
        if sFile:
            f.write('#include "arm_nnfunctions.h"\n')
            f.write('#include "weights.h"\n')
            f.write('#include "mdw_nn.h"\n')
            f.write('#include "mdw_mem.h"\n')
            f.write('#include "local_fct.h"\n')
            f.write('#include <stdint.h>\n')
            f.write('#include "app_conf.h"\n\n')
        else:
            f.write('#include "arm_nnfunctions.h"\n')

    def writeLayer(self, f, nd):
        """ Write functions call for dense layers

        f:   file pointer
        nd:  number of the dense layers already parsed
        """
        if self.fixPointBits == 7:
            f.write('    arm_fully_connected_q7(\n')
            f.write('        aq7_out_Buf,\n')
            f.write('        aq7_dense_' + str(nd) + '_weights,\n')
            f.write('        DENSE_' + str(nd) + '_IN_DIM,\n')
            f.write('        DENSE_' + str(nd) + '_OU_DIM,\n')
            f.write('        DENSE_' + str(nd) + '_IN_SHIFT,\n')
            f.write('        DENSE_' + str(nd) + '_OU_SHIFT,\n')
            f.write('        aq7_dense_' + str(nd) + '_bias,\n')
            f.write('        aq7_out_Buf,\n')
            f.write('        aq15_in_Buf);\n')
        elif self.fixPointBits == 15:
            f.write('    /* FULL CONNECTED LAYER */\n')
            f.write('    memcpy(aq15_input_Buf, aq15_output_Buf, sizeof(q15_t)*MAX_BUFFER_SIZE);\n')
            f.write('    arm_fully_connected_q15(aq15_input_Buf,\n')
            f.write('                            aq15_dense_' + str(nd) + '_weights,\n')
            f.write('                            DENSE_' + str(nd) + '_IN_DIM,\n')
            f.write('                            DENSE_' + str(nd) + '_OU_DIM,\n')
            f.write('                            DENSE_' + str(nd) + '_IN_SHIFT,\n')
            f.write('                            DENSE_' + str(nd) + '_OU_SHIFT,\n')
            f.write('                            aq15_dense_' + str(nd) + '_bias,\n')
            f.write('                            aq15_output_Buf,\n')
            f.write('                            NULL);\n')

    def writeActivation(self, f, act, nd, shift_idx, last):
        """ Write functions call for activation layer

        f:    file pointer
        act:  string of activation function, ['tanh', 'relu', 'sigmoid', 'softmax']
        nd:   number of the dense layers already parsed
        """
        f.write('    /* ACTIVATION LAYER */\n')
        # TANH or SIGMOID
        if act == 'tanh':
            f.write('    int16_t index = 0;\n\n')
            f.write('    for (int i = 0; i < ')
            if "dense" in last:
                f.write('DENSE_' + str(nd - 1) + '_OU_DIM')
            elif "conv1d" in last:
                f.write('CONV1D_' + str(nd) + '_OU_CH*CONV1D_' + str(nd) + '_OU_DIM')
            f.write('; i++)\n')
            f.write('    {\n')
            f.write('        index = (int16_t) aq15_output_Buf[i]>>TANH_SHIFT;\n')
            f.write('        if(index < 0)\n')
            f.write('            index = 0;\n')
            f.write('        if(index >= sizeof(tanh_lut))\n')
            f.write('            index = sizeof(tanh_lut) - 1;\n')
            f.write('        aq15_output_Buf[i] = (q31_t) tanh_lut[index];\n')
            f.write('    }\n')
        elif act == 'sigmoid':
            if self.fixPointBits == 7:
                f.write('    arm_nn_activations_direct_q7(aq7_output_Buf,\n')
            elif self.fixPointBits == 15:
                f.write('    arm_nn_activations_direct_q15(  aq15_output_Buf,\n')
            if "dense" in last:
                f.write('                                    DENSE_' + str(nd - 1) + '_OU_DIM,\n')
            elif "conv1d" in last:
                f.write('                                    CONV1D_' + str(nd) + '_OU_CH*CONV1D_' + str(nd) + '_OU_DIM,\n')
            # optimal integer part is: 7 - the shiftParameter
            # but it has to be <= 3 see source code of arm_nn_activations_direct_q7:
            # https://arm-software.github.io/CMSIS_5/NN/html/group__Acti.html
            intRange = self.fixPointBits - self.outputShift[shift_idx]
            if intRange > 3:
                print("Warning: Saturation in Activation, Input Int Range = ", intRange)
                intRange = 3
            f.write('                                    ' + str(int(intRange)) + ',\n')
            f.write('                                    ARM_SIGMOID);\n')

        # RELU
        elif act == 'relu':
            if self.fixPointBits == 7:
                f.write('    arm_relu_q7(aq7_output_Buf,\n')
            elif self.fixPointBits == 15:
                f.write('    arm_relu_q15(   aq15_output_Buf,\n')
            if "dense" in last:
                f.write('                    DENSE_' + str(nd-1) + '_OU_DIM);\n')
            elif "conv1d" in last:
                f.write('                    CONV1D_' + str(nd-1) + '_OU_CH*CONV1D_' + str(nd-1) + '_OU_DIM);\n')
            

        # SOFTMAX
        elif act == 'softmax':
            if self.fixPointBits == 7:
                f.write('    arm_softmax_q7(\n')
                f.write('        aq7_out_Buf,\n')
                f.write('        LAYER_' + str(nd - 1) + '_OU_DIM,\n')
                f.write('        aq7_out_Buf);\n')
            elif self.fixPointBits == 15:
                f.write('    memcpy(aq15_input_Buf, aq15_out_Buf, sizeof(aq15_out_Buf));\n')
                f.write('    arm_softmax_q15(\n')
                f.write('        aq15_input_Buf,\n')
                f.write('        LAYER_' + str(nd - 1) + '_OU_DIM,\n')
                f.write('        aq15_out_Buf);\n')

    def writeConv1D(self, f, nd):
        """ Write functions call for conv1d layers

        f:   file pointer
        nd:  number of the conv1d layers already parsed
        """
        if self.fixPointBits == 7:
            f.write('    /* CONVOLUTION LAYER '+ str(nd) +' */\n')
            if nd == 1:
                f.write('    memcpy(aq15_input_Buf, aq15_input_data, sizeof(q15_t)*CONV1D_1_IN_DIM);\n')
            else:
                f.write('    memcpy(aq15_input_Buf, aq15_output_Buf, sizeof(q15_t)*MAX_BUFFER_SIZE);\n')
            f.write('    arm_convolve_HWC_q7_fast_nonsquare(aq7_input_Buf\n')
            f.write('        CONV1D_' + str(nd) + '_IN_DIM,\n')
            f.write('        1,\n')
            f.write('        CONV1D_' + str(nd) + '_IN_CH,\n')
            f.write('        aq7_layer_' + str(nd) + '_weights,\n')
            f.write('        CONV1D_' + str(nd) + '_OU_CH,\n')
            f.write('        CONV1D_' + str(nd) + '_KER_DIM,\n')
            f.write('        1,\n')
            f.write('        CONV1D_' + str(nd) + '_PADDING,\n')
            f.write('        1,\n')
            f.write('        CONV1D_' + str(nd) + '_STRIDE,\n')
            f.write('        1,\n')
            f.write('        aq7_layer_' + str(nd) + '_bias,\n')
            f.write('        CONV1D_' + str(nd) + '_IN_SHIFT,\n')
            f.write('        CONV1D_' + str(nd) + '_OU_SHIFT,\n')
            f.write('        aq7_output_Buf,\n')
            f.write('        CONV1D_' + str(nd) + '_OU_DIM,\n')
            f.write('        1,\n')
            f.write('        aq15_input_Buf,\n')
            f.write('        NULL);\n')
        elif self.fixPointBits == 15:
            f.write('    /* CONVOLUTION LAYER '+ str(nd) +' */\n')
            if nd == 1:
                f.write('    memcpy(aq15_input_Buf, aq15_input_data, sizeof(q15_t)*CONV1D_1_IN_DIM);\n')
            else:
                f.write('    memcpy(aq15_input_Buf, aq15_output_Buf, sizeof(q15_t)*MAX_BUFFER_SIZE);\n')
            f.write('    local_convolve_HWC_q15_fast_nonsquare(aq15_input_Buf,\n')
            f.write('                                        CONV1D_' + str(nd) + '_IN_DIM,\n')
            f.write('                                        1,\n')
            f.write('                                        CONV1D_' + str(nd) + '_IN_CH,\n')
            f.write('                                        aq15_conv1d_' + str(nd) + '_weights,\n')
            f.write('                                        CONV1D_' + str(nd) + '_OU_CH,\n')
            f.write('                                        CONV1D_' + str(nd) + '_KER_DIM,\n')
            f.write('                                        1,\n')
            f.write('                                        CONV1D_' + str(nd) + '_PADDING,\n')
            f.write('                                        1,\n')
            f.write('                                        CONV1D_' + str(nd) + '_STRIDE,\n')
            f.write('                                        1,\n')
            f.write('                                        aq15_conv1d_' + str(nd) + '_bias,\n')
            f.write('                                        CONV1D_' + str(nd) + '_IN_SHIFT,\n')
            f.write('                                        CONV1D_' + str(nd) + '_OU_SHIFT,\n')
            f.write('                                        aq15_output_Buf,\n')
            f.write('                                        CONV1D_' + str(nd) + '_OU_DIM,\n')
            f.write('                                        1,\n')
            f.write('                                        buffer,\n')
            f.write('                                        NULL);\n\n')

    def writeGlobalAveragePooling1D(self, f, nd):
        """ Write functions call for global average pooling 1D layers

        f:   file pointer
        nd:  number of the global average pooling 1D layers already parsed
        """
        if self.fixPointBits == 7:
            f.write('    arm_avepool_q7_HWC(\n')
            f.write('        aq7_input_Buf,\n')
            f.write('        POOL_' + str(nd) + '_IN_DIM,\n')
            f.write('        POOL_' + str(nd) + '_IN_CH,\n')
            f.write('        POOL_' + str(nd) + '_KER_DIM,\n')
            f.write('        POOL_' + str(nd) + '_PADDING,\n')
            f.write('        POOL_' + str(nd) + '_STRIDE,\n')
            f.write('        POOL_' + str(nd) + '_OU_DIM,\n')
            f.write('        aq15_input_Buf),\n')
            f.write('        aq7_output_Buf);\n')
        elif self.fixPointBits == 15:
            f.write('    /* AVERAGE POOLING LAYER '+ str(nd) +' */\n')
            f.write('    memcpy(aq15_input_Buf, aq15_output_Buf, sizeof(q15_t)*MAX_BUFFER_SIZE);\n')
            f.write('    local_avepool_q15_HWC(  aq15_input_Buf,\n')
            f.write('                            POOL_' + str(nd) + '_IN_DIM,\n')
            f.write('                            1,\n')
            f.write('                            POOL_' + str(nd) + '_IN_CH,\n')
            f.write('                            POOL_' + str(nd) + '_KER_DIM,\n')
            f.write('                            1,\n')
            f.write('                            POOL_' + str(nd) + '_PADDING,\n')
            f.write('                            1,\n')
            f.write('                            POOL_' + str(nd) + '_STRIDE,\n')
            f.write('                            1,\n')
            f.write('                            POOL_' + str(nd) + '_OU_DIM,\n')
            f.write('                            1,\n')
            f.write('                            0,\n')
            f.write('                            (q7_t*)buffer,\n')
            f.write('                            aq15_output_Buf);\n\n')


    def storeExample(self):
        """ Write header file with data example
        """
        print()
        print("#################################################################################")
        print("#                          Store Example                                        #")
        print("#################################################################################")
        fp = self.outputFilePath + 'test_data.h'
        print('Write Data example define into:' + fp)
        with open(fp, 'w') as f:
            idx = 0
            print(self.evalData[0].shape)
            f.write('q15_t input_ex[' + str(len(self.evalData[0]/2)) + '][' + str(len(self.evalData[0][0])) + '] = {\n')
            for i in range(0, len(self.evalData[0]),2):
                f.write('    {')
                np.transpose(self.evalData[0][i]).tofile(
                    f, sep=", ", format="%d")
                f.write('},\n')
                idx = idx + 1
            f.write('};\n')