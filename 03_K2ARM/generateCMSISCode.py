"""
Warper script for the k2arm framework to
create .h and .c source files which represent a pre trained keras model.
This code can be used to run the keras model with the ARM CMSIS-NN functions
on an embedded target such as the Cortex-M4

References:
    [1]: KEIL CMSIS Documentation,
         https://www.keil.com/pack/doc/CMSIS/DSP/html/group__float__to__x.html

    [2]: Tensorflow documentation,
         https://www.tensorflow.org/api_docs/python/tf/quantization/fake_quant_with_min_max_args

    [3]: liangzhen-lai, ARM-CMSIS Github, https://github.com/ARM-software/CMSIS_5/issues/327

    [4]: Raphael Zingg, ZHAW / Institute of Embedded systems
         https://blog.zhaw.ch/high-performance/2018/12/21/machine-learning-on-cortex-m4-using-keras-and-arm-cmsis-nn/

@author: Baptiste Solioz <baptiste.solioz@gmail.com>
"""
import keras as k
from k2arm import k2arm
import pandas as pd
import numpy as np
from sklearn.preprocessing import StandardScaler

# --------------------------------------------------------------------------------------------------
#                                           settings
# --------------------------------------------------------------------------------------------------
modelPath = 'models/cnn_model_tanh_large.h5' # chose the model (modelMin, modelLar, modelMid)
qFormat = 15 # set to 7 or 15 -> 7 not tested !!!!

# --------------------------------------------------------------------------------------------------
#                                      load and scale data
# --------------------------------------------------------------------------------------------------
dataset = pd.read_csv("models/feat_fft_20_150.csv")
dataset = dataset.drop(dataset.columns[0], axis=1)
y_data = dataset['FlowRate'].astype(float).to_numpy()
x_data = dataset.drop(['FlowRate'], axis=1).to_numpy()

x_data_n = x_data.reshape((x_data.shape[0],x_data.shape[1], 1))
print(x_data_n.shape)
print(y_data.shape)
evalData = [x_data_n, y_data]

# --------------------------------------------------------------------------------------------------
#                                      load pre-trained model
# --------------------------------------------------------------------------------------------------
classifier = k.models.load_model(modelPath, compile=False)

# --------------------------------------------------------------------------------------------------
#                                      create k2arm parser
# --------------------------------------------------------------------------------------------------
parser = k2arm( outputFilePath='../02_Firmware/Middlewares/mdw_nn/', 
                fixPointBits=qFormat,
                model=classifier, 
                evalData=evalData)

# --------------------------------------------------------------------------------------------------
#                      parse the model from keras to CMSIS-NN parameters
# --------------------------------------------------------------------------------------------------
parser.quantizeWeights()
parser.findOutputFormat()
parser.calculateShift()

# --------------------------------------------------------------------------------------------------
#                      Simulate the behavior of the model with CMSIS-NN
# --------------------------------------------------------------------------------------------------
parser.verifyAccuracy()

# --------------------------------------------------------------------------------------------------
#                                      write the C-code files
# --------------------------------------------------------------------------------------------------

parser.storeWeights()
parser.storeDimension()
parser.storeConvParam()
parser.storePoolParam()
parser.storeOutShiftParams()
parser.storeNetFunction()
parser.storeBitSize()
parser.storeExample()

print()
print()
print('Model successfully parsed and C-code generated!')
