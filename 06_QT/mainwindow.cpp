/****************************************************************************
**
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/


#include "mainwindow.h"

#include "ui_mainwindow.h"

#include <QtCore/QDateTime>
#include <QtMqtt/QMqttClient>
#include <QtWidgets/QMessageBox>

// Define the EUI for the 4 flowmeters
#define     DEV_EUI_1   "application/3/device/f018dfe7c668b101/#"
#define     DEV_EUI_2   "application/3/device/f018dfe7c668b102/#"
#define     DEV_EUI_3   "application/3/device/f018dfe7c668b103/#"
#define     DEV_EUI_4   "application/3/device/f018dfe7c668b104/#"

/***********************************************************************************
 * \brief   Constructor
 * \param   None
 * \retval  None
 ***********************************************************************************/
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    _conParam = new ConnectWindow();
    // Create the MQTT client
    m_client = new QMqttClient(this);
    m_client->setHostname(_conParam->getHost());
    m_client->setPort(_conParam->getPort());
    // Connect the MQTT client with callbacks
    connect(m_client, &QMqttClient::stateChanged, this, &MainWindow::updateLogStateChange);
    connect(m_client, &QMqttClient::disconnected, this, &MainWindow::brokerDisconnected);
    connect(m_client, &QMqttClient::messageReceived, this, [this](const QByteArray &message, const QMqttTopicName &topic) {
        const QString content = QDateTime::currentDateTime().toString()
                    + QLatin1String(" Received Topic: ")
                    + topic.name()
                    + QLatin1Char('\n');
        ui->editLog->insertPlainText(content);
    });
    connect(m_client, &QMqttClient::pingResponseReceived, this, [this]() {
        const QString content = QDateTime::currentDateTime().toString()
                    + QLatin1String(" PingResponse")
                    + QLatin1Char('\n');
        ui->editLog->insertPlainText(content);
    });

    updateLogStateChange();
    // Disable unused buttons until the connection is established
    ui->buttonDisplay->setDisabled(true);
    ui->selectedDevice->setDisabled(true);
    // Create the four subscription window
    _subDisplay[0] = new SubscriptionWindow();
    _subDisplay[1] = new SubscriptionWindow();
    _subDisplay[2] = new SubscriptionWindow();
    _subDisplay[3] = new SubscriptionWindow();

    qDebug() << "MainWindow Launched";
}
/***********************************************************************************
 * \brief   Destructor
 * \param   None
 * \retval  None
 ***********************************************************************************/
MainWindow::~MainWindow()
{
    delete ui;
    qApp->quit();
}
/***********************************************************************************
 * \brief   Callback when the buttonConnect is clicked
 * \param   None
 * \retval  None
 ***********************************************************************************/
void MainWindow::on_buttonConnect_clicked()
{
    qDebug() << "buttonConnect clicked";
    if (m_client->state() == QMqttClient::Disconnected) {
        // Status Connected -> try connection
        m_client->setHostname(_conParam->getHost());
        m_client->setUsername(_conParam->getUser());
        m_client->setPassword(_conParam->getPassword());
        m_client->setPort(_conParam->getPort());
        ui->buttonConnect->setText(tr("Disconnect"));
        m_client->connectToHost();
    } else {
        // Status Disconnected -> disconnect from broker
        ui->buttonConnect->setText(tr("Connect"));
        m_client->disconnectFromHost();
    }
}
/***********************************************************************************
 * \brief   Callback when the buttonParam is clicked
 * \param   None
 * \retval  None
 ***********************************************************************************/
void MainWindow::on_buttonParam_clicked()
{
    // Open the parameter window
    _conParam->updateDisplay();
    _conParam->setWindowTitle("MQTT Param");
    _conParam->show();
}
/***********************************************************************************
 * \brief   Callback when the buttonQuit is clicked
 * \param   None
 * \retval  None
 ***********************************************************************************/
void MainWindow::on_buttonQuit_clicked()
{
    // Kill the application
    QApplication::quit();
}
/***********************************************************************************
 * \brief   Callback when the buttonDisplay is clicked
 * \param   None
 * \retval  None
 ***********************************************************************************/
void MainWindow::on_buttonDisplay_clicked()
{
    uint16_t devid = ui->selectedDevice->value()-1;
    QString topic;
    qDebug() << "buttonDisplay clicked with devid = " + QString::number(devid+1);
    // Check if the window is already opened
    if(!_subDisplay[devid]->isVisible())
    {
        // Select the right topic
        switch(devid)
        {
            case 0: topic = DEV_EUI_1; break;
            case 1: topic = DEV_EUI_2; break;
            case 2: topic = DEV_EUI_3; break;
            case 3: topic = DEV_EUI_4; break;
            default: break;
        }
        qDebug() << "buttonDisplay clicked with topic = " + topic;
        // Subscribe to the topic
        _sub[devid] = m_client->subscribe(topic, 0);
        if (!_sub[devid]) {
            QMessageBox::critical(this, QLatin1String("Error"), QLatin1String("Could not subscribe. Is there a valid connection?"));
            return;
        }
        QString devname = QString("dev-")+QString::number(devid+1);
        qDebug() << "buttonDisplay clicked start subdisplay " + devname;
        // Show the Subscription window
        _subDisplay[devid]->setSubscription(_sub[devid]);
        _subDisplay[devid]->setWindowTitle(_sub[devid]->topic().filter());
        _subDisplay[devid]->setDeviceName(devname);
        _subDisplay[devid]->show();
    }
    else
    {
    }
}
/***********************************************************************************
 * \brief   Callback when the MQTT status changes
 * \param   None
 * \retval  None
 ***********************************************************************************/
void MainWindow::updateLogStateChange()
{
    const QString content = QDateTime::currentDateTime().toString()
                    + QLatin1String(": State Change")
                    + QString::number(m_client->state())
                    + QLatin1Char('\n');
    ui->editLog->insertPlainText(content);
    if(m_client->state() == 2)
    {
        // Enable the display data button
        // Allow to subscribe to a topic
        ui->buttonDisplay->setEnabled(true);
        ui->selectedDevice->setEnabled(true);
    }
    else
    {
        // Disable the display data button
        ui->buttonDisplay->setDisabled(true);
        ui->selectedDevice->setDisabled(true);
    }
}
/***********************************************************************************
 * \brief   Callback when a broker's disconnection occurs
 * \param   None
 * \retval  None
 ***********************************************************************************/
void MainWindow::brokerDisconnected()
{
    ui->buttonConnect->setText(tr("Connect"));
}
/***********************************************************************************
 * \brief   Allow to change the client port
 * \param   p : new client port
 * \retval  None
 ***********************************************************************************/
void MainWindow::setClientPort(int p)
{
    m_client->setPort(p);
}
