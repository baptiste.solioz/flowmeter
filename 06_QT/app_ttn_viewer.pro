QT       += core gui
QT       += network
QT       += mqtt
QT += charts

RC_ICONS += flowmeter.ico

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    chart.cpp \
    connectwindow.cpp \
    main.cpp \
    mainwindow.cpp \
    subscriptionwindow.cpp

HEADERS += \
    chart.h \
    connectwindow.h \
    mainwindow.h \
    subscriptionwindow.h

FORMS += \
    connectwindow.ui \
    mainwindow.ui \
    subscriptionwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    flowmeter.png
