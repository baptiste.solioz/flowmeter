/****************************************************************************
**
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/


#include "subscriptionwindow.h"
#include "ui_subscriptionwindow.h"

#include <QJsonObject>
#include <QJsonDocument>
#include <QtCharts/QAbstractAxis>
#include <QtCharts/QSplineSeries>
#include <QtCharts/QValueAxis>
#include <QtCharts/QChartView>

/***********************************************************************************
 * \brief   Constructor
 * \param   None
 * \retval  None
 ***********************************************************************************/
SubscriptionWindow::SubscriptionWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SubscriptionWindow)
{
    ui->setupUi(this);

    // Create Series
    series = new QLineSeries();
    // Create Chart Dev1
    _chartDev1 = new QChart;
    _chartDev1->setTitle("Waiting for data");
    _chartDev1->legend()->hide();
    // Add the new serie to the chart
    _chartDev1->addSeries(series);
    qDebug() << "Series added to chart bordel de merde";
    // Prepare X axis with timestamp
    axisX = new QDateTimeAxis;
    axisX->setTickCount(10);
    axisX->setFormat("hh:mm:ss");
    axisX->setTitleText("Time");
    _chartDev1->addAxis(axisX, Qt::AlignBottom);
    series->attachAxis(axisX);
    // Prepare Y axis with flow rate
    axisY = new QValueAxis;
    axisY->setLabelFormat("%i");
    axisY->setTitleText("FlowRate [m3/s]");
    axisY->setRange(0, 40);
    _chartDev1->addAxis(axisY, Qt::AlignLeft);
    series->attachAxis(axisY);

    // Set the UI graphics view with the chart
    ui->graphicsView->setChart(_chartDev1);
    ui->graphicsView->setRenderHint(QPainter::Antialiasing);
}

/***********************************************************************************
 * \brief   Destructor
 * \param   None
 * \retval  None
 ***********************************************************************************/
SubscriptionWindow::~SubscriptionWindow()
{
    qDebug() << "Unsubscribe and delete subscription";
    _sub->unsubscribe();
    delete _sub;
    delete ui;
}
/***********************************************************************************
 * \brief   Set the subscription parameters
 * \param   sub : New subscription
 * \retval  None
 ***********************************************************************************/
void SubscriptionWindow::setSubscription(QMqttSubscription *sub)
{
    _sub = sub;
    // Set the label with topic
    ui->labelSub->setText(_sub->topic().filter());
    ui->labelDevEUI->setText("None");
    updateStatus(_sub->state());
    // Connect the subscription with the callback
    connect(_sub, &QMqttSubscription::messageReceived, this, &SubscriptionWindow::updateMessage);
    connect(_sub, &QMqttSubscription::stateChanged, this, &SubscriptionWindow::updateStatus);
}
/***********************************************************************************
 * \brief   Update message callback -> update the chart
 * \param   msg: New message
 * \retval  None
 ***********************************************************************************/
void SubscriptionWindow::updateMessage(const QMqttMessage &msg)
{
    QByteArray br = msg.payload();

    QJsonDocument doc = QJsonDocument::fromJson(br);

    QJsonObject obj = doc.object();
    // Check the device name
    if(obj.value("deviceName") == _deviceName)
    {
        QString devEUI = obj.value("devEUI").toString();
        _chartDev1->setTitle(_deviceName);
        // Check the port = 1 for application message
        if(obj.value("fPort") == 1)
        {
            QString data = obj.value("object").toObject().value("DecodeDataHex").toString();
            QStringList list2 = data.split(QLatin1Char(',')).replaceInStrings("0x","");
            QDateTime timestamp;
            qreal val = 0.0;
            int i = 0;
            bool ok;
            qreal cumul = 0.0;
            int nb_samples = 0;
            // Retrieve all message with flowRate + Timestamps
            while(i<list2.size())
            {
                if(i%6 == 0)
                {
                   val = (qreal)list2.at(i).toUInt(&ok, 16) + (list2.at(i+1).toUInt(&ok, 16)<<8);
                   val /= 512;
                   i = i + 2;
                   cumul += val;
                   nb_samples++;
                }
                else
                {
                    uint32_t ts_1 = (uint32_t)list2.at(i).toUInt(&ok, 16);
                    uint32_t ts_2 = (uint32_t)list2.at(i+1).toUInt(&ok, 16)<<8;
                    uint32_t ts_3 = (uint32_t)list2.at(i+2).toUInt(&ok, 16)<<16;
                    uint32_t ts_4 = (uint32_t)list2.at(i+3).toUInt(&ok, 16)<<24;
                    timestamp.setTime_t(ts_1 + ts_2 + ts_3 + ts_4);
                    qDebug() << "Timestamp = " + timestamp.toString() + "Val = " + QString::number(val);
                    ui->resultEdit->insertPlainText("Timestamp = " +
                                                    timestamp.toString() +
                                                    " FlowRate = " + QString::number(val)+
                                                    "[m3/s]\n"
                                                    );
                    i = i + 4;
                }
            }
            // Average the flow rate
            cumul = cumul /nb_samples;
            // Add the flowrate to the serie
            series->append(timestamp.toMSecsSinceEpoch(), cumul);
            // Update the axis range
            axisX->setRange(QDateTime::fromMSecsSinceEpoch(series->at(0).x()),
                      QDateTime::fromMSecsSinceEpoch(series->at(series->count()-1).x()));
            // Keep only 20 values
            if(series->count() > 20)
                series->remove(0);

        }
        // Label = DevEUI received
        ui->labelDevEUI->setText(devEUI);
    }
}
/***********************************************************************************
 * \brief   Update the status of the subscription
 * \param   state : new state
 * \retval  None
 ***********************************************************************************/
void SubscriptionWindow::updateStatus(QMqttSubscription::SubscriptionState state)
{
    // Change the status text
    switch (state) {
    case QMqttSubscription::Unsubscribed:
        ui->labelStatus->setText(QLatin1String("Unsubscribed"));
        break;
    case QMqttSubscription::SubscriptionPending:
        ui->labelStatus->setText(QLatin1String("Pending"));
        break;
    case QMqttSubscription::Subscribed:
        ui->labelStatus->setText(QLatin1String("Subscribed"));
        break;
    case QMqttSubscription::Error:
        ui->labelStatus->setText(QLatin1String("Error"));
        break;
    default:
        ui->labelStatus->setText(QLatin1String("--Unknown--"));
        break;
    }
}
/***********************************************************************************
 * \brief   Set the Device name
 * \param   deviceName : New device
 * \retval  None
 ***********************************************************************************/
void SubscriptionWindow::setDeviceName(QString deviceName)
{
    _deviceName = deviceName;
}
/***********************************************************************************
 * \brief   Get the device name
 * \param   None
 * \retval  Device Name
 ***********************************************************************************/
QString SubscriptionWindow::getDeviceName()
{
    return _deviceName;
}
/***********************************************************************************
 * \brief   Set the topic
 * \param   topic : New topic
 * \retval  None
 ***********************************************************************************/
void SubscriptionWindow::setTopic(QString topic)
{
    _topic = topic;
}
/***********************************************************************************
 * \brief   Get the actual topic
 * \param   None
 * \retval  Topic
 ***********************************************************************************/
QString SubscriptionWindow::getTopic()
{
    return _topic;
}
/***********************************************************************************
 * \brief   Callback for the Reset serie button
 * \param   None
 * \retval  None
 ***********************************************************************************/
void SubscriptionWindow::on_buttonResetSerie_clicked()
{
    // Clear the complete serie
    series->clear();
    ui->resultEdit->clear();
}


