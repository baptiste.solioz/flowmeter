#include "connectwindow.h"
#include "ui_connectwindow.h"
#include "mainwindow.h"

/***********************************************************************************
 * \brief   Constructor
 * \param   parent
 * \retval  None
 ***********************************************************************************/
ConnectWindow::ConnectWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ConnectWindow)
{
    ui->setupUi(this);

    //_host = "192.168.1.123";
    _host = "192.168.43.176";           // Set the host address
    _user = "";                         // No user and password with ChirpStack
    _password = "";
    _topic = "application/3/#";         // Default application topic
    _port = 1883;                       // Non-secure port

    // Initialize the UI text
    ui->lineEditHost->setText(_host);
    ui->lineEditUser->setText(_user);
    ui->lineEditPassword->setText(_password);
    ui->lineEditTopic->setText(_topic);
    ui->spinBoxPort->setValue(_port);
    // Connect Accept button with callback
    connect(ui->buttonBox, &QDialogButtonBox::accepted, this, &ConnectWindow::button_accepted);

}

/***********************************************************************************
 * \brief   Destructor
 * \param   None
 * \retval  None
 ***********************************************************************************/
ConnectWindow::~ConnectWindow()
{
    delete ui;
}

/***********************************************************************************
 * \brief   Button accepted callback, called from UI
 * \param   None
 * \retval  None
 ***********************************************************************************/
void ConnectWindow::button_accepted()
{
    // Change value
    _host = ui->lineEditHost->text();
    _user = ui->lineEditUser->text();
    _password = ui->lineEditPassword->text();
    _topic = ui->lineEditTopic->text();
}

/***********************************************************************************
 * \brief   Update the Display if needed
 * \param   None
 * \retval  None
 ***********************************************************************************/
void ConnectWindow::updateDisplay()
{
    // Set the UI text value
    ui->lineEditHost->setText(_host);
    ui->lineEditUser->setText(_user);
    ui->lineEditPassword->setText(_password);
    ui->lineEditTopic->setText(_topic);
    ui->spinBoxPort->setValue(_port);
}

/***********************************************************************************
 * \brief   Set the host value
 * \param   host : New Host
 * \retval  None
 ***********************************************************************************/
void ConnectWindow::setHost(QString host)
{
    _host = host;
}
/***********************************************************************************
 * \brief   Set the User name
 * \param   user : New user
 * \retval  None
 ***********************************************************************************/
void ConnectWindow::setUser(QString user)
{
    _user = user;
}
/***********************************************************************************
 * \brief   Set the new password
 * \param   password : New password
 * \retval  None
 ***********************************************************************************/
void ConnectWindow::setPassword(QString password)
{
    _password = password;
}
/***********************************************************************************
 * \brief   Set the new port number
 * \param   port : New port number
 * \retval  None
 ***********************************************************************************/
void ConnectWindow::setPort(uint16_t port)
{
    _port = port;
}
/***********************************************************************************
 * \brief   Set the new topic name
 * \param   topic : New Topic
 * \retval  None
 ***********************************************************************************/
void ConnectWindow::setTopic(QString topic)
{
    _topic = topic;
}
/***********************************************************************************
 * \brief   Get host value
 * \param   None
 * \retval  host
 ***********************************************************************************/
QString ConnectWindow::getHost()
{
    return _host;
}
/***********************************************************************************
 * \brief   Get user value
 * \param   None
 * \retval  user
 ***********************************************************************************/
QString ConnectWindow::getUser()
{
    return _user;
}
/***********************************************************************************
 * \brief   Get password value
 * \param   None
 * \retval  password
 ***********************************************************************************/
QString ConnectWindow::getPassword()
{
    return _password;
}
/***********************************************************************************
 * \brief   Get port value
 * \param   None
 * \retval  port
 ***********************************************************************************/
uint16_t ConnectWindow::getPort()
{
    return _port;
}
/***********************************************************************************
 * \brief   Get topic value
 * \param   None
 * \retval  topic
 ***********************************************************************************/
QString ConnectWindow::getTopic()
{
    return _topic;
}
