#ifndef CONNECTWINDOW_H
#define CONNECTWINDOW_H

#include <QDialog>

namespace Ui {
class ConnectWindow;
}

class ConnectWindow : public QDialog
{
    Q_OBJECT

public:
    explicit ConnectWindow(QWidget *parent = nullptr);
    ~ConnectWindow();

public slots:
    void button_accepted();

public:
    void updateDisplay();
    void setHost(QString host);
    void setUser(QString user);
    void setPassword(QString password);
    void setPort(uint16_t port);
    void setTopic(QString topic);

    QString getHost();
    QString getUser();
    QString getPassword();
    uint16_t getPort();
    QString getTopic();

private:
    Ui::ConnectWindow *ui;

    QString _host;
    QString _user;
    QString _password;
    QString _topic;
    uint16_t _port;
};

#endif // CONNECTWINDOW_H
