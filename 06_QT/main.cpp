#include "mainwindow.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    // Set the title name
    w.setWindowTitle("Flow Rate Measurements");
    w.show();
    return a.exec();
}
